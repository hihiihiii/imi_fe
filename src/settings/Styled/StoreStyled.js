import styled from "@emotion/native";

const ContactContainer = styled.View`
  margin-top: ${(props) => {
    if (props.top) {
      return "0px";
    } else {
      return "20px";
    }
  }};
  width: 100%;
  height: 125px;
  background-color: white;
`;

const ContactImg = styled.View`
  width: 80px;
  height: 80px;
  border-radius: 5px;
`;
// 문의 내역 제목
const ContactTitle = styled.Text`
  font-size: 15px;
  font-weight: bold;
  color: #000;
`;

//문의 날짜
const ContactDate = styled.Text`
  font-size: 8px;
  font-weight: bold;
`;

// 문의 내역 문의 내용
const ContactContents = styled.Text`
  font-size: 13px;
  font-weight: bold;
  opacity: ${(props) => {
    if (props.opa) {
      return "1";
    } else {
      return "0.35";
    }
  }};
  color: #000;
  text-align: left;
`;

const ContactTextBox = styled.View`
  padding-left: 13px;
  padding-top: 13px;
  height: 158px;
  border-radius: 10px;
  background-color: #ededed;
`;
const ContactSubText = styled.Text`
  font-size: 13px;
  color: #31323b;
  font-weight: 500;
`;

const Answer = styled.TextInput`
  padding-top: 20px;
  padding-left: 14px;

  font-size: 15px;
  background-color: #fff;
  border-radius: 10px;
  width: 100%;
  height: 225;
`;

const ContactImgBox = styled.View`
  margin-top: 10px;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
  align-items: flex-start;
  justify-content: space-between;
`;

const GoodsImg = styled.ImageBackground`
  width: 165px;
  height: 165px;
  border-radius: 10px;
  background-color: white;
  margin-bottom: 5px;
`;

const CategoryCheckBox = styled.TouchableOpacity`
  width: 30px;
  height: 30px;
  border-radius: 30px;
  border-width: ${(props) => {
    if (props.action) {
      return "2px";
    }
    return;
  }};
  background-color: #ededed;
`;

const CategoryTitle = styled.Text`
  margin-left: 10px;
  font-size: 14px;
  color: #000;
  font-weight: 500;
  border-bottom-width: ${(props) => {
    let bottom = "30px";
    if (props.bottom) return (bottom = "0px");
  }};
`;
const ProductImg = styled.ImageBackground`
  width: 330px;
  height: 330px;
  margin-right: 7px;
`;
const HeartBox = styled.View`
  width: 29px;
  height: 29px;
  border-radius: 29px;
  background-color: #fe1577;
`;

export {
  Answer,
  ContactContainer,
  ContactSubText,
  ContactImg,
  ContactTextBox,
  ContactTitle,
  ContactImgBox,
  ContactDate,
  ContactContents,
  GoodsImg,
  CategoryCheckBox,
  CategoryTitle,
  HeartBox,
  ProductImg,
};
