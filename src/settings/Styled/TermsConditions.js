import styled from "@emotion/native";


const TermsConditionsBackground = styled.View`
    width: 100%;
    height: 100%;
    background-color: #fff;
`;



const TermsHeadText = styled.Text`
    font-size: 20px;
    font-weight: 600;
    color: #000;
    text-align: left;

`; 

const TermsFirstBox = styled.View`
    width: 100%;
    height: 136px;
    background-color: #fff;
`

const TermsSecondBox = styled.View`
    width: 100%;
    height: 448px;
    background-color: #fff;
`

export {TermsConditionsBackground}