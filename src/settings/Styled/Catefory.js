import styled from "@emotion/native";

const CategoryProfileImg = styled.ImageBackground`
  width: 35px;
  height: 35px;
  border-radius: 35px;
  background-color: white;
`;

const CategoryProfileText = styled.Text`
  font-size: ${(props) => {
    // 폰트는 기본 15px  사이즈 10 을 입력하면  바뀜
    let size = "15px";
    if (props.size) size = "10px";
    return size;
  }};

  opacity: ${(props) => {
    let opa = 1;
    if (props.opa) opa = "0.5";
    return opa;
  }};

  font-weight: ${(props) => {
    if (props.weight) return weight;
  }};

  color: #181818;
`;
// //게시글 날짜?
// const CategoryDate = styled.Text`
//     font-size : 10px;
//     opacity: 0.5;
//     color : #181818;
// `

const CategoryContainer = styled.TouchableOpacity`
  margin-top: 15px;
  padding: 0px 9px;
  width: 100%;
  height: 445px;
  border-radius: 20px;
  background-color: #ededed;
`;

const CategoryImgBox = styled.ImageBackground`
  //width: 100%;
  height: 295px;
  border-radius: 13px;
  background-color: white;
`;

//조회수
const Views = styled.Text`
  font-size: 10px;
  color: #565656;
  margin-top: 7px;
`;
//게시글 설명란.
const ViewContent = styled.Text`
  margin-top: 7px;
  font-size: 10px;
  color: #181818;
  font-weight: bold;
`;

const LikeBox = styled.View`
  width: 37px;
  height: 17px;
  align-items: center;
  justify-content: center;
  opacity: 0.5;
  border-radius: 9px;
  background-color: black;
`;

const LikeCount = styled.Text`
  font-size: 8px;
  font-weight: bold;
  color: white;
`;

const SilderCountBox = styled.View`
  width: 37px;
  height: 17px;
  align-items: center;
  justify-content: center;
  opacity: 0.5;
  border-radius: 9px;
  background-color: black;
`;

const SilderCountText = styled.Text`
  font-size: 8px;
  color: white;
`;

const DetailReadingBox = styled.View`
  margin-top: 17px;
  width: 100%;
  height: 213px;
  background-color: #fff;
  padding: 0px 25px;
`;
const DetailTitle = styled.Text`
  margin-top: 32px;
  font-size: 32px;
  font-weight: bold;
  color: #241332;
`;
const DetailContents = styled.Text`
  margin-top: 19px;
  font-size: 16px;
`;

export {
  SilderCountText,
  SilderCountBox,
  LikeCount,
  LikeBox,
  ViewContent,
  Views,
  CategoryImgBox,
  CategoryContainer,
  CategoryProfileText,
  CategoryProfileImg,
  DetailReadingBox,
  DetailTitle,
  DetailContents,
};
