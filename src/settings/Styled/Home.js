import styled from "@emotion/native";

const HomeScrolView = styled.ScrollView`
  padding-top: 15px;
  margin-bottom: 50px;
`;

const HomeBox = styled.View`
  border-radius: ${(props) => {
    let radius = "25px";
    if (props.radius) {
      radius = "15px";
      return radius;
    }
  }};
  height: 175px;
`;

const HomeBackGround = styled.ImageBackground`
  width: 100%;
  padding-left: 6px;
  border-radius: ${(props) => {
    let radius = "25px";
    if (props.radius) {
      radius = "15px";
      return radius;
    }
  }};
  height: 175px;
`;

const InfluenceBox = styled.View`
  margin-top: 50px;
  margin-bottom: 50px;
  padding-left: 28px;
  width: 100%;
`;

const InfluenceText = styled.Text`
  font-size: 20px;
  text-align: left;
  font-weight: bold;
  color: #000;
  margin-bottom: 20px;
`;

const InfluenceCircle = styled.View`
  width: 72px;
  margin-right: 21px;
  height: 72px;
  border-radius: 50px;
  padding: 3px;
  background-color: blue;
`;

const InfluenceCircleImg = styled.ImageBackground`
  width: 66px;
  height: 66px;
  border-radius: 66px;
  background-color: #f5f6fa;
`;

const InfluenceCircleName = styled.Text`
  font-size: 10px;
  color: #737476;
`;

export {
  HomeScrolView,
  HomeBox,
  HomeBackGround,
  InfluenceBox,
  InfluenceText,
  InfluenceCircle,
  InfluenceCircleImg,
  InfluenceCircleName,
};
