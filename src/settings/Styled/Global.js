import styled from '@emotion/native';
import LinearGradient from 'react-native-linear-gradient';

// Containers
const Wrapper = styled.View`
  width: 100%;
  padding: 0px 16px;
`;

const FlexBox = styled.View`
  width: 100%;
  flex-direction: ${props => {
    return props.dir;
  }};
  align-items: ${props => {
    return props.align;
  }};
  justify-content: ${props => {
    return props.justify;
  }};
`;

//condi
const Condi = styled.View`
  background-color: #fff;
  margin-top: 8px;
  border-radius: 10px;
`;

const Condi_par = styled.View`
  padding: 16px;
`;

const Condi_Text = styled.Text`
  font-size: 15px;
  line-height: 21px;
  color: #707070;
`;
const Condi_Title = styled.Text`
  font-size: 20px;
  font-weight: 600;
`;

//soscialWeb
const SocialWebMainBox = styled.View`
  margin-top: 24px;
`;

// 그라디언트
const LinearButton = styled(LinearGradient)`
  width: 100%;
  border-radius: 100px;
  height: 50px;
  align-items: center;
  justify-content: center;
`;

const LinearView = styled(LinearGradient)`
  width: 100%;
  height: 102px;

  border-radius: 100px;
  align-items: center;
  justify-content: center;
`;
// Buttons
// Main Round Button with Border Radius
const MainButton = styled.TouchableOpacity`
  width: 100%;
  border-radius: 100px;
  height: 50px;
  align-items: center;
  justify-content: center;
  background-color: #a780ff;
  /* background-color: linear-gradient(#e66465, #9198e5); */
`;

// Sub Button without Border Radius (Rectentular)
const SubButton = styled.TouchableOpacity``;

// TextInput
const MainInput = styled.TextInput`
  width: 100%;
  height: 47px;
  border-width: ${props => {
    let border = '0px';
    if (props.border) border = '1px';
    return border;
  }};
  border-color: #ebebeb;
  background-color: #fff;
  font-size: 15px;
  font-weight: 500;
  border-radius: 10px;
  margin-bottom: 20px;
  padding-left: 15px;
`;

const MainBox = styled.TouchableOpacity`
  width: 100%;
  height: 47px;
  border-width: ${props => {
    if (props.action) {
      return '2px';
    }
    return;
  }};
  border-color: ${props => {
    if (props.action) {
      //
      return '#7e8ddf';
    }
    return '#fff';
  }};
  justify-content: center;
  background-color: #fff;
  border-radius: 10px;
  padding-left: 15px;
  margin-bottom: 20px;
`;

const MainTextCheckBox = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
`;

const MainText = styled.Text`
  font-size: 15px;
  font-weight: bold;
  text-align: left;
`;

const MainCheckBox = styled.View`
  width: 14px;
  height: 14px;
  border-radius: 16px;
  background-color: #f4f4f4;
`;

const MainMarginTop = styled.View`
  margin-top: 45px;
`;

const OnOffBox = styled.View`
  width: 77px;
  height: 40px;
  border-radius: 40px;
  justify-content: center;
  padding-left: 3px;
  background-color: #eaebff;
`;
const OnOff = styled.View`
  width: 35px;
  height: 35px;
  background-color: #868be5;
  border-radius: 35px;
`;
//북마크

const ContentsImg = styled.View`
  width: 45px;
  height: 45px;
  border-radius: 7px;
  background-color: #ededed;
  margin-right: 19px;
`;

const ContentsTitle = styled.Text`
  font-size: 18px;
  font-weight: 500;
  text-align: left;
  margin-bottom: 5px;
  color: #000;
`;

const ContentsText = styled.Text`
  font-size: 14px;
  font-weight: 500;
  text-align: left;
  opacity: 0.5;
`;

const DrewBox = styled.View`
  margin-top: 39px;
  padding-right: 17px;
  padding-left: 17px;
`;

const DrewText = styled.Text`
  font-size: 20px;
  font-weight: bold;
  text-align: right;
  color: #000;
`;

const DrawerNaviStyle = styled.TouchableOpacity`
  height: 68px;
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  border-top-width: ${props => {
    if (props.first) {
      return;
    } else {
      return '1px';
    }
  }};
  border-top-color: ${props => {
    if (props.first) {
      return;
    } else {
      return '#e6e6e6';
    }
  }};
`;

const DrawerNavi_Text = styled.Text`
  font-size: 20px;
`;
const InfluenceTitle = styled.Text`
  margin-top: 10px;
  text-align: center;
  font-size: 15px;
  font-weight: bold;
  color: #000;
`;

const StoreCategoryImg = styled.ImageBackground`
  width: 141px;
  height: 180px;
  border-radius: 40px;
  background-color: #ededed;
`;

const ModalTitle = styled.Text`
  font-size: 22px;
  font-weight: bold;
`;

const ModalMainText = styled.Text`
  font-size: 15px;
  font-weight: bold;
  color: #31323b;
`;

const HeaderTitle = styled.Text`
  font-size: 20px;
  font-weight: 600;
  color: #000;
`;
const Acct_Compo = styled.View`
  width: 100%;
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const Acct_Text = styled.Text`
  font-weight: bold;
  font-size: 20px;
  line-height: 27px;
`;

const BackButtonBox = styled.View`
  width: 100px;
  height: 100px;
`;
export {
  DrawerNaviStyle,
  Acct_Compo,
  Acct_Text,
  DrawerNavi_Text,
  Wrapper,
  FlexBox,
  MainInput,
  MainButton,
  MainBox,
  MainText,
  MainCheckBox,
  MainTextCheckBox,
  MainMarginTop,
  LinearButton,
  LinearView,
  BackButtonBox,
  OnOffBox,
  OnOff,
  Condi,
  Condi_par,
  Condi_Text,
  Condi_Title,
  InfluenceTitle,
  SocialWebMainBox,
  ContentsImg,
  ContentsTitle,
  ContentsText,
  DrewText,
  DrewBox,
  StoreCategoryImg,
  ModalMainText,
  ModalTitle,
  HeaderTitle,
};
