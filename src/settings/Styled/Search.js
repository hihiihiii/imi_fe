import styled from "@emotion/native";

const SearchBox = styled.View`
  padding-left: 16px;
  width: 100%;
  margin-bottom: 38px;
  background-color: #fff;
  border-radius: 26px;
  height: 44px;
  flex-direction: row;
`;

const SearchText = styled.TextInput`
  text-align: left;
  font-size: 15px;
`;

const SearchResult = styled.Text`
  font-size: 15px;
  font-weight: 600;
  text-align: left;
  color: #4d4d4d;
`;

const SearchImgBox = styled.View`
  margin-top: 10px;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
`;

const SearchContents = styled.ImageBackground`
  border-radius: 19px;
  background-color: white;
  margin: 5px 5px;
`;

export { SearchText, SearchBox, SearchContents, SearchResult, SearchImgBox };
