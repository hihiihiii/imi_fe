import styled from "@emotion/native";

const BoxShadow = styled.View`
  margin-bottom: 13px;
  background-color: white;
  border-bottom-left-radius: 30px;
  border-bottom-right-radius: 30px;
`;

const HeadIconBox = styled.View`
  margin-top: 30.8px;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  width: 100%;
  padding: 0px 16px;
`;

const ProfileImg = styled.View`
  width: 106px;
  height: 106px;
  border-radius: 53px;
  align-items: center;
  justify-content: center;
  background-color: blue;
  margin-bottom: 10px;
`;

const ProfilePic = styled.ImageBackground`
  width: 98px;
  height: 98px;
  border-radius: 98px;
  background-color: white;
`;

const ProfileName = styled.Text`
  font-size: 25px;
  font-weight: bold;
  color: #181818;
  text-align: center;
  margin-bottom: 4px;
`;

const ProfileBoxBox = styled.View`
  margin: 0 14px;
`;

const ProfileBox = styled.View`
  width: 100%;
  height: 40px;
  background-color: white;
  border-radius: 10px;
  border-color: #798fdc;
  border-width: 1px;
  align-items: center;
  justify-content: center;
  margin-bottom: 18px;
`;

const ProfileText = styled.Text`
  font-size: 15px;
  font-weight: 500;
  color: #798fdc;
  text-align: center;
`;

const Portfolio_Profile = styled.View`
  margin-top: 7px;
  flex-direction: row;
  padding-left: 19px;
`;
const PortfolioButton = styled.Text`
  font-size: 15px;
  font-weight: bold;
  text-align: center;
`;

const ProfileButton = styled.Text`
  font-size: 15px;
  font-weight: bold;
  text-align: center;
`;

const PortfolioLeftImg = styled.ImageBackground`
  margin-top: 12px;

  width: ${(props) => {
    if (props.second) {
      return "142px";
    }
    if (props.third) {
      return "48.5%";
    }
    return "205px";
  }};

  height: ${(props) => {
    if (props.second) {
      return "128.5px";
    }
    return "269px";
  }};

  border-radius: 10px;
  //margin-right: 11px;
`;

const ProfileContent = styled.Text`
  font-size: 14px;
  font-weight: bold;
  margin-top: 13px;
  padding-left: 9px;
  color: #000;
`;

const ProfileContentTextBox = styled.View`
  width: 100%;
  height: 50px;
  margin-top: 2px;
  padding-left: 14;
  background-color: #ededed;
  border-radius: 10px;
`;

const ProfileButtonText = styled.Text`
  font-size: 13px;
  font-weight: bold;
  color: #fff;
`;

//profile 게시글 팔로워 팔로잉
const ProfileSubContentBox = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-around;
`;

const ProfileSubCount = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: #31323b;
  text-align: center;
`;

const ProfileSub = styled.Text`
  font-size: 10px;
  color: #a3a3a3;
  text-align: center;
`;

const ProfileIntroduction = styled.View`
  margin-bottom: 27px;
  width: 234px;
  height: 27px;
  color: #31323b;
  align-self: center;
`;

const ProfileIntroductionText = styled.Text`
  font-size: 10px;
  text-align: center;
`;

const BtnEff = styled.View`
  display: ${(props) => {
    return props.display;
  }};

  width: 75px;
  margin-top: 2px;
  border-bottom-width: 2px;
  border-color: #848ce4;
`;

const PortContainer = styled.View`
  margin-top: 5px;
  width: 100%;
  height: 252px;
  background-color: #fff;
`;

const PortImg = styled.View`
  margin-top: 20px;
  width: 165px;

  height: 165px;
  border-radius: 10px;
  background-color: #f0f0f0;
`;

const Strapline = styled.Text`
  margin-top: 21px;
  margin-bottom: 21px;
  font-size: 15px;
  font-weight: bold;
  text-align: left;
  color: #000;
`;
const SubContent = styled.Text`
  margin-bottom: 10px;
  font-size: 13px;
  text-align: left;
  color: #31323b;
`;
const DetailInfo = styled.TextInput`
  padding-top: 20px;
  padding-left: 14px;
  font-size: 15px;
  font-weight: bold;
  background-color: #fff;
  border-radius: 10px;
  width: 100%;
  height: 126px;
`;

export {
  BoxShadow,
  HeadIconBox,
  BtnEff,
  DetailInfo,
  ProfileBoxBox,
  ProfileImg,
  ProfilePic,
  ProfileBox,
  ProfileText,
  ProfileButton,
  Portfolio_Profile,
  PortfolioButton,
  PortfolioLeftImg,
  ProfileContent,
  ProfileName,
  ProfileContentTextBox,
  ProfileSubContentBox,
  ProfileSubCount,
  ProfileSub,
  ProfileButtonText,
  ProfileIntroduction,
  ProfileIntroductionText,
  PortContainer,
  PortImg,
  Strapline,
  SubContent,
};
