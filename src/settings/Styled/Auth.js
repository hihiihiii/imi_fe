import styled from "@emotion/native";

//Text 
const MainButtonText = styled.Text`
  font-size: 15px;
  color : #fff;
`;

const SubText = styled.Text`
    font-size : 13px; 
    color : ${(props)=>{
      let color = "#7391d7";
      if(props.color) color = "#43454b"
      return color; 
    }};
`;

// border-width: ${(props) => {
//   let border = "0px";
//   if (props.border) border = "1px";
//   return border;
// }};

const TextFelxEnd = styled.View`
    align-items: flex-end;
`;

const BottomText = styled.View`
    flex-direction: row;
    align-items : center;
    justify-content: center;
    margin-top: 25px;
`;

const AuthBox = styled.View`

`;

const AuthInputBox = styled.View`
  margin-bottom: 100px;
`;

const WelcomeBox = styled.View`
  align-items: center;
  margin-top:125px;
  margin-bottom: 30px;

  
`;

const WelcomeText = styled.Text`
  font-size: 28px;
  text-align: center;
  font-weight: bold;
  opacity: 0.5;
  width: 182px;
  height: 123px;
`;

const SplashView = styled.View`
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

const ChangePasswordButtonText = styled.Text`
  font-size: 13px;
  font-weight: bold;
  color: #fff;
`;

const SelfIntroductionBox = styled.TextInput`
  margin-top : 45px;
  padding-top : 20px;
  padding-left: 14px;
  font-size : 15px;
  background-color: #fff;
  border-radius : 10px;
  width: 100%;
  height: 225;
`; 

const SelfIntroductionButtonText = styled.Text`
  color : #fff;
  font-size : 13px;
  font-weight: bold;
  text-align: left;
`;



export {
  MainButtonText,
  SubText,
  TextFelxEnd,
  BottomText,
  AuthBox,
  AuthInputBox,
  WelcomeBox,
  WelcomeText,
  SplashView,
  ChangePasswordButtonText,
  SelfIntroductionBox,
  SelfIntroductionButtonText
};
