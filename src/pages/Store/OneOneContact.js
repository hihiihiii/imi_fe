import React from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { Back } from "../../Assets";
import { useHeader } from "../../Handler";
import { MainButtonText } from "../../settings/Styled/Auth";
import {
  MainBox,
  MainText,
  Wrapper,
  MainInput,
  LinearButton,
  FlexBox,
  HeaderTitle,
} from "../../settings/Styled/Global";
import {
  ContactTitle,
  ContactSubText,
  Answer,
} from "../../settings/Styled/StoreStyled";

const OneOneContact = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>1대1 문의하기</HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ height: "92.5%" }}>
          <Wrapper>
            <ContactTitle
              style={{ marginTop: 25, marginBottom: 20, fontSize: 18 }}
            >
              상품 정보
            </ContactTitle>

            <ContactSubText>상품명</ContactSubText>
            <MainInput
              placeholder="상품명을 입력하세요"
              style={{ backgroundColor: "#ededed", marginTop: 8 }}
            />
            <ContactSubText>제목</ContactSubText>
            <MainInput
              placeholder="제목을 입력하세요"
              style={{ backgroundColor: "#ededed", marginTop: 8 }}
            />
            <ContactSubText>상품명</ContactSubText>
            <Answer
              multiline={true}
              placeholder="문의 하실 내용을 입력해주세요"
              style={{
                backgroundColor: "#ededed",
                marginTop: 8,
                height: 126,
              }}
            />
          </Wrapper>
        </ScrollView>
        <Wrapper>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("ContactDetails");
            }}
          >
            <LinearButton
              colors={["#a780ff", "#20ad97", "#20ad97"]}
              start={{ x: 0.1, y: 0.35 }}
              end={{ x: 2.0, y: 0 }}
            >
              <MainButtonText>문의 하기</MainButtonText>
            </LinearButton>
          </TouchableOpacity>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

export default OneOneContact;
