import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import {Back} from '../../Assets';
import CustomModal from '../../Components/Modal';
import AddProductModal from '../../Components/Modal/Blocks/Store/AddProductModal';
import {useHeader} from '../../Handler';
import {
  Wrapper,
  MainInput,
  MainBox,
  MainText,
  FlexBox,
  HeaderTitle,
} from '../../settings/Styled/Global';
import {
  ContactContainer,
  ContactSubText,
  ContactTitle,
  GoodsImg,
} from '../../settings/Styled/StoreStyled';

const AddProduct = ({navigation}) => {
  const [isOpen, setIsOpen] = useState(false);
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>상품 추가</HeaderTitle>,
  });
  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        height={'55%'}
        Content={AddProductModal}
      />
      <SafeAreaView>
        <ScrollView style={{backgroundColor: '#ededed'}}>
          <ContactContainer style={{marginTop: 5, height: 209}}>
            <Wrapper>
              <GoodsImg
                style={{backgroundColor: '#ededed', marginTop: 20}}></GoodsImg>
            </Wrapper>
          </ContactContainer>
          <ContactContainer style={{marginTop: 5, height: 335}}>
            <Wrapper>
              <View style={{marginTop: 21}}>
                <ContactTitle style={{marginBottom: 21}}>
                  상품 정보
                </ContactTitle>

                <ContactSubText style={{marginBottom: 7}}>
                  상품명
                </ContactSubText>
                <MainInput
                  placeholder="상품명을 입력해주세요."
                  style={{backgroundColor: '#ededed'}}
                />

                <ContactSubText style={{marginBottom: 7}}>금액</ContactSubText>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="판매가를 입력해주세요."
                  style={{backgroundColor: '#ededed'}}
                />

                <ContactTitle style={{marginBottom: 21}}>옵션</ContactTitle>
                <MainBox
                  style={{backgroundColor: '#798fdc'}}
                  onPress={() => {
                    setIsOpen(true);
                  }}>
                  <MainText style={{color: 'white', textAlign: 'center'}}>
                    옵션추가
                  </MainText>
                </MainBox>
              </View>
            </Wrapper>
          </ContactContainer>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default AddProduct;
