import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  Wrapper,
  SocialWebMainBox,
  MainInput,
  LinearButton,
  FlexBox,
  HeaderTitle,
  LinearView,
  MainCheckBox,
} from '../../settings/Styled/Global';
import {ContactSubText} from '../../settings/Styled/StoreStyled';
import {MainButtonText} from '../../settings/Styled/Auth';
import styled from '@emotion/native';
import {Back, Check} from '../../Assets';
import {useHeader} from '../../Handler';

const GenderBox = styled.TouchableOpacity`
  width: 100px;
  height: 45px;
  background-color: #fff;
  border-width: ${props => {
    if (props.action) {
      return '1.5px';
    }
    return '0px';
  }};
  border-radius: 10px;
  border-color: ${props => {
    if (props.action) {
      return '#7e8ddf';
    }
    return '#b7b7bb';
  }};
  justify-content: center;
`;

const Gender = styled.Text`
  font-size: 15px;
  font-weight: 600;
  text-align: center;
  color: ${props => {
    if (props.action) {
      return '#050505';
    }
    return '#b7b7b7';
  }};
`;

const SellerEntry = ({navigation}) => {
  const [data, setData] = useState(false);

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{width: 50, height: 50, justifyContent: 'center'}}
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>판매자 입점</HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView style={{backgroundColor: '#ededed', height: '100%'}}>
        <ScrollView style={{height: '80%'}}>
          <Wrapper>
            <SocialWebMainBox style={{marginTop: 23}}>
              <ContactSubText style={{marginBottom: 5}}>
                검색 아이디입니다.
              </ContactSubText>
              <MainInput
                placeholder="상점 아이디를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="관리자 아이디를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="비밀번호를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="비밀번호 확인"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="대표자 성함을 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>

              {!data ? (
                <>
                  <FlexBox>
                    <View>
                      <MainInput
                        placeholder="휴대폰 번호를 입력해주세요"
                        placeholderTextColor="#c5c8ce"></MainInput>
                      <TouchableOpacity
                        style={{position: 'absolute', right: 20, top: 15}}
                        onPress={() => {
                          setData(true);
                        }}>
                        <Text
                          style={{
                            color: '#818de2',
                            fontSize: 14,
                            fontWeight: 'bold',
                          }}>
                          전송
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </FlexBox>
                </>
              ) : (
                <>
                  <Text
                    style={{
                      color: '#e28181',
                      fontWeight: 'bold',
                      fontSize: 14,
                      marginBottom: 5,
                    }}>
                    5:00
                  </Text>
                  <FlexBox style={{marginBottom: 5}}>
                    <View>
                      <MainInput
                        placeholder="인증번호"
                        placeholderTextColor="#c5c8ce"
                        style={{marginBottom: 0}}></MainInput>
                      <TouchableOpacity
                        style={{position: 'absolute', right: 20, top: 15}}>
                        <Text
                          style={{
                            color: '#818de2',
                            fontSize: 14,
                            fontWeight: 'bold',
                          }}>
                          인증
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </FlexBox>
                  <FlexBox
                    dir="row"
                    align="center"
                    justify="flex-end"
                    style={{marginBottom: 20}}>
                    <Text style={{color: '#b7b7bb'}}>
                      인증번호를 받지 못하셨나요?
                    </Text>
                    <TouchableOpacity>
                      <Text
                        style={{
                          color: '#818de2',
                          fontSize: 14,
                        }}>
                        재전송
                      </Text>
                    </TouchableOpacity>
                  </FlexBox>
                </>
              )}

              <MainInput
                placeholder="대표번호"
                placeholderTextColor="#c5c8ce"></MainInput>
            </SocialWebMainBox>
          </Wrapper>
        </ScrollView>

        <Wrapper>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('SellerEntrySecond');
            }}>
            <LinearButton
              colors={['#a780ff', '#20ad97', '#20ad97']}
              start={{x: 0.1, y: 0.35}}
              end={{x: 2.0, y: 0}}
              style={{alignSelf: 'center'}}>
              <MainButtonText>다음</MainButtonText>
            </LinearButton>
          </TouchableOpacity>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

const SellerEntrySecond = ({navigation}) => {
  const [data, setData] = useState([
    {gender: '사업자', check: true},
    {gender: '통신판매', check: false},
    {gender: '신분증', check: false},
  ]);

  const _handleChecker = idx => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].check) {
        data[i].check = false;
      }
    }
    const datas = [...data];
    datas[idx].check = !data[idx].check;
    setData(datas);
  };

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{width: 50, height: 50, justifyContent: 'center'}}
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>판매자 입점</HeaderTitle>,
  });

  //   useHeader({
  //     hideBorder: true,
  //     headerLeft: () => (
  //       <TouchableOpacity
  //         onPress={() => {
  //           navigation.goBack();
  //         }}
  //       >
  //         <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
  //           <Image source={Back} />
  //         </FlexBox>
  //       </TouchableOpacity>
  //     ),
  //     headerRight: () => <HeaderTitle></HeaderTitle>,
  //     headerTitle: () => <HeaderTitle>판매자 입점</HeaderTitle>,
  //   });
  return (
    <>
      <SafeAreaView style={{backgroundColor: '#ededed', height: '100%'}}>
        <ScrollView style={{height: '80%'}}>
          <Wrapper style={{paddingLeft: 28, paddingRight: 28}}>
            <SocialWebMainBox style={{marginTop: 23}}>
              <HeaderTitle style={{marginBottom: 5, fontWeight: 'bold'}}>
                SNS 정보
              </HeaderTitle>
              <MainInput
                placeholder="인스타그램 아이디"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="채널주소"
                placeholderTextColor="#c5c8ce"></MainInput>
              <HeaderTitle style={{marginBottom: 5, fontWeight: 'bold'}}>
                업체 정보
              </HeaderTitle>
              <MainInput
                placeholder="사업자 정보"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="업체명"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="사업자등록번호"
                placeholderTextColor="#c5c8ce"></MainInput>

              <FlexBox
                dir="row"
                justify="space-between"
                style={{width: '100%'}}>
                {data?.map((el, idx) => {
                  return (
                    <>
                      <GenderBox
                        onPress={() => {
                          _handleChecker(idx);
                        }}
                        action={el?.check}>
                        <Gender action={el?.check}>{el?.gender}</Gender>
                      </GenderBox>
                    </>
                  );
                })}
              </FlexBox>
            </SocialWebMainBox>
          </Wrapper>
        </ScrollView>
        <Wrapper>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('SellerEntryThird');
            }}>
            <LinearButton
              colors={['#a780ff', '#20ad97', '#20ad97']}
              start={{x: 0.1, y: 0.35}}
              end={{x: 2.0, y: 0}}
              style={{alignSelf: 'center'}}>
              <MainButtonText>다음</MainButtonText>
            </LinearButton>
          </TouchableOpacity>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

const SellerEntryThird = ({navigation}) => {
  const [checked, setChecked] = useState([
    {
      name: '체크',
      value: 'checked',
      checked: false,
    },
  ]);

  const _handleChecker = () => {
    const checkeds = [...checked];

    checkeds[0].checked = !checked[0].checked;
    setChecked(checkeds);
  };

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{width: 50, height: 50, justifyContent: 'center'}}
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>판매자 입점</HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView style={{backgroundColor: '#ededed', height: '100%'}}>
        <ScrollView style={{height: '80%'}}>
          <Wrapper style={{paddingLeft: 28, paddingRight: 28}}>
            <SocialWebMainBox style={{marginTop: 23}}>
              <HeaderTitle style={{marginBottom: 15, fontWeight: 'bold'}}>
                기타 정보
              </HeaderTitle>
              <MainInput
                placeholder="판매품목"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="브랜드명"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="제품 받을 주소"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="은행"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="계좌번호"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="예금주"
                placeholderTextColor="#c5c8ce"></MainInput>
              <TouchableOpacity
                onPress={() => {
                  _handleChecker();
                }}>
                <FlexBox dir="row" align="center" justify="flex-end">
                  <Text style={{marginRight: 15}}>약관 동의</Text>
                  {checked[0]?.checked ? (
                    <>
                      <View style={{width: 18}}>
                        <Image source={Check} />
                      </View>
                    </>
                  ) : (
                    <>
                      <LinearView
                        colors={['#a780ff', '#20ad97', '#20ad97']}
                        start={{x: 0.1, y: 0.35}}
                        end={{x: 2.0, y: 0}}
                        style={{
                          width: 18,
                          height: 18,
                        }}>
                        <MainCheckBox
                          style={{width: 16, height: 16}}></MainCheckBox>
                      </LinearView>
                    </>
                  )}
                </FlexBox>
              </TouchableOpacity>
            </SocialWebMainBox>
          </Wrapper>
        </ScrollView>

        <Wrapper>
          <TouchableOpacity
            onPress={() => {
              navigation.replace('Bottom');
            }}>
            <LinearButton
              colors={['#a780ff', '#20ad97', '#20ad97']}
              start={{x: 0.1, y: 0.35}}
              end={{x: 2.0, y: 0}}
              style={{alignSelf: 'center'}}>
              <MainButtonText>다음</MainButtonText>
            </LinearButton>
          </TouchableOpacity>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

export {SellerEntry, SellerEntrySecond, SellerEntryThird};
