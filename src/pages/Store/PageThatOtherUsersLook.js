import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
  Image,
  Text,
} from "react-native";
import { SubText } from "../../settings/Styled/Auth";
import {
  LinearView,
  Wrapper,
  FlexBox,
  MainText,
} from "../../settings/Styled/Global";
import { InfluenceCircleImg } from "../../settings/Styled/Home";
import { SearchText, SearchBox } from "../../settings/Styled/Search";
import { Back, HeaderRight, SearchImg } from "../../Assets";
import {
  ContactContents,
  ContactImgBox,
  ContactTitle,
  GoodsImg,
} from "../../settings/Styled/StoreStyled";
import CustomModal from "../../Components/Modal";
import ContactModal from "../../Components/Modal/Blocks/Store/ContactModal";
import { useHeader } from "../../Handler";
import { DrawerActions } from "@react-navigation/routers";

const PageThatOtherUsersLook = ({ navigation, route }) => {
  const [isOpen, setIsOpen] = useState(false);

  const [data, setData] = useState([
    {
      title: "Hollywood’s Bleeding",
      name: "1",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "beerbongs & bentleys",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Sunflower",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Motley crew",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Post malone pink-jacket",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Congratulations",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
  ]);
  console.log(route.parmas?.Details);
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
          <Text
            style={{
              fontWeight: "bold",
              fontSize: 24,
              textAlign: "left",
              letterSpacing: -0.39,
              width: 156,
            }}
          >
            {"5252byoioi"}
          </Text>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <>
        {route.params?.Details === false && (
          <TouchableOpacity
            onPress={() => {
              setIsOpen(true);
            }}
          >
            <View style={{ paddingRight: 28 }}>
              <Image source={HeaderRight} />
            </View>
          </TouchableOpacity>
        )}
      </>
    ),
    headerTitle: () => <Text></Text>,
  });

  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        height={"40%"}
        Content={() => (
          <ContactModal navigation={navigation} setIsOpen={setIsOpen} />
        )}
      />

      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", height: "100%" }}>
          <Wrapper>
            <FlexBox dir="row" align="center" style={{ marginTop: 25 }}>
              <TouchableOpacity>
                <LinearView
                  colors={["#a780ff", "#20ad97", "#20ad97"]}
                  start={{ x: 0.1, y: 0.35 }}
                  end={{ x: 2.0, y: 0 }}
                  style={{
                    width: 72,
                    height: 72,
                    padding: 3,
                  }}
                >
                  <InfluenceCircleImg
                    style={{ backgroundColor: "#ededed" }}
                    source={{
                      uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
                    }}
                    borderRadius={50}
                    resizeMode="cover"
                  ></InfluenceCircleImg>
                </LinearView>
              </TouchableOpacity>

              <FlexBox style={{ width: "63%", marginLeft: 15 }}>
                <MainText style={{ fontSize: 20 }}>hihi</MainText>
                <MainText style={{ fontSize: 16 }}>266k followers</MainText>
              </FlexBox>
              {route.params.Details !== false && (
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("OneOneContact");
                  }}
                >
                  <SubText>문의하기</SubText>
                </TouchableOpacity>
              )}
            </FlexBox>
            <SearchBox style={{ marginBottom: 10, marginTop: 25 }}>
              <TouchableOpacity
                style={{ alignSelf: "center", marginRight: 16.4 }}
              >
                <Image source={SearchImg} />
              </TouchableOpacity>
              <SearchText placeholder="검색어를 입력해주세요"></SearchText>
            </SearchBox>
            <FlexBox
              dir="row"
              justify="space-between"
              style={{ marginTop: 25 }}
            >
              <ContactTitle>상품 목록</ContactTitle>

              <ContactTitle style={{ opacity: 0.5, alignSelf: "flex-end" }}>
                총 21개
              </ContactTitle>
            </FlexBox>

            <ContactImgBox>
              {data?.map((el, idx) => {
                return (
                  <>
                    {/* width 값 주의 */}
                    <FlexBox style={{ width: "47.5%", marginBottom: 20 }}>
                      <TouchableOpacity
                        onPress={() => {
                          navigation.navigate("ProductDetails", {
                            Details: route.params?.Details,
                          });
                        }}
                      >
                        <GoodsImg
                          source={{ uri: el.image }}
                          borderRadius={14}
                          resizeMode="cover"
                        ></GoodsImg>
                      </TouchableOpacity>
                      <ContactTitle>{el.title}</ContactTitle>
                      <ContactContents>{el.price}</ContactContents>
                    </FlexBox>
                  </>
                );
              })}
            </ContactImgBox>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default PageThatOtherUsersLook;
