import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Text,
  View,
  Image,
} from "react-native";
import { Back, Check } from "../../Assets";
import { useHeader } from "../../Handler";
import { Wrapper, FlexBox, HeaderTitle } from "../../settings/Styled/Global";
import {
  Answer,
  ContactContainer,
  ContactSubText,
  ContactImg,
  ContactTextBox,
  ContactTitle,
  ContactImgBox,
  ContactDate,
  ContactContents,
  GoodsImg,
  CategoryCheckBox,
  CategoryTitle,
} from "../../settings/Styled/StoreStyled";

const CategoryItem = ({ navigation }) => {
  const [datas, setDatas] = useState([
    {
      title: "Hollywood’s Bleeding",
      name: "1",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "beerbongs & bentleys",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Sunflower",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Motley crew",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Post malone pink-jacket",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Congratulations",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
  ]);

  const [data, setData] = useState([
    {
      id: 1,
      title: "티셔츠",
      checked: false,
    },
    {
      id: 1,
      title: "후드/집업/맨투맨",
      checked: false,
    },
    {
      id: 1,
      title: "아우터",
      checked: false,
    },
    {
      id: 1,
      title: "니트/가디건",
      checked: false,
    },
    {
      id: 1,
      title: "셔츠/블라우스",
      checked: false,
    },
    {
      id: 1,
      title: "팬츠",
      checked: false,
    },
    {
      id: 1,
      title: "스커트",
      checked: false,
    },
    {
      id: 1,
      title: "원피스",
      checked: false,
    },
    {
      id: 1,
      title: "언더웨어",
      checked: false,
    },
    {
      id: 1,
      title: "스포츠웨어",
      checked: false,
    },
  ]);

  const _handleChecker = (idx) => {
    // for (let i = 0; i < data.length; i++) {
    //   if (data[i].checked) {
    //     data[i].checked = false;
    //   }
    // }
    const datas = [...data];
    datas[idx].checked = !data[idx].checked;
    setData(datas);
  };

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
          <HeaderTitle>카테고리 명</HeaderTitle>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle></HeaderTitle>,
  });

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ minHeight: "100%", backgroundColor: "#ededed" }}>
          <ContactContainer style={{ marginTop: 5, height: 246 }}>
            <Wrapper>
              <FlexBox dir="row" style={{ flexWrap: "wrap", marginTop: 16 }}>
                {data?.map((el, idx) => {
                  return (
                    <FlexBox
                      dir="row"
                      align="center"
                      style={{ width: "50%", marginBottom: 16 }}
                    >
                      {el?.checked ? (
                        <TouchableOpacity
                          onPress={() => {
                            _handleChecker(idx);
                          }}
                        >
                          <Image source={Check} />
                        </TouchableOpacity>
                      ) : (
                        <CategoryCheckBox
                          onPress={() => {
                            _handleChecker(idx);
                          }}
                          action={el?.checked}
                        ></CategoryCheckBox>
                      )}
                      <CategoryTitle>{el.title}</CategoryTitle>
                    </FlexBox>
                  );
                })}
              </FlexBox>
            </Wrapper>
          </ContactContainer>

          <ContactContainer
            style={{ marginTop: 5, height: 85 }}
          ></ContactContainer>

          <ContactContainer style={{ marginTop: 5, height: "100%" }}>
            <Wrapper>
              <ContactTitle
                style={{ opacity: 0.5, alignSelf: "flex-start", marginTop: 25 }}
              >
                총 21개
              </ContactTitle>

              <ContactImgBox>
                {datas?.map((el, idx) => {
                  return (
                    <>
                      {/* width 값 주의 */}
                      <FlexBox style={{ width: "47.5%", marginBottom: 20 }}>
                        <TouchableOpacity
                          onPress={() => {
                            navigation.navigate("ProductDetails", {
                              Details: true,
                            });
                          }}
                        >
                          <GoodsImg
                            source={{ uri: el.image }}
                            borderRadius={14}
                          ></GoodsImg>
                        </TouchableOpacity>
                        <ContactTitle>{el.title}</ContactTitle>
                        <ContactContents>{el.price}</ContactContents>
                      </FlexBox>
                    </>
                  );
                })}
              </ContactImgBox>
            </Wrapper>
          </ContactContainer>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default CategoryItem;
