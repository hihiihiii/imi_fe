import React, {useState} from 'react';
import {Image, SafeAreaView, ScrollView, Text, View} from 'react-native';
import {
  FlexBox,
  HeaderTitle,
  LinearButton,
  MainInput,
  ModalMainText,
  ModalTitle,
  Wrapper,
} from '../../settings/Styled/Global';
import {
  ContactContents,
  ContactImg,
  ContactTitle,
  CategoryTitle,
} from '../../settings/Styled/StoreStyled';
import styled from '@emotion/native';
import {Back, CountMinus, CountPlus} from '../../Assets';
import {TouchableOpacity} from 'react-native-gesture-handler';
import DropDownPicker from 'react-native-dropdown-picker';
import {Strapline, SubContent} from '../../settings/Styled/Profile';
import {MainButtonText} from '../../settings/Styled/Auth';
import {useHeader} from '../../Handler';

const CartCountBox = styled.View`
  width: 78px;
  height: 30px;
  border-radius: 100px;
  border-color: #e8e8e8;
  border-width: 1px;
`;

const CartCount = styled.Text`
  color: #000;
  font-size: 15px;
  font-weight: 600;
`;

const Payment = ({navigation}) => {
  const [data, setData] = useState([{}, {}]);
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([{label: 'Apple', value: 'apple'}]);

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{width: 50, height: 50, justifyContent: 'center'}}
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>상품결제</HeaderTitle>,
  });

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{height: '92.5%'}}>
          <Wrapper style={{backgroundColor: '#ededed'}}>
            <FlexBox
              align="flex-end"
              justify="flex-end"
              style={{marginTop: 27, marginBottom: 12}}>
              <Text style={{fontSize: 15, fontWeight: 'bold'}}>
                배송 상품 2개
              </Text>
            </FlexBox>
          </Wrapper>

          {data?.map((el, idx) => {
            return (
              <Wrapper
                style={{
                  borderBottomColor: '#ededed',
                  borderBottomWidth: 5,
                  paddingBottom: 23,
                }}>
                <FlexBox
                  dir="row"
                  align="center"
                  justify="space-between"
                  style={{marginTop: 17}}>
                  <ContactImg style={{backgroundColor: '#ededed'}}></ContactImg>

                  <FlexBox style={{width: '70%'}}>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactTitle>아이템 이름입니다.</ContactTitle>
                      <ContactContents
                        style={{textDecorationLine: 'line-through'}}>
                        77,000원
                      </ContactContents>
                    </FlexBox>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactContents>옵션 : L</ContactContents>
                      <ContactTitle>88,000원</ContactTitle>
                    </FlexBox>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactContents>수량:2</ContactContents>
                    </FlexBox>
                    <FlexBox
                      dir="row"
                      align="center"
                      justify="flex-end"
                      style={{marginTop: 2}}>
                      <ContactTitle>10,000원</ContactTitle>
                    </FlexBox>
                  </FlexBox>
                </FlexBox>
              </Wrapper>
            );
          })}
          <View style={{marginTop: 22}}>
            <Wrapper>
              <ModalMainText>쿠폰 / 할인</ModalMainText>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{marginTop: 14}}>
                <Text>포인트 사용</Text>
                <TouchableOpacity>
                  <FlexBox
                    align="center"
                    justify="center"
                    style={{
                      backgroundColor: '#f5f5f5',
                      width: 58,
                      height: 30,
                      borderRadius: 3,
                    }}>
                    <Text style={{alignSelf: 'center'}}>사용</Text>
                  </FlexBox>
                </TouchableOpacity>
              </FlexBox>
              <ContactContents
                style={{
                  textAlign: 'right',
                  fontWeight: 'bold',
                  fontSize: 10,
                  marginTop: 7,
                }}>
                사용가능 금액 1,380원
              </ContactContents>
            </Wrapper>
            <Wrapper
              style={{
                borderBottomColor: '#ededed',
                borderBottomWidth: 5,
                paddingBottom: 23,
              }}>
              <View style={{marginTop: 15}}>
                <Text>쿠폰 선택</Text>
                <DropDownPicker
                  placeholder="쿠폰 선택"
                  open={open}
                  value={value}
                  items={items}
                  setOpen={setOpen}
                  setValue={setValue}
                  setItems={setItems}
                  dropDownContainerStyle={{
                    backgroundColor: '#f5f5f5',
                    borderWidth: 0,
                  }}
                  textStyle={{fontSize: 15, fontWeight: 'bold'}}
                  min={0}
                  max={5}
                  style={{
                    backgroundColor: '#f5f5f5',
                    borderWidth: 0,
                    marginBottom: 10,
                    marginTop: 10,
                  }}
                  listMode="SCROLLVIEW"
                />
              </View>
            </Wrapper>

            <View style={{marginTop: 0}}>
              <Wrapper style={{borderBottomWidth: 5, borderColor: '#ededed'}}>
                <Strapline>주문자 정보</Strapline>
                <SubContent>이름</SubContent>
                <MainInput
                  placeholder="이름을 입력해주세요."
                  placeholderTextColor="#c5c8ce"
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
                <SubContent>전화번호</SubContent>
                <MainInput
                  placeholder="전화번호를 입력해주세요."
                  placeholderTextColor="#c5c8ce"
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
                <SubContent>주소</SubContent>
                <MainInput
                  placeholder="주소를 입력해주세요."
                  placeholderTextColor="#c5c8ce"
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                    marginBottom: 5,
                  }}
                />
                <MainInput
                  placeholder="상세주소를 입력해주세요."
                  placeholderTextColor="#c5c8ce"
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
                <SubContent>이메일</SubContent>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="이메일를 입력해주세요."
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
              </Wrapper>
            </View>
            <View style={{marginTop: 0}}>
              <Wrapper
                style={{
                  borderBottomColor: '#ededed',
                  borderBottomWidth: 5,
                  paddingBottom: 23,
                }}>
                <Strapline>배송자 입력</Strapline>
                <SubContent>받는 사람</SubContent>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="받는사람을 입력해주세요."
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
                <SubContent>전화번호</SubContent>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="전화번호를 입력해주세요."
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
                <SubContent>배송지명</SubContent>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="배송지명를 입력해주세요."
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />

                <SubContent>주소</SubContent>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="이메일를 입력해주세요."
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                    marginBottom: 5,
                  }}
                />
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="주소를 입력해주세요."
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
                <SubContent>배송 요청 사항</SubContent>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="배송 요청 사항을 입력해주세요."
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
              </Wrapper>
            </View>
          </View>
          <Wrapper
            style={{
              borderBottomWidth: 5,
              borderBottomColor: '#ededed',
              paddingBottom: 23,
            }}>
            <Strapline>결제수단</Strapline>
            <DropDownPicker
              placeholder="결제 수단을 입력해주세요."
              open={open}
              value={value}
              items={items}
              setOpen={setOpen}
              setValue={setValue}
              setItems={setItems}
              dropDownContainerStyle={{
                backgroundColor: '#f5f5f5',
                borderWidth: 0,
              }}
              textStyle={{fontSize: 15, fontWeight: 'bold'}}
              min={0}
              max={5}
              style={{
                backgroundColor: '#f5f5f5',
                borderWidth: 0,
                marginBottom: 10,
              }}
              listMode="SCROLLVIEW"
            />
          </Wrapper>
          <Wrapper>
            <View
              style={{
                marginTop: 23.8,
                borderBottomWidth: 0.5,
                borderBottomColor: '#575757',
              }}>
              <TouchableOpacity
                onPress={() => {
                  setIsOpen(true);
                }}>
                <CategoryTitle
                  style={{
                    fontSize: 16,
                    marginBottom: 6.2,
                    fontWeight: 'bold',
                  }}>
                  장바구니 합계
                </CategoryTitle>
              </TouchableOpacity>
            </View>
            <FlexBox
              dir="row"
              align="center"
              justify="space-between"
              style={{
                marginTop: 23.8,
                borderBottomWidth: 0.5,
                borderBottomColor: '#575757',
              }}>
              <CategoryTitle
                style={{fontSize: 16, marginBottom: 6.2, opacity: 0.45}}>
                선택 상품 개수
              </CategoryTitle>
              <CategoryTitle style={{fontSize: 16, marginBottom: 6.2}}>
                2개
              </CategoryTitle>
            </FlexBox>
            <FlexBox
              dir="row"
              align="center"
              justify="space-between"
              style={{
                marginTop: 23.8,
                borderBottomWidth: 0.5,
                borderBottomColor: '#575757',
              }}>
              <CategoryTitle
                style={{fontSize: 16, marginBottom: 6.2, opacity: 0.45}}>
                상품 금액
              </CategoryTitle>
              <CategoryTitle style={{fontSize: 16, marginBottom: 6.2}}>
                11,000원
              </CategoryTitle>
            </FlexBox>
            <FlexBox
              dir="row"
              align="center"
              justify="space-between"
              style={{
                marginTop: 23.8,
                borderBottomWidth: 0.5,
                borderBottomColor: '#575757',
              }}>
              <CategoryTitle
                style={{fontSize: 16, marginBottom: 6.2, opacity: 0.45}}>
                할인 금액
              </CategoryTitle>
              <CategoryTitle
                style={{fontSize: 16, marginBottom: 6.2, color: '#9586f1'}}>
                11,000원
              </CategoryTitle>
            </FlexBox>
            <FlexBox
              dir="row"
              align="center"
              justify="space-between"
              style={{
                marginTop: 23.8,
                borderBottomWidth: 0.5,
                borderBottomColor: '#575757',
              }}>
              <CategoryTitle
                style={{fontSize: 16, marginBottom: 6.2, opacity: 0.45}}>
                결제 금액
              </CategoryTitle>
              <CategoryTitle style={{fontSize: 16, marginBottom: 6.2}}>
                12,000원
              </CategoryTitle>
            </FlexBox>
          </Wrapper>
        </ScrollView>
        <Wrapper>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('PaymentCompleted');
            }}>
            <LinearButton
              colors={['#a780ff', '#20ad97', '#20ad97']}
              start={{x: 0.1, y: 0.35}}
              end={{x: 2.0, y: 0}}
              style={{alignSelf: 'center'}}>
              <MainButtonText>517,406원 결제하기</MainButtonText>
            </LinearButton>
          </TouchableOpacity>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

export default Payment;
