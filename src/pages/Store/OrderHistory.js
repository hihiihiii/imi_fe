import React from "react";
import { SafeAreaView, ScrollView, View, TouchableOpacity } from "react-native";
import { Wrapper, FlexBox } from "../../settings/Styled/Global";
import {
  ContactContainer,
  ContactContents,
  ContactImg,
  ContactTitle,
} from "../../settings/Styled/StoreStyled";
import { ProfileBox, ProfileText } from "../../settings/Styled/Profile";

const OrderHistory = () => {
  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", minHeight: "100%" }}>
          <ContactContainer style={{ marginTop: 10, height: 237 }}>
            <Wrapper style={{ paddingLeft: 28, paddingRight: 28 }}>
              <View style={{ marginTop: 18 }}>
                <FlexBox dir="row" justify="space-between" align="center">
                  <ContactTitle style={{ opacity: 0.35, fontSize: 18 }}>
                    2021.07.01
                  </ContactTitle>
                  <ContactTitle style={{ fontSize: 13 }}>
                    배송상품 2개
                  </ContactTitle>
                </FlexBox>
                <FlexBox
                  dir="row"
                  align="center"
                  justify="space-between"
                  style={{ marginTop: 25 }}
                >
                  <ContactImg
                    style={{ backgroundColor: "#ededed" }}
                  ></ContactImg>
                  <FlexBox style={{ width: "70%" }}>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactTitle>아이템 이름입니다.</ContactTitle>
                      <ContactContents
                        style={{ textDecorationLine: "line-through" }}
                      >
                        59,000
                      </ContactContents>
                    </FlexBox>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactContents>옵션 : L</ContactContents>
                      <ContactTitle>49,000원</ContactTitle>
                    </FlexBox>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactContents>수량:2</ContactContents>
                    </FlexBox>
                    <FlexBox dir="row" align="center" justify="flex-end">
                      <ContactTitle>98,000원</ContactTitle>
                    </FlexBox>
                  </FlexBox>
                </FlexBox>
                <TouchableOpacity>
                  <ProfileBox style={{ marginTop: 25 }}>
                    <ProfileText>주문 내역</ProfileText>
                  </ProfileBox>
                </TouchableOpacity>
              </View>
            </Wrapper>
          </ContactContainer>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default OrderHistory;
