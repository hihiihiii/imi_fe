import React from "react";
import { Image, SafeAreaView, ScrollView, View } from "react-native";
import {
  ContactContainer,
  ContactImg,
  ContactTitle,
  ContactDate,
  ContactContents,
  ContactTextBox,
  Answer,
} from "../../settings/Styled/StoreStyled";
import {
  Wrapper,
  FlexBox,
  LinearButton,
  HeaderTitle,
} from "../../settings/Styled/Global";

import {
  MainButtonText,
  SelfIntroductionBox,
} from "../../settings/Styled/Auth";
import { useHeader } from "../../Handler";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Back } from "../../Assets";

const OneOneAnswer = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>1대1 문의하기</HeaderTitle>,
  });

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "white", minHeight: "100%" }}>
          <ContactContainer
            top
            style={{
              borderBottomWidth: 9,
              borderBottomColor: "#ededed",
              marginTop: 50,
              height: 311,
            }}
          >
            <Wrapper>
              <ContactTitle style={{ marginBottom: 12 }}>
                문의 정보
              </ContactTitle>

              <FlexBox dir="row" style={{ marginBottom: 12 }}>
                <ContactImg style={{ backgroundColor: "#ededed" }}></ContactImg>
                <FlexBox justify="space-around" style={{ marginLeft: 15 }}>
                  <FlexBox
                    dir="row"
                    align="center"
                    style={{ width: "73%" }}
                    justify="space-between"
                  >
                    <ContactTitle>상품명</ContactTitle>
                    <ContactTitle>베이직 패치 W 니트 가디건</ContactTitle>
                  </FlexBox>
                  <FlexBox
                    dir="row"
                    align="center"
                    style={{ width: "73%" }}
                    justify="space-between"
                  >
                    <ContactTitle>문의 일시</ContactTitle>
                    <ContactTitle>2021.09.14 12:43</ContactTitle>
                  </FlexBox>
                </FlexBox>
              </FlexBox>

              <ContactTextBox>
                <ContactTitle>문의 내용입니다.</ContactTitle>
              </ContactTextBox>
            </Wrapper>
          </ContactContainer>

          <ContactContainer style={{ height: "100%" }}>
            <Wrapper>
              <ContactTitle>답변</ContactTitle>
              <Answer
                placeholder="답변을 입력해주세요"
                multiline={true}
                style={{ backgroundColor: "#ededed", marginTop: 5 }}
              />

              <View style={{ marginTop: 85 }}>
                <TouchableOpacity
                  onPress={() => {
                    navigation.goBack();
                  }}
                >
                  <LinearButton
                    colors={["#a780ff", "#20ad97", "#20ad97"]}
                    start={{ x: 0.1, y: 0.35 }}
                    end={{ x: 2.0, y: 0 }}
                    style={{ width: 319, alignSelf: "center" }}
                  >
                    <MainButtonText>답변하기</MainButtonText>
                  </LinearButton>
                </TouchableOpacity>
              </View>
            </Wrapper>
          </ContactContainer>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default OneOneAnswer;
