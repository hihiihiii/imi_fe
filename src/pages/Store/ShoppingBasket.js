import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  Text,
  View,
  Dimensions,
  Image,
} from "react-native";
import {
  FlexBox,
  HeaderTitle,
  LinearButton,
  LinearView,
  MainCheckBox,
  ModalTitle,
  Wrapper,
} from "../../settings/Styled/Global";
import { Back, Check, CountMinus, CountPlus, Xdark } from "../../Assets";
import { TouchableOpacity } from "react-native-gesture-handler";
import {
  CategoryTitle,
  ContactContents,
  ContactImg,
  ContactTitle,
  ProductImg,
} from "../../settings/Styled/StoreStyled";
import styled from "@emotion/native";
import { ChangePasswordButtonText } from "../../settings/Styled/Auth";
import { useHeader } from "../../Handler";

const CartCountBox = styled.View`
  width: 78px;
  height: 30px;
  border-radius: 100px;
  border-color: #e8e8e8;
  border-width: 1px;
`;

const CartCount = styled.Text`
  color: #000;
  font-size: 15px;
  font-weight: 600;
`;

const { width, height } = Dimensions.get("window");

const ShoppingBasket = ({ navigation }) => {
  //const [count, setCount] = useState
  const [allCount, setAllCount] = useState(0);
  const [allDiscount, setAllDiscount] = useState(0);
  const [allPrice, setAllPrice] = useState(0);
  // Data 형태는 추후 DB에 따라 변경됨
  // 현재는 임시 데이터 입니다
  const [datas, setDatas] = useState([
    {
      id: 1,
      title: "아이템 1번",
      option: [
        {
          label: "L",
          value: "L",
        },
      ],
      price: 19000,
      discount: 1800,
      // 추후 DB 테이블 참고하여 변경
      count: 1, // API 요청 결과 Data랑 map을 통하여 insert 시켜줘야함
      check: true,
    },
    {
      id: 2,
      title: "아이템 2번",
      option: [
        {
          label: "L",
          value: "L",
        },
      ],
      price: 19000,
      discount: 1800,
      // 추후 DB 테이블 참고하여 변경
      count: 1, // API 요청 결과 Data랑 map을 통하여 insert 시켜줘야함
      check: true,
    },
  ]);

  const allTrue = datas?.filter((el, idx) => !el?.check); // el?.check === false [{...}, {...}]

  const _handleAllCount = () => {
    let tempCnt = 0;
    for (let index = 0; index < datas.length; index++) {
      tempCnt = tempCnt + datas[index].count;
    }
    setAllCount(tempCnt);
  };

  useEffect(() => {
    _handleAllCount();
    _handleDiscount();
    _handlePrice();
  }, [datas]);

  const _handlerCountPlus = ({ idx, type }) => {
    const tempArr = [...datas]; // setState
    if (type === "minus") {
      if (tempArr[idx].count !== 0) tempArr[idx].count = tempArr[idx].count - 1;
    } else {
      tempArr[idx].count = tempArr[idx].count + 1;
    }
    setDatas(tempArr);
  };

  const _handleDiscount = () => {
    let tempDis = 0;
    for (let index = 0; index < datas.length; index++) {
      tempDis = tempDis + datas[index].discount * datas[index].count;
    }
    setAllDiscount(tempDis);
  };

  const _handlePrice = () => {
    let tempPrice = 0;
    for (let index = 0; index < datas.length; index++) {
      tempPrice =
        tempPrice +
        datas[index]?.count * (datas[index]?.price - datas[index].discount);
    }
    setAllPrice(tempPrice);
  };

  const _handleAllCheck = () => {
    const tempArr = [...datas]; // 데이터를 불러옴
    let status = true; // true 값을 하나 지정해둠
    if (allTrue?.length === 0) status = false; //if  allTrue 가 length 가 0이면 false 가 없다는 뜻 이죠
    for (let index = 0; index < tempArr.length; index++) {
      // tempArr 배열을 다돌려서
      tempArr[index].check = status; //true로 두는거죠 ?
    }
    setDatas(tempArr); //그걸 셋팅
  };

  const _handleSingleCheck = (idx) => {
    const tempArr = [...datas]; // setState
    tempArr[idx].check = !datas[idx].check;
    setDatas(tempArr);
  };

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>장바구니</HeaderTitle>,
  });

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ height: "92.5%" }}>
          <Wrapper
            style={{
              borderTopColor: "#ededed",
              borderTopWidth: 5,
            }}
          ></Wrapper>
          <Wrapper
            style={{
              height: 55,
              display: "flex",
              justifyContent: "center",
              borderBottomColor: "#ededed",
              borderBottomWidth: 5,
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <FlexBox dir="row" align="center" style={{ width: "50%" }}>
                {allTrue?.length === 0 ? (
                  <TouchableOpacity
                    onPress={() => {
                      _handleAllCheck();
                    }}
                  >
                    <LinearView
                      colors={["#a780ff", "#20ad97", "#20ad97"]}
                      start={{ x: 0.1, y: 0.35 }}
                      end={{ x: 2.0, y: 0 }}
                      style={{
                        width: 18,
                        height: 18,
                      }}
                    >
                      <MainCheckBox
                        style={{ width: 16, height: 16 }}
                      ></MainCheckBox>
                    </LinearView>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    onPress={() => {
                      _handleAllCheck();
                    }}
                  >
                    <Image source={Check} />
                  </TouchableOpacity>
                )}

                <Text style={{ marginLeft: 9 }}>전체 {datas?.length}개</Text>
              </FlexBox>

              <View>
                <Text>선택삭제</Text>
              </View>
            </FlexBox>
          </Wrapper>

          {datas?.map((el, idx) => {
            return (
              <>
                <Wrapper
                  style={{
                    height: 150,
                    display: "flex",
                    justifyContent: "center",
                    borderBottomColor: "#ededed",
                    borderBottomWidth: 5,
                  }}
                >
                  <FlexBox dir="row" align="center" justify="space-between">
                    <FlexBox dir="row" align="center" style={{ width: "50%" }}>
                      {el.check ? (
                        <TouchableOpacity
                          onPress={() => _handleSingleCheck(idx)}
                        >
                          <LinearView
                            colors={["#a780ff", "#20ad97", "#20ad97"]}
                            start={{ x: 0.1, y: 0.35 }}
                            end={{ x: 2.0, y: 0 }}
                            style={{
                              width: 18,
                              height: 18,
                            }}
                          >
                            <MainCheckBox
                              style={{ width: 16, height: 16 }}
                            ></MainCheckBox>
                          </LinearView>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          onPress={() => _handleSingleCheck(idx)}
                        >
                          <Image source={Check} />
                        </TouchableOpacity>
                      )}

                      <Text style={{ marginLeft: 9 }}>선택</Text>
                    </FlexBox>

                    <TouchableOpacity>
                      <Image source={Xdark} />
                    </TouchableOpacity>
                  </FlexBox>

                  <FlexBox
                    dir="row"
                    align="center"
                    justify="space-between"
                    style={{ marginTop: 17 }}
                  >
                    <ContactImg
                      style={{ backgroundColor: "#ededed" }}
                    ></ContactImg>
                    <FlexBox style={{ width: "70%" }}>
                      <FlexBox dir="row" align="center" justify="space-between">
                        <ContactTitle>{el.title}</ContactTitle>
                        <ContactContents
                          style={{ textDecorationLine: "line-through" }}
                        >
                          {el.discount.toLocaleString()}
                        </ContactContents>
                      </FlexBox>
                      <FlexBox dir="row" align="center" justify="space-between">
                        <ContactContents>옵션 : L</ContactContents>
                        <ContactTitle>
                          {el.price.toLocaleString()}원
                        </ContactTitle>
                      </FlexBox>

                      <FlexBox
                        dir="row"
                        align="center"
                        justify="space-between"
                        style={{ marginTop: 2 }}
                      >
                        <CartCountBox
                          style={{
                            display: "flex",
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                          }}
                        >
                          <TouchableOpacity
                            onPress={() =>
                              _handlerCountPlus({ idx: idx, type: "minus" })
                            }
                          >
                            <Image source={CountMinus} />
                          </TouchableOpacity>

                          <CartCount style={{ marginHorizontal: 9 }}>
                            {el.count}
                          </CartCount>
                          <TouchableOpacity
                            onPress={() =>
                              _handlerCountPlus({ idx: idx, type: "plus" })
                            }
                          >
                            <Image source={CountPlus} />
                          </TouchableOpacity>
                        </CartCountBox>

                        <ContactTitle>
                          {(el.price - el.discount).toLocaleString()}원
                        </ContactTitle>
                      </FlexBox>
                    </FlexBox>
                  </FlexBox>
                </Wrapper>
              </>
            );
          })}

          <View
            style={{
              borderBottomColor: "#ededed",
              borderTopColor: "#ededed",
              height: 300,
            }}
          >
            <Wrapper>
              <View
                style={{
                  marginTop: 23.8,
                  borderBottomWidth: 0.5,
                  borderBottomColor: "#575757",
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    setIsOpen(true);
                  }}
                >
                  <CategoryTitle
                    style={{
                      fontSize: 16,
                      marginBottom: 6.2,
                      fontWeight: "bold",
                    }}
                  >
                    장바구니 합계
                  </CategoryTitle>
                </TouchableOpacity>
              </View>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{
                  marginTop: 23.8,
                  borderBottomWidth: 0.5,
                  borderBottomColor: "#575757",
                }}
              >
                <CategoryTitle
                  style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
                >
                  선택 상품 개수
                </CategoryTitle>
                <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                  {allCount}
                </CategoryTitle>
              </FlexBox>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{
                  marginTop: 23.8,
                  borderBottomWidth: 0.5,
                  borderBottomColor: "#575757",
                }}
              >
                <CategoryTitle
                  style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
                >
                  상품 금액
                </CategoryTitle>
                <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                  {allDiscount.toLocaleString()}
                </CategoryTitle>
              </FlexBox>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{
                  marginTop: 23.8,
                  borderBottomWidth: 0.5,
                  borderBottomColor: "#575757",
                }}
              >
                <CategoryTitle
                  style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
                >
                  할인 금액
                </CategoryTitle>
                <CategoryTitle
                  style={{ fontSize: 16, marginBottom: 6.2, color: "#9586f1" }}
                >
                  -{allDiscount.toLocaleString()}원
                </CategoryTitle>
              </FlexBox>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{
                  marginTop: 23.8,
                  borderBottomWidth: 0.5,
                  borderBottomColor: "#575757",
                }}
              >
                <CategoryTitle
                  style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
                >
                  결제 금액
                </CategoryTitle>
                <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                  {allPrice.toLocaleString()}원
                </CategoryTitle>
              </FlexBox>
            </Wrapper>
          </View>
        </ScrollView>
        <Wrapper>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("Payment");
            }}
          >
            <LinearButton
              colors={["#a780ff", "#20ad97", "#20ad97"]}
              start={{ x: 0.1, y: 0.35 }}
              end={{ x: 2.0, y: 0 }}
              style={{ height: 50, alignSelf: "center" }}
            >
              <ChangePasswordButtonText>
                {allPrice.toLocaleString()}원 결제하기
              </ChangePasswordButtonText>
            </LinearButton>
          </TouchableOpacity>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

export default ShoppingBasket;
