import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import { Wrapper, FlexBox, HeaderTitle } from "../../settings/Styled/Global";
import {
  ContactContainer,
  ContactContents,
  ContactImg,
  ContactTitle,
} from "../../settings/Styled/StoreStyled";
import { ProfileBox, ProfileText } from "../../settings/Styled/Profile";
import CustomModal from "../../Components/Modal";
import OrderManagementModal from "../../Components/Modal/Blocks/Store/OrderManagementModal";
import { Back } from "../../Assets";
import { useHeader } from "../../Handler";

const OrderManagement = ({ navigation }) => {
  const [isOpen, setIsOpen] = useState(false);
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>주문관리</HeaderTitle>,
  });

  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        height={"65%"}
        Content={OrderManagementModal}
      />

      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", height: "100%" }}>
          <ContactContainer style={{ marginTop: 10, height: 237 }}>
            <Wrapper style={{ paddingLeft: 28, paddingRight: 28 }}>
              <View style={{ marginTop: 18 }}>
                <FlexBox dir="row" justify="space-between" align="center">
                  <ContactTitle style={{ opacity: 0.35, fontSize: 18 }}>
                    2021.07.01
                  </ContactTitle>
                  <ContactTitle style={{ fontSize: 13 }}>
                    배송상품 2개
                  </ContactTitle>
                </FlexBox>

                <FlexBox
                  dir="row"
                  align="center"
                  justify="space-between"
                  style={{ marginTop: 25 }}
                >
                  <ContactImg
                    style={{ backgroundColor: "#ededed" }}
                  ></ContactImg>
                  <FlexBox style={{ width: "70%" }}>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactTitle>아이템 이름입니다.</ContactTitle>
                      <ContactContents
                        style={{ textDecorationLine: "line-through" }}
                      >
                        59,000
                      </ContactContents>
                    </FlexBox>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactContents>옵션 : L</ContactContents>
                      <ContactTitle>49,000원</ContactTitle>
                    </FlexBox>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactContents>수량:2</ContactContents>
                    </FlexBox>
                    <FlexBox dir="row" align="center" justify="flex-end">
                      <ContactTitle>98,000원</ContactTitle>
                    </FlexBox>
                  </FlexBox>
                </FlexBox>
                <TouchableOpacity
                  onPress={() => {
                    setIsOpen(true);
                  }}
                >
                  <ProfileBox style={{ marginTop: 25 }}>
                    <ProfileText>주문 내역</ProfileText>
                  </ProfileBox>
                </TouchableOpacity>
              </View>
            </Wrapper>
          </ContactContainer>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default OrderManagement;
