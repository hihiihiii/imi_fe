import { DrawerActions } from "@react-navigation/routers";
import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
  Text,
  Image,
} from "react-native";
import { HeaderRight } from "../../Assets";
import CustomModal from "../../Components/Modal";
import StoreModal from "../../Components/Modal/Blocks/Store";
import { useHeader } from "../../Handler";
import {
  LinearView,
  Wrapper,
  StoreCategoryImg,
  FlexBox,
} from "../../settings/Styled/Global";
import {
  InfluenceCircleImg,
  InfluenceCircleName,
  InfluenceBox,
  InfluenceText,
} from "../../settings/Styled/Home";
import { PortContainer } from "../../settings/Styled/Profile";

const Store = ({ navigation }) => {
  const [isOpen, setIsOpen] = useState(false);

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Text
          style={{
            marginLeft: 26,
            fontWeight: "bold",
            fontSize: 24,
            textAlign: "left",
            letterSpacing: -0.39,
            width: 156,
          }}
        >
          {"Store"}
        </Text>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity
        onPress={() => {
          setIsOpen(true);
        }}
      >
        <View style={{ paddingRight: 28 }}>
          <Image source={HeaderRight} />
        </View>
      </TouchableOpacity>
    ),
    headerTitle: () => <Text></Text>,
  });

  const [entries, setEntries] = useState([
    {
      id: 1,
      img: [
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      ],
      name: "hihiihiii",
    },
    {
      id: 1,
      img: [
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      ],
      name: "hihiihiii",
    },
    {
      id: 1,
      img: [
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      ],
      name: "hihiihiii",
    },
    {
      id: 1,
      img: [
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      ],
      name: "hihiihiii",
    },
    {
      id: 1,
      img: [
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      ],
      name: "hihiihiii",
    },
    {
      id: 1,
      img: [
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      ],
      name: "hihiihiii",
    },
  ]);
  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        height={"40%"}
        Content={() => (
          <StoreModal setIsOpen={setIsOpen} navigation={navigation} />
        )}
      />
      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", height: "100%" }}>
          <PortContainer style={{ height: 178 }}>
            <InfluenceBox style={{ marginTop: 24, paddingLeft: 17 }}>
              <InfluenceText>인플루언서 마켓</InfluenceText>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                {entries?.map((el, idx) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate("PageThatOtherUsersLook", {
                          Details: true,
                        });
                      }}
                    >
                      <View style={{ alignItems: "center", marginRight: 15 }}>
                        <LinearView
                          colors={["#a780ff", "#20ad97", "#20ad97"]}
                          start={{ x: 0.1, y: 0.35 }}
                          end={{ x: 2.0, y: 0 }}
                          style={{
                            width: 72,
                            height: 72,
                            padding: 3,
                          }}
                        >
                          <InfluenceCircleImg
                            source={{ uri: el?.img[0] }}
                            borderRadius={999}
                          ></InfluenceCircleImg>
                        </LinearView>

                        <InfluenceCircleName>{el.name}</InfluenceCircleName>
                      </View>
                    </TouchableOpacity>
                  );
                })}
              </ScrollView>
            </InfluenceBox>
          </PortContainer>
          <PortContainer style={{ height: 272 }}>
            <InfluenceBox
              style={{ height: "100%", marginTop: 24, paddingLeft: 17 }}
            >
              <InfluenceText>품목 별 카테고리</InfluenceText>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                {entries?.map((el, idx) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate("CategoryItem");
                      }}
                    >
                      <StoreCategoryImg
                        style={{ marginRight: 19 }}
                        borderRadius={40}
                        source={{ uri: el?.img[0] }}
                      >
                        <FlexBox
                          justify="space-between"
                          style={{
                            marginTop: 107,
                            paddingLeft: 20,
                            height: 52,
                          }}
                        >
                          <Text style={{ color: "white" }}>카드 1번</Text>
                          <Text style={{ color: "white" }}>4.6</Text>
                        </FlexBox>
                      </StoreCategoryImg>
                    </TouchableOpacity>
                  );
                })}
              </ScrollView>
            </InfluenceBox>
          </PortContainer>
          <PortContainer style={{ height: 178 }}>
            <InfluenceBox style={{ marginTop: 24, paddingLeft: 17 }}>
              <TouchableOpacity>
                <InfluenceText>마켓별 카테고리</InfluenceText>
              </TouchableOpacity>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                {entries?.map((el, idx) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate("CategoryMarket");
                      }}
                    >
                      <View style={{ alignItems: "center", marginRight: 15 }}>
                        <LinearView
                          colors={["#a780ff", "#20ad97", "#20ad97"]}
                          start={{ x: 0.1, y: 0.35 }}
                          end={{ x: 2.0, y: 0 }}
                          style={{
                            width: 72,
                            height: 72,
                            padding: 3,
                          }}
                        >
                          <InfluenceCircleImg
                            source={{ uri: el?.img[0] }}
                            borderRadius={999}
                          ></InfluenceCircleImg>
                        </LinearView>

                        <InfluenceCircleName>{el.name}</InfluenceCircleName>
                      </View>
                    </TouchableOpacity>
                  );
                })}
              </ScrollView>
            </InfluenceBox>
          </PortContainer>
          <PortContainer style={{ height: 178 }}>
            <InfluenceBox style={{ marginTop: 24, paddingLeft: 17 }}>
              <InfluenceText>인기 상품</InfluenceText>
            </InfluenceBox>
          </PortContainer>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Store;
