import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
} from "react-native";
import Carousel from "react-native-snap-carousel";
import { CategoryProfileImg } from "../../settings/Styled/Catefory";
import {
  FlexBox,
  HeaderTitle,
  MainBox,
  MainText,
  Wrapper,
} from "../../settings/Styled/Global";

import {
  CategoryTitle,
  ContactSubText,
  ContactTitle,
  ProductImg,
} from "../../settings/Styled/StoreStyled";
import CustomModal from "../../Components/Modal";
import ProductDetailsModal from "../../Components/Modal/Blocks/Store/ProductDetailsModal";
import { useHeader } from "../../Handler";
import { Back } from "../../Assets";

const { width, height } = Dimensions.get("window");

const _renderItem = ({ item, index }) => {
  return (
    <>
      <ProductImg source={{ uri: item?.img }} resizeMode="cover"></ProductImg>
    </>
  );
};

const ProductDetails = ({ navigation, route }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [entries, setEntries] = useState([
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Post malone",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "The Kid LAROI",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Justin Bieber",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Walker Hayes",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Ed Sheeran",
    },
  ]);

  console.log(route?.params?.Details);
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <>
        {route?.params?.Details === false && (
          <>
            <TouchableOpacity
              onPress={() => {
                navigation.replace("ProductModifications");
              }}
            >
              <HeaderTitle
                style={{
                  paddingRight: 16,
                  fontSize: 16,
                  fontWeight: "bold",
                  color: "#818de2",
                }}
              >
                상품수정
              </HeaderTitle>
            </TouchableOpacity>
          </>
        )}
      </>
    ),
    headerTitle: () => <HeaderTitle>상품이름</HeaderTitle>,
  });

  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        height={"50%"}
        Content={() => (
          <ProductDetailsModal
            navigation={navigation}
            setIsOpen={setIsOpen}
          ></ProductDetailsModal>
        )}
      />
      <SafeAreaView>
        <ScrollView style={{ height: "100%" }}>
          <Carousel
            ref={(c) => {
              _carousel = c;
            }}
            data={entries}
            renderItem={_renderItem}
            sliderWidth={width}
            // autoplay={true}
            // lockScrollWhileSnapping={true}
            // enableMomentum={false}
            itemWidth={width * 0.85}
            loop={true}
            layoutCardOffset={18}
          />
          <Wrapper>
            <FlexBox
              dir="row"
              align="center"
              style={{
                marginBottom: 12,
                marginTop: 15,
              }}
            >
              <FlexBox dir="row" align="center">
                <CategoryProfileImg
                  style={{ backgroundColor: "#ededed" }}
                  source={{
                    uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
                  }}
                  resizeMode={"cover"}
                  borderRadius={35}
                ></CategoryProfileImg>
                <CategoryTitle>PostMalone</CategoryTitle>
              </FlexBox>
            </FlexBox>
            <View style={{ width: 212, height: 43 }}>
              <CategoryTitle
                style={{ fontSize: 16, letterSpacing: -0.35, marginLeft: 0 }}
              >
                [아크메드라비] adlv baby face Hoodie black yawn
              </CategoryTitle>
            </View>
            <ContactSubText style={{ marginTop: 7 }}>109,000원</ContactSubText>

            <MainBox
              style={{ backgroundColor: "#798fdc", marginTop: 25 }}
              onPress={() => {
                setIsOpen(true);
              }}
            >
              <MainText style={{ color: "white", textAlign: "center" }}>
                구매하기
              </MainText>
            </MainBox>
          </Wrapper>

          <View style={{ borderTopWidth: 5, borderTopColor: "#ededed" }}>
            <Wrapper>
              <ContactTitle style={{ fontSize: 20, marginTop: 20 }}>
                상세 정보
              </ContactTitle>
            </Wrapper>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default ProductDetails;
