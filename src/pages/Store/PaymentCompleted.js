import React from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Back } from "../../Assets";
import { useHeader } from "../../Handler";
import { MainButtonText } from "../../settings/Styled/Auth";
import {
  Acct_Compo,
  Acct_Text,
  FlexBox,
  HeaderTitle,
  LinearButton,
  Wrapper,
} from "../../settings/Styled/Global";

const PaymentCompleted = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>상품 결제 완료</HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView style={{ flex: 1, backgroundColor: "#ededed" }}>
        <Acct_Compo>
          <Acct_Text>{`결제가 완료되었습니다\n         감사합니다`}</Acct_Text>
        </Acct_Compo>

        <Wrapper>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("PaymentDetails");
            }}
          >
            <LinearButton
              colors={["#a780ff", "#20ad97", "#20ad97"]}
              start={{ x: 0.1, y: 0.35 }}
              end={{ x: 2.0, y: 0 }}
              style={{ alignSelf: "center" }}
            >
              <MainButtonText>결제 내역 확인</MainButtonText>
            </LinearButton>
          </TouchableOpacity>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

export default PaymentCompleted;
