import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import { Back } from "../../Assets";
import { useHeader } from "../../Handler";
import { MainButtonText } from "../../settings/Styled/Auth";
import {
  FlexBox,
  HeaderTitle,
  LinearButton,
  Wrapper,
} from "../../settings/Styled/Global";
import {
  CategoryTitle,
  ContactContents,
  ContactImg,
  ContactTitle,
} from "../../settings/Styled/StoreStyled";

const PaymentDetails = ({ navigation }) => {
  const [data, setData] = useState([{}, {}]);
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>결제 내역</HeaderTitle>,
  });

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ height: "90%" }}>
          <FlexBox
            align="flex-end"
            justify="flex-end"
            style={{ backgroundColor: "#ededed", height: 45 }}
          >
            <Wrapper>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: "bold",
                  textAlign: "right",
                  marginBottom: 5,
                }}
              >
                결제 상품 2개
              </Text>
            </Wrapper>
          </FlexBox>

          {data?.map((el, idx) => {
            return (
              <Wrapper
                style={{
                  borderBottomColor: "#ededed",
                  borderBottomWidth: 5,
                  paddingBottom: 23,
                }}
              >
                <FlexBox
                  dir="row"
                  align="center"
                  justify="space-between"
                  style={{ marginTop: 17 }}
                >
                  <ContactImg
                    style={{ backgroundColor: "#ededed" }}
                  ></ContactImg>

                  <FlexBox style={{ width: "70%" }}>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactTitle>아이템 이름입니다.</ContactTitle>
                      <ContactContents
                        style={{ textDecorationLine: "line-through" }}
                      >
                        77,000원
                      </ContactContents>
                    </FlexBox>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactContents>옵션 : L</ContactContents>
                      <ContactTitle>88,000원</ContactTitle>
                    </FlexBox>
                    <FlexBox dir="row" align="center" justify="space-between">
                      <ContactContents>수량:2</ContactContents>
                    </FlexBox>
                    <FlexBox
                      dir="row"
                      align="center"
                      justify="flex-end"
                      style={{ marginTop: 2 }}
                    >
                      <ContactTitle>10,000원</ContactTitle>
                    </FlexBox>
                  </FlexBox>
                </FlexBox>
              </Wrapper>
            );
          })}

          <Wrapper>
            <View
              style={{
                marginTop: 23.8,
                borderBottomWidth: 0.5,
                borderBottomColor: "#575757",
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  setIsOpen(true);
                }}
              >
                <CategoryTitle
                  style={{
                    fontSize: 16,
                    marginBottom: 6.2,
                    fontWeight: "bold",
                  }}
                >
                  장바구니 합계
                </CategoryTitle>
              </TouchableOpacity>
            </View>
            <FlexBox
              dir="row"
              align="center"
              justify="space-between"
              style={{
                marginTop: 23.8,
                borderBottomWidth: 0.5,
                borderBottomColor: "#575757",
              }}
            >
              <CategoryTitle
                style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
              >
                선택 상품 개수
              </CategoryTitle>
              <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                2개
              </CategoryTitle>
            </FlexBox>
            <FlexBox
              dir="row"
              align="center"
              justify="space-between"
              style={{
                marginTop: 23.8,
                borderBottomWidth: 0.5,
                borderBottomColor: "#575757",
              }}
            >
              <CategoryTitle
                style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
              >
                상품 금액
              </CategoryTitle>
              <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                11,000원
              </CategoryTitle>
            </FlexBox>
            <FlexBox
              dir="row"
              align="center"
              justify="space-between"
              style={{
                marginTop: 23.8,
                borderBottomWidth: 0.5,
                borderBottomColor: "#575757",
              }}
            >
              <CategoryTitle
                style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
              >
                할인 금액
              </CategoryTitle>
              <CategoryTitle
                style={{ fontSize: 16, marginBottom: 6.2, color: "#9586f1" }}
              >
                11,000원
              </CategoryTitle>
            </FlexBox>
            <FlexBox
              dir="row"
              align="center"
              justify="space-between"
              style={{
                marginTop: 23.8,
                borderBottomWidth: 0.5,
                borderBottomColor: "#575757",
              }}
            >
              <CategoryTitle
                style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
              >
                결제 금액
              </CategoryTitle>
              <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                12,000원
              </CategoryTitle>
            </FlexBox>
          </Wrapper>
        </ScrollView>
        <Wrapper>
          <TouchableOpacity
            onPress={() => {
              navigation.replace("Bottom");
            }}
          >
            <LinearButton
              colors={["#a780ff", "#20ad97", "#20ad97"]}
              start={{ x: 0.1, y: 0.35 }}
              end={{ x: 2.0, y: 0 }}
              style={{ alignSelf: "center" }}
            >
              <MainButtonText style={{ fontWeight: "bold" }}>
                홈으로 돌아가기
              </MainButtonText>
            </LinearButton>
          </TouchableOpacity>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

export default PaymentDetails;
