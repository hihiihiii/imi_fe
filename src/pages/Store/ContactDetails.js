import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  Image,
} from "react-native";
import { useHeader } from "../../Handler";
import { Wrapper, FlexBox, HeaderTitle } from "../../settings/Styled/Global";
import {
  ContactContainer,
  ContactImg,
  ContactTitle,
  ContactDate,
  ContactContents,
  ContactTextBox,
  Answer,
} from "../../settings/Styled/StoreStyled";
import { Back, Plus } from "../../Assets";

const ContactDetails = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>문의 내역</HeaderTitle>,
  });

  const [data, setData] = useState([
    {
      title: "제목입니다",
      content: "내용입니다.",
      img: [],
      date: "2021.09.10 12:43",
    },
    {
      title: "제목입니다",
      content: "내용입니다.",
      img: [],
      date: "2021.09.10 12:43",
    },
    {
      title: "제목입니다",
      content: "내용입니다.",
      img: [],
      date: "2021.09.10 12:43",
    },
  ]);

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", height: "100%" }}>
          {data?.map((el, idx) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("OneOneAnswer");
                }}
              >
                <ContactContainer>
                  <Wrapper style={{ marginTop: 7 }}>
                    <FlexBox align="flex-end">
                      <ContactDate>{el.date}</ContactDate>
                    </FlexBox>
                    <FlexBox dir="row">
                      <ContactImg
                        style={{ backgroundColor: "#ededed" }}
                      ></ContactImg>
                      <FlexBox
                        justify="space-around"
                        style={{ marginLeft: 15 }}
                      >
                        <ContactTitle>{el.title}</ContactTitle>
                        <ContactContents>{el.content}</ContactContents>
                      </FlexBox>
                    </FlexBox>
                  </Wrapper>
                </ContactContainer>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default ContactDetails;
