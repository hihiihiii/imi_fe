import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
} from "react-native";
import { Back } from "../../Assets";
import { useHeader } from "../../Handler";
import { Wrapper, FlexBox, HeaderTitle } from "../../settings/Styled/Global";
import {
  ContactContents,
  ContactTitle,
  GoodsImg,
  ContactImgBox,
} from "../../settings/Styled/StoreStyled";

const ProductManagement = ({ navigation }) => {
  const [data, setData] = useState([
    {
      title: "Hollywood’s Bleeding",
      name: "1",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "beerbongs & bentleys",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Sunflower",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Motley crew",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Post malone pink-jacket",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Congratulations",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
  ]);
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("AddProduct");
        }}
      >
        <HeaderTitle
          style={{
            paddingRight: 16,
            fontSize: 16,
            fontWeight: "bold",
            color: "#818de2",
          }}
        >
          상품추가
        </HeaderTitle>
      </TouchableOpacity>
    ),
    headerTitle: () => <HeaderTitle>상품관리</HeaderTitle>,
  });

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", minHeight: "100%" }}>
          <Wrapper>
            <ContactTitle
              style={{ opacity: 0.5, alignSelf: "flex-end", marginTop: 25 }}
            >
              총 21개
            </ContactTitle>

            <ContactImgBox>
              {data?.map((el, idx) => {
                return (
                  <>
                    {/* width 값 주의 */}
                    <FlexBox style={{ width: "47.5%", marginBottom: 20 }}>
                      <TouchableOpacity
                        onPress={() => {
                          navigation.navigate("ProductDetails", {
                            Details: false,
                          });
                        }}
                      >
                        <GoodsImg
                          source={{ uri: el.image }}
                          borderRadius={14}
                        ></GoodsImg>
                      </TouchableOpacity>
                      <ContactTitle>{el.title}</ContactTitle>
                      <ContactContents>{el.price}</ContactContents>
                    </FlexBox>
                  </>
                );
              })}
            </ContactImgBox>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default ProductManagement;
