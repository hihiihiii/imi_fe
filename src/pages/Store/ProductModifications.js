import React, {useState} from 'react';
import {Dimensions, Image, SafeAreaView, ScrollView, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Back} from '../../Assets';
import CustomModal from '../../Components/Modal';
import AddProductModal from '../../Components/Modal/Blocks/Store/AddProductModal';
import {useHeader} from '../../Handler';
import {
  Wrapper,
  FlexBox,
  MainInput,
  MainButton,
  MainBox,
  MainText,
  HeaderTitle,
} from '../../settings/Styled/Global';
import {
  ContactContainer,
  ContactSubText,
  ContactTitle,
  GoodsImg,
} from '../../settings/Styled/StoreStyled';

const {width, height} = Dimensions.get('window');

const ProductModifications = ({navigation}) => {
  const [isOpen, setIsOpen] = useState(false);

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}>
        <HeaderTitle
          style={{
            paddingRight: 16,
            fontSize: 16,
            fontWeight: 'bold',
            color: '#818de2',
          }}>
          완료
        </HeaderTitle>
      </TouchableOpacity>
    ),
    headerTitle: () => <HeaderTitle>상품수정</HeaderTitle>,
  });
  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        height={'55%'}
        Content={AddProductModal}
      />
      <SafeAreaView style={{minHeight: height}}>
        <ScrollView>
          <ContactContainer style={{marginTop: 5, height: 209}}>
            <Wrapper>
              <GoodsImg
                style={{backgroundColor: '#ededed', marginTop: 20}}></GoodsImg>
            </Wrapper>
          </ContactContainer>
          <ContactContainer style={{marginTop: 5, height: 335}}>
            <Wrapper>
              <View style={{marginTop: 21}}>
                <ContactTitle style={{marginBottom: 21}}>
                  상품 정보
                </ContactTitle>

                <ContactSubText style={{marginBottom: 7}}>
                  상품명
                </ContactSubText>
                <MainInput
                  placeholder="상품명을 입력해주세요."
                  placeholderTextColor="#c5c8ce"
                  style={{backgroundColor: '#ededed'}}
                />

                <ContactSubText style={{marginBottom: 7}}>금액</ContactSubText>
                <MainInput
                  placeholder="판매가를 입력해주세요."
                  placeholderTextColor="#c5c8ce"
                  style={{backgroundColor: '#ededed'}}
                />

                <ContactTitle style={{marginBottom: 21}}>옵션</ContactTitle>
                <MainBox
                  style={{backgroundColor: '#798fdc', paddingLeft: 0}}
                  onPress={() => {
                    setIsOpen(true);
                  }}>
                  <MainText style={{color: 'white', textAlign: 'center'}}>
                    옵션추가
                  </MainText>
                </MainBox>
              </View>
            </Wrapper>
          </ContactContainer>
        </ScrollView>

        <Wrapper
          style={{
            position: 'absolute',
            bottom: '4%',
          }}>
          <MainBox
            style={{
              backgroundColor: '#dc7979',
              marginTop: 35,
              paddingLeft: 0,
            }}>
            <MainText style={{color: 'white', textAlign: 'center'}}>
              상품 삭제
            </MainText>
          </MainBox>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

export default ProductModifications;
