import React, { useState } from "react";
import {
  ScrollView,
  Dimensions,
  SafeAreaView,
  View,
  Image,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import Carousel from "react-native-snap-carousel";
import { Back } from "../../Assets";
import { useHeader } from "../../Handler";
import { CategoryProfileImg } from "../../settings/Styled/Catefory";
import {
  ContentsText,
  FlexBox,
  HeaderTitle,
  MainBox,
  MainText,
  Wrapper,
} from "../../settings/Styled/Global";
import {
  CategoryTitle,
  ContactTitle,
  HeartBox,
  ProductImg,
} from "../../settings/Styled/StoreStyled";

const { width, height } = Dimensions.get("window");
const ManagementDetails = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>매니지먼트 캐스팅</HeaderTitle>,
  });
  const [entries, setEntries] = useState([
    {
      id: 1,
      img: "https://w.namu.la/s/ac1c5f4eb532e087d107b7fc6dae56792397c96b3d544df81b9aaf4663b481b8b1360b68de3f92dbd6af7274438a1de43d7ad05ec786b27d6562a7e3e9c4a709555f7d81c12ce79d568e9b0b3f471064c1a032014363fa825cc31d7eb01b14f18053426fca0f42364f26392004b9162f",
      name: "Post malone",
    },
    {
      id: 1,
      img: "https://w.namu.la/s/1f58c24db9a5787698d5e3c3b650a6075342bed3b6c353414187bdf7461f37cbd232e8238c9c7544a0a70dfc1e5d920a6f16a98acd19f1ce195425a2d008dd20d9c846b50fcdb9e0a993ce357dbd822f3d211b214354ba47d8e5f1a82555a789764149e6897506257d604da6405bdb89",
      name: "The Kid LAROI",
    },
    {
      id: 1,
      img: "https://w.namu.la/s/da7681983c275c770e25b51e633205f80484dc42a4fb29a88ae752fcb213b6f0e1748cb3561318aa3ab2bbc03db33e7be5936e1c6bed5e5e93873c33c2f8bf258ecb44b9f812d461ecaf37248d35c6ece5c9eafa69ed47229397f5f0bb3d756272315e4079e8786990dac526fd875b44",
      name: "Justin Bieber",
    },
    {
      id: 1,
      img: "https://w.namu.la/s/1e6bfd66dc767e9960ea0fcb245a1b18bed5df4ccdddc7f04dfbaef4c05df54dc59876d11348cbfaa53c10433a4e9842ea028e012971721e3ddf4b76eef50851751bf898a7d0dfb799a052708f00d3d63dca5d3ab8e52ccf33b290b7e151e84a",
      name: "Walker Hayes",
    },
    {
      id: 1,
      img: "https://w.namu.la/s/12a4ad418f0b16bac12cfe88a0780a8b802cd17b311b6b0131cf8ae5cc67aeb5ca4e8953dbaf72c5c644b1ad969154f5e3e0a021aab73350e3b796fca4d291ab0f0d26ed0c74461c6c5d2d08e1dd5095af9965884c56164ccc879f35870e7240334461ca84c9de1e0e9c31e0fa574b89",
      name: "Ed Sheeran",
    },
  ]);

  const _renderItem = ({ item, index }) => {
    return (
      <>
        <ProductImg source={{ uri: item?.img }} resizeMode="cover"></ProductImg>
      </>
    );
  };
  return (
    <>
      <SafeAreaView>
        <Carousel
          ref={(c) => {
            _carousel = c;
          }}
          data={entries}
          renderItem={_renderItem}
          sliderWidth={width}
          autoplay={true}
          lockScrollWhileSnapping={true}
          enableMomentum={false}
          itemWidth={width * 0.85}
          loop={true}
          layoutCardOffset={18}
        />
        <Wrapper>
          <FlexBox
            dir="row"
            align="center"
            style={{
              marginBottom: 12,
              marginTop: 15,
            }}
          >
            <FlexBox dir="row" align="center">
              <CategoryProfileImg
                style={{ backgroundColor: "#ededed" }}
                source={{
                  uri: "https://w.namu.la/s/ac1c5f4eb532e087d107b7fc6dae56792397c96b3d544df81b9aaf4663b481b8b1360b68de3f92dbd6af7274438a1de43d7ad05ec786b27d6562a7e3e9c4a709555f7d81c12ce79d568e9b0b3f471064c1a032014363fa825cc31d7eb01b14f18053426fca0f42364f26392004b9162f",
                }}
                resizeMode={"cover"}
                borderRadius={35}
              ></CategoryProfileImg>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{ width: "88%" }}
              >
                <CategoryTitle>PostMalone</CategoryTitle>
                <CategoryTitle>2021-08-07까지</CategoryTitle>
              </FlexBox>
            </FlexBox>
          </FlexBox>

          <View style={{ width: 212 }}>
            <CategoryTitle
              style={{
                fontSize: 16,
                letterSpacing: -0.35,
                marginLeft: 0,
              }}
            >
              [드라마 주연배우] SBS,김승환
            </CategoryTitle>
            <CategoryTitle
              style={{
                fontSize: 13,
                letterSpacing: -0.35,
                marginTop: 10,
                marginLeft: 0,
              }}
            >
              109,000원
            </CategoryTitle>
            <ContentsText style={{ marginTop: 15 }}>
              #해쉬태그 #SBS #배우
            </ContentsText>
          </View>
          <MainBox
            style={{
              backgroundColor: "#798fdc",
              marginTop: 20,
              marginBottom: 0,
            }}
            onPress={() => {
              navigation.navigate("CastingRegistration");
            }}
          >
            <MainText style={{ color: "white", textAlign: "center" }}>
              지원하기
            </MainText>
          </MainBox>

          <FlexBox dir="row" align="center" style={{ marginTop: 15 }}>
            <HeartBox style={{ marginRight: 7 }}></HeartBox>
            <ContactTitle>1,200</ContactTitle>
          </FlexBox>
        </Wrapper>
        <View
          style={{
            borderTopWidth: 5,
            borderTopColor: "#ededed",
            marginTop: 34,
          }}
        >
          <Wrapper>
            <ContactTitle style={{ marginTop: 20, fontSize: 20 }}>
              상세 정보
            </ContactTitle>
          </Wrapper>
        </View>
      </SafeAreaView>
    </>
  );
};

export default ManagementDetails;
