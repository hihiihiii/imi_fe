import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Back} from '../../Assets';
import CustomModal from '../../Components/Modal';
import MngmnRgstrModal from '../../Components/Modal/Blocks/Management/MngmnRgstrModal';
import {useHeader} from '../../Handler';
import {SubText} from '../../settings/Styled/Auth';
import {
  LinearView,
  Wrapper,
  FlexBox,
  MainInput,
  MainBox,
  MainText,
  HeaderTitle,
} from '../../settings/Styled/Global';
import {InfluenceCircleImg} from '../../settings/Styled/Home';
import {ContactSubText, ContactTitle} from '../../settings/Styled/StoreStyled';

const MngmnRgstr = ({navigation}) => {
  const [isOpen, setIsOpen] = useState(false);

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{
          width: 50,
          height: 50,
          justifyContent: 'center',
        }}
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>매니지먼트 지원</HeaderTitle>,
  });
  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        height={'50%'}
        Content={MngmnRgstrModal}
      />

      <SafeAreaView>
        <ScrollView>
          <View
            style={{
              borderTopWidth: 5,
              borderTopColor: '#ededed',
              borderBottomColor: '#ededed',
              borderBottomWidth: 5,
            }}>
            <Wrapper>
              <FlexBox
                dir="row"
                align="center"
                style={{marginTop: 20, marginBottom: 20}}>
                <LinearView
                  colors={['#a780ff', '#20ad97', '#20ad97']}
                  start={{x: 0.1, y: 0.35}}
                  end={{x: 2.0, y: 0}}
                  style={{
                    width: 72,
                    height: 72,
                    padding: 3,
                  }}>
                  <InfluenceCircleImg
                    source={{
                      uri: 'https://w.namu.la/s/ac1c5f4eb532e087d107b7fc6dae56792397c96b3d544df81b9aaf4663b481b8b1360b68de3f92dbd6af7274438a1de43d7ad05ec786b27d6562a7e3e9c4a709555f7d81c12ce79d568e9b0b3f471064c1a032014363fa825cc31d7eb01b14f18053426fca0f42364f26392004b9162f',
                    }}
                    resizeMode="cover"
                    borderRadius={72}></InfluenceCircleImg>
                </LinearView>

                <FlexBox style={{marginLeft: 15}}>
                  <FlexBox dir="row" align="center">
                    <ContactTitle style={{marginRight: 15, fontSize: 20}}>
                      Post Malone
                    </ContactTitle>
                    <SubText>팔로우</SubText>
                  </FlexBox>
                  <ContactSubText style={{marginTop: 5}}>
                    266k followers
                  </ContactSubText>
                </FlexBox>
              </FlexBox>
            </Wrapper>
          </View>
          <View
            style={{
              borderBottomColor: '#ededed',
              borderBottomWidth: 5,
              marginTop: 28,
            }}>
            <Wrapper>
              <ContactTitle style={{marginBottom: 21}}>
                지원자 정보
              </ContactTitle>
              <ContactSubText style={{marginBottom: 10}}>이름</ContactSubText>
              <MainInput
                placeholder="이름을 입력하세요"
                placeholderTextColor="#c5c8ce"
                style={{backgroundColor: '#ededed'}}></MainInput>
              <ContactSubText style={{marginBottom: 10}}>이메일</ContactSubText>
              <MainInput
                placeholder="이메일을 입력하세요"
                placeholderTextColor="#c5c8ce"
                style={{backgroundColor: '#ededed'}}></MainInput>
              <ContactSubText style={{marginBottom: 10}}>연락처</ContactSubText>
              <MainInput
                placeholder="연락처를 입력하세요"
                placeholderTextColor="#c5c8ce"
                style={{backgroundColor: '#ededed'}}></MainInput>
            </Wrapper>
          </View>
          <Wrapper>
            <View style={{marginTop: 34}}>
              <ContactTitle>포트폴리오</ContactTitle>
              <MainBox
                style={{
                  backgroundColor: '#798fdc',
                  marginTop: 20,
                  marginBottom: 15,
                }}
                onPress={() => {
                  setIsOpen(true);
                }}>
                <MainText style={{color: 'white', textAlign: 'center'}}>
                  포트폴리오 추가
                </MainText>
              </MainBox>
              <MainBox style={{backgroundColor: '#ededed', paddingRight: 15}}>
                <FlexBox dir="row" align="center" justify="space-between">
                  <MainText>포트폴리오</MainText>
                  <TouchableOpacity>
                    <Text
                      style={{
                        color: '#f77272',
                        fontSize: 12,
                        fontWeight: 'bold',
                      }}>
                      삭제
                    </Text>
                  </TouchableOpacity>
                </FlexBox>
              </MainBox>
            </View>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default MngmnRgstr;
