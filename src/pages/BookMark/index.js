import React from "react";
import { SafeAreaView, ScrollView, View, Text, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Back } from "../../Assets";
import { useHeader } from "../../Handler";
import {
  FlexBox,
  MainBox,
  Wrapper,
  ContentsImg,
  ContentsTitle,
  ContentsText,
  HeaderTitle,
} from "../../settings/Styled/Global";

const BookMark = ({ navigation }) => {
  const data = [
    {
      title: "안녕하세요",
      content: "안녕하세요 반갑습니다.",
      time: "27:30",
    },
    {
      title: "할리스",
      content: "안녕하세요 할리스입니다",
      time: "44:44",
    },
    {
      title: "스타벅스",
      content: "안녕하세요 스타벅스입니다.",
      time: "12:22",
    },
    {
      title: "탐탐입니다.",
      content: "안녕하세요 탐탐입니다",
      time: "44:30",
    },
  ];

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
          <HeaderTitle></HeaderTitle>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>북마크 한 게시글</HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ height: "100%" }}>
          <Wrapper>
            <View style={{ marginTop: 25 }}>
              {data?.map((el, idx) => {
                return (
                  <MainBox>
                    <FlexBox dir="row">
                      <FlexBox dir="row" style={{ width: "20%" }}>
                        <ContentsImg></ContentsImg>
                      </FlexBox>
                      <FlexBox style={{ width: "65%" }}>
                        <ContentsTitle>{el.title}</ContentsTitle>
                        <ContentsText>{el.content}</ContentsText>
                      </FlexBox>
                      <FlexBox style={{ width: "20%" }}>
                        <ContentsText>{el.time}</ContentsText>
                      </FlexBox>
                    </FlexBox>
                  </MainBox>
                );
              })}
            </View>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default BookMark;
