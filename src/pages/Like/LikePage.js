import React from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from "react-native";
import { Back } from "../../Assets";
import { useHeader } from "../../Handler";
import {
  ContentsImg,
  ContentsTitle,
  ContentsText,
  FlexBox,
  MainBox,
  Wrapper,
  HeaderTitle,
} from "../../settings/Styled/Global";

const LikePage = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
          <HeaderTitle></HeaderTitle>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>좋아요한 게시물</HeaderTitle>,
  });
  const data = [
    {
      title: "안녕하세요",
      content: "안녕하세요 반갑습니다.",
      time: "27:30",
    },
    {
      title: "할리스",
      content: "여긴 좋아요 한 게시물 입니다.",
      time: "44:44",
    },
    {
      title: "스타벅스",
      content: "게시물이 이렇습니다.",
      time: "12:22",
    },
    {
      title: "탐탐입니다.",
      content: "다들 행복하게 지내세요.",
      time: "44:30",
    },
    {
      title: "투썸입니다.",
      content: "비가 와서 너무 꾸리꾸리해용",
      time: "30:30",
    },
  ];
  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ height: "100%" }}>
          <Wrapper>
            <View style={{ marginTop: 25 }}>
              {data?.map((el, idx) => {
                return (
                  <MainBox>
                    <FlexBox dir="row">
                      <FlexBox dir="row" style={{ width: "20%" }}>
                        <ContentsImg></ContentsImg>
                      </FlexBox>
                      <FlexBox style={{ width: "65%" }}>
                        <ContentsTitle>{el.title}</ContentsTitle>
                        <ContentsText>{el.content}</ContentsText>
                      </FlexBox>
                      <FlexBox style={{ width: "20%" }}>
                        <ContentsText>{el.time}</ContentsText>
                      </FlexBox>
                    </FlexBox>
                  </MainBox>
                );
              })}
            </View>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default LikePage;
