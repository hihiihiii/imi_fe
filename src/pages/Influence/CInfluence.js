import React, { useState } from "react";
import {
  Text,
  ScrollView,
  SafeAreaView,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { Wrapper, FlexBox } from "../../settings/Styled/Global";
import {
  SilderCountText,
  SilderCountBox,
  LikeCount,
  LikeBox,
  ViewContent,
  Views,
  CategoryImgBox,
  CategoryContainer,
  CategoryProfileText,
  CategoryProfileImg,
} from "../../settings/Styled/Catefory";

import { HartIcon, Plus } from "../../Assets/index";
import { useHeader } from "../../Handler";

const { width, height } = Dimensions.get("window");

const CInfluence = ({ navigation }) => {
  const [data, setData] = useState({
    imgs: [{}, {}, {}, {}],
  });
  const [currIdx, setCurrIdx] = useState(0);
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Text
          style={{
            marginLeft: 26,
            fontWeight: "bold",
            fontSize: 24,
            textAlign: "left",
            letterSpacing: -0.39,
            width: 156,
          }}
        >
          {"인플루언서"}
        </Text>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("Portfolio");
        }}
      >
        <View style={{ paddingRight: 28 }}>
          <Image source={Plus} />
        </View>
      </TouchableOpacity>
    ),
    headerTitle: () => <Text></Text>,
  });
  const handleScroll = ({ nativeEvent }) => {
    // the current offset, {x: number, y: number}
    const position = nativeEvent.contentOffset;
    const index = Math.round(position.x / width);
    if (index !== currIdx) {
      setCurrIdx(index);
    }
  };
  return (
    <>
      <SafeAreaView>
        <ScrollView>
          <Wrapper>
            <CategoryContainer
              onPress={() => {
                navigation.navigate("Detail");
              }}
            >
              <FlexBox
                dir="row"
                align="center"
                style={{ marginLeft: 12, marginBottom: 12, marginTop: 15 }}
              >
                <CategoryProfileImg
                  style={{ marginRight: 17 }}
                ></CategoryProfileImg>
                <FlexBox>
                  <CategoryProfileText bold="bold">
                    PostMalone
                  </CategoryProfileText>
                  <CategoryProfileText opa="0.5" size="10px">
                    08/28
                  </CategoryProfileText>
                </FlexBox>
              </FlexBox>
              <FlexBox
                dir="row"
                style={{ width: "100%", position: "relative" }}
              >
                <View
                  style={{
                    position: "absolute",
                    top: 15,
                    right: 15,
                    zIndex: 9,
                  }}
                >
                  <SilderCountBox>
                    <SilderCountText>
                      {currIdx + 1}/{data.imgs.length}
                    </SilderCountText>
                  </SilderCountBox>
                </View>
                <ScrollView
                  horizontal
                  pagingEnabled
                  showsHorizontalScrollIndicator={false}
                  showsVerticalScrollIndicator={false}
                  onScroll={handleScroll}
                  scrollEventThrottle={16}
                >
                  <CategoryImgBox style={{ width: width * 0.875 }}>
                    <View
                      style={{
                        height: "100%",
                        flexDirection: "column",
                        justifyContent: "space-between",
                      }}
                    >
                      <FlexBox
                        style={{
                          alignItems: "flex-end",
                          marginTop: 20,
                          paddingRight: 20,
                        }}
                      ></FlexBox>
                      <FlexBox dir="row" align="center">
                        <TouchableOpacity>
                          <Image source={HartIcon} />
                        </TouchableOpacity>
                        <LikeBox>
                          <LikeCount>4,558</LikeCount>
                        </LikeBox>
                      </FlexBox>
                    </View>
                  </CategoryImgBox>

                  <CategoryImgBox style={{ width: width * 0.875 }}>
                    <View
                      style={{
                        height: "100%",
                        flexDirection: "column",
                        justifyContent: "space-between",
                      }}
                    >
                      <FlexBox
                        style={{
                          alignItems: "flex-end",
                          marginTop: 20,
                          paddingRight: 20,
                        }}
                      ></FlexBox>
                      <FlexBox dir="row" align="center">
                        <TouchableOpacity>
                          <Image source={HartIcon} />
                        </TouchableOpacity>
                        <LikeBox>
                          <LikeCount>4,558</LikeCount>
                        </LikeBox>
                      </FlexBox>
                    </View>
                  </CategoryImgBox>

                  <CategoryImgBox style={{ width: width * 0.875 }}>
                    <View
                      style={{
                        height: "100%",
                        flexDirection: "column",
                        justifyContent: "space-between",
                      }}
                    >
                      <FlexBox
                        style={{
                          alignItems: "flex-end",
                          marginTop: 20,
                          paddingRight: 20,
                        }}
                      ></FlexBox>
                      <FlexBox dir="row" align="center">
                        <TouchableOpacity>
                          <Image source={HartIcon} />
                        </TouchableOpacity>
                        <LikeBox>
                          <LikeCount>4,558</LikeCount>
                        </LikeBox>
                      </FlexBox>
                    </View>
                  </CategoryImgBox>

                  <CategoryImgBox style={{ width: width * 0.875 }}>
                    <View
                      style={{
                        height: "100%",
                        flexDirection: "column",
                        justifyContent: "space-between",
                      }}
                    >
                      <FlexBox
                        style={{
                          alignItems: "flex-end",
                          marginTop: 20,
                          paddingRight: 20,
                        }}
                      ></FlexBox>
                      <FlexBox dir="row" align="center">
                        <TouchableOpacity>
                          <Image source={HartIcon} />
                        </TouchableOpacity>
                        <LikeBox>
                          <LikeCount>4,558</LikeCount>
                        </LikeBox>
                      </FlexBox>
                    </View>
                  </CategoryImgBox>
                </ScrollView>
              </FlexBox>

              <Views>조회수 25,000</Views>
              <ViewContent>
                SACRIFICE | VIRUS this photomanipulation inspired in the virus
              </ViewContent>
            </CategoryContainer>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default CInfluence;
