import React from "react";
import {
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Text,
  Image,
  View,
  Dimensions,
} from "react-native";
import { Wrapper, InfluenceTitle } from "../../settings/Styled/Global";
import { useHeader } from "../../Handler/index";
import { DrawerActions } from "@react-navigation/native";
import { HeaderRight } from "../../Assets/index";
import { SearchContents, SearchImgBox } from "../../settings/Styled/Search";

const { width, height } = Dimensions.get("window");

const Influence = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <Text
        style={{
          marginLeft: 26,
          fontWeight: "bold",
          fontSize: 24,
          textAlign: "left",
          letterSpacing: -0.39,
          width: 156,
        }}
      >
        인플루언서
      </Text>
    ),
    headerRight: () => (
      <TouchableOpacity
        onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
      >
        <View style={{ paddingRight: 28 }}>
          <Image source={HeaderRight} />
        </View>
      </TouchableOpacity>
    ),
    headerTitle: () => <Text></Text>,
  });
  const data = [
    {
      title: "포토/영상",
      id: 1,
      uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "CPhotoVideo",
    },
    {
      title: "인플루언서",
      id: 2,
      uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "CInfluence",
    },
    {
      title: "헤어/메이크업",
      id: 3,
      uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "CHairMakeup",
    },
    {
      title: "스튜디오",
      id: 4,
      uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "CStudio",
    },
    {
      title: "프로듀서",
      id: 5,
      uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "CProducer",
    },
  ];

  return (
    <>
      <SafeAreaView>
        <ScrollView>
          <Wrapper>
            <SearchImgBox style={{ marginTop: 25 }}>
              {data?.map((el, idx) => {
                return (
                  <TouchableOpacity
                    style={{ marginTop: 15 }}
                    onPress={() => {
                      navigation.navigate(el.name);
                      //navigation.replace("Sponsorship"); // --> Login 후에 replace로 경로아동
                    }}
                  >
                    <SearchContents
                      style={{ width: width * 0.4175, height: width * 0.4175 }}
                      source={{
                        uri: el.uri,
                      }}
                      borderRadius={14}
                      resizeMode="cover"
                    ></SearchContents>
                    <InfluenceTitle>{el.title}</InfluenceTitle>
                  </TouchableOpacity>
                );
              })}
            </SearchImgBox>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Influence;
