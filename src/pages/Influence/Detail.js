import React from "react";
import {
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
} from "react-native";

import {
  CategoryImgBox,
  CategoryProfileImg,
  CategoryProfileText,
  DetailReadingBox,
  DetailTitle,
  DetailContents,
} from "../../settings/Styled/Catefory";
import {
  FlexBox,
  HeaderTitle,
  LinearButton,
  MainText,
  Wrapper,
} from "../../settings/Styled/Global";
import {
  ProfileSub,
  ProfileSubContentBox,
  ProfileSubCount,
} from "../../settings/Styled/Profile";
import { Back, BigHeart } from "../../Assets/index";
import { useHeader } from "../../Handler";

const Detail = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>게시판</HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", height: "100%" }}>
          <Wrapper>
            <View style={{ marginTop: 40 }}>
              <CategoryImgBox></CategoryImgBox>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{
                  marginTop: 18,
                }}
              >
                <FlexBox
                  dir="row"
                  align="center"
                  style={{
                    width: "70%",
                  }}
                >
                  <CategoryProfileImg
                    style={{ marginLeft: 6, marginRight: 12 }}
                  ></CategoryProfileImg>
                  <CategoryProfileText>hihi</CategoryProfileText>
                </FlexBox>
                <TouchableOpacity>
                  <LinearButton
                    colors={["#a780ff", "#20ad97", "#20ad97"]}
                    start={{ x: 0.1, y: 0.35 }}
                    end={{ x: 2.0, y: 0 }}
                    style={{ width: 100, height: 44 }}
                  >
                    <MainText style={{ color: "white" }}>팔로우</MainText>
                  </LinearButton>
                </TouchableOpacity>
              </FlexBox>

              <ProfileSubContentBox style={{ marginTop: 31 }}>
                <TouchableOpacity>
                  <FlexBox>
                    <ProfileSubCount>444</ProfileSubCount>
                    <ProfileSub>조회수</ProfileSub>
                  </FlexBox>
                </TouchableOpacity>
                <TouchableOpacity>
                  <FlexBox>
                    <ProfileSubCount>125</ProfileSubCount>
                    <ProfileSub>좋아요</ProfileSub>
                  </FlexBox>
                </TouchableOpacity>
              </ProfileSubContentBox>

              <DetailReadingBox>
                <DetailTitle>Montserrat</DetailTitle>
                <DetailContents>
                  ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz
                  1234567890!@#$%?&*()
                </DetailContents>
              </DetailReadingBox>
            </View>
          </Wrapper>
        </ScrollView>
        <View
          style={{
            position: "absolute",
            bottom: 0,
            right: 0,
          }}
        >
          <TouchableOpacity>
            <Image source={BigHeart} />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </>
  );
};

export default Detail;
