import React, { useState } from "react";
import { SafeAreaView, ScrollView, View, Image, Text } from "react-native";
import { FlexBox, Wrapper } from "../../settings/Styled/Global";
import styled from "@emotion/native";
import {
  ContactContents,
  ContactImg,
  ContactSubText,
  ContactTitle,
} from "../../settings/Styled/StoreStyled";
import { useHeader } from "../../Handler";
import { TouchableOpacity } from "react-native-gesture-handler";
import { DrawerActions } from "@react-navigation/routers";
import { Back, Filter, Plus } from "../../Assets";
import CustomModal from "../../Components/Modal";
import ClientModal from "../../Components/Modal/Blocks/Client/index";

const SubWrapper = styled.View`
  width: 100%;
  padding: 0px 28px;
`;

const ManagementMain = styled.View`
  margin-top: 25px;
  width: 100%;
  height: 65px;
  border-radius: 10px;
  background-color: #fff;
  align-self: center;
`;

const Client = ({ navigation }) => {
  const [isOpen, setIsOpen] = useState(false);

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
          navigation.dispatch(DrawerActions.closeDrawer());
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
          <Text
            style={{
              fontWeight: "bold",
              fontSize: 24,
              textAlign: "left",
              letterSpacing: -0.39,
              width: 156,
            }}
          >
            {"클라이언트"}
          </Text>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity onPress={() => navigation.navigate("ClientCasting")}>
        <View style={{ paddingRight: 28 }}>
          <Image source={Plus} />
        </View>
      </TouchableOpacity>
    ),
    headerTitle: () => <Text></Text>,
  });

  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        height={"70%"}
        Content={() => <ClientModal setIsOpen={setIsOpen} />}
      />
      <SafeAreaView>
        <ScrollView>
          <View style={{ backgroundColor: "#ededed", height: 115 }}>
            <SubWrapper>
              <ManagementMain>
                <Wrapper style={{ paddingTop: 11 }}>
                  <FlexBox
                    dir="row"
                    justify="space-between"
                    style={{ marginBottom: 2 }}
                  >
                    <ContactTitle style={{ fontSize: 13 }}>
                      나의 현황
                    </ContactTitle>
                    <ContactTitle style={{ fontSize: 11 }}>
                      지원현황 보러가기
                    </ContactTitle>
                  </FlexBox>
                  <ContactTitle style={{ fontSize: 20 }}>0개 지원</ContactTitle>
                </Wrapper>
              </ManagementMain>
            </SubWrapper>
          </View>
          <SubWrapper>
            <View
              style={{
                marginTop: 27,
                borderBottomWidth: 1,
                borderBottomColor: "#ededed",
                height: 32,
              }}
            >
              <ContactTitle style={{ fontSize: 20 }}>
                오디션 / 캐스팅 찾기
              </ContactTitle>
            </View>
            <TouchableOpacity
              onPress={() => {
                setIsOpen(true);
              }}
            >
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{ marginTop: 5 }}
              >
                <ContactSubText>상세 필터</ContactSubText>
                <Image source={Filter} />
              </FlexBox>
            </TouchableOpacity>
            <FlexBox
              dir="row"
              align="center"
              justify="space-between"
              style={{ marginTop: 25 }}
            >
              <TouchableOpacity
                onPress={() => navigation.navigate("ClientDetails")}
              >
                <ContactImg style={{ backgroundColor: "#ededed" }}></ContactImg>
              </TouchableOpacity>
              <FlexBox style={{ width: "70%" }}>
                <FlexBox dir="row" align="center" justify="space-between">
                  <ContactTitle>드라마 주연배우</ContactTitle>
                  <ContactContents>연령무관</ContactContents>
                </FlexBox>
                <FlexBox dir="row" align="center" justify="space-between">
                  <ContactContents>#드라마 #방송 #배우</ContactContents>
                </FlexBox>
                <FlexBox dir="row" align="center" justify="space-between">
                  <ContactContents></ContactContents>
                </FlexBox>
                <FlexBox dir="row" align="center" justify="space-between">
                  <ContactTitle>전국</ContactTitle>
                  <ContactContents>2021/08/01 마감</ContactContents>
                </FlexBox>
              </FlexBox>
            </FlexBox>
          </SubWrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Client;
