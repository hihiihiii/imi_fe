import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {
  MainBox,
  MainInput,
  MainText,
  Wrapper,
  FlexBox,
  LinearView,
  MainCheckBox,
  HeaderTitle,
} from '../../settings/Styled/Global';
import {
  ContactTitle,
  ContactSubText,
  ContactContents,
} from '../../settings/Styled/StoreStyled';
import {Back, BottomChevron, Check} from '../../Assets';
import {
  DetailInfo,
  PortContainer,
  PortImg,
  Strapline,
  SubContent,
} from '../../settings/Styled/Profile';
import {useHeader} from '../../Handler';
import DropDownPicker from 'react-native-dropdown-picker';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

const {width, height} = Dimensions.get('window');

// 멀티 슬라이더 커스텀
const CustomMarker = () => {
  return (
    <>
      <View
        style={{
          width: 17,
          height: 17,
          borderRadius: 999,
          backgroundColor: '#798fdc',
          justifyContent: 'center',
        }}>
        <View
          style={{
            width: 11,
            height: 11,
            backgroundColor: '#fff',
            borderRadius: 11,
            alignSelf: 'center',
          }}></View>
      </View>
    </>
  );
};

const ClientCasting = ({navigation}) => {
  const [data, setData] = useState([
    {
      id: 1,
      check: false,
    },
  ]);

  const [durationIsUndecided, setDurationIsUndecided] = useState([
    {
      id: 1,
      check: false,
    },
  ]);

  // 드롭박스
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    {label: '남자', value: 'male'},
    {label: '여자', value: 'Female'},
  ]);

  const [debriefing, setDebriefing] = useState(false);
  const [debriefingValue, setDebriefingValue] = useState(false);
  const [debriefingItems, setDebriefingItems] = useState([
    {
      label: '하이',
      value: 'hihi',
    },
    {
      label: '하이',
      value: 'hihihihi',
    },
  ]);

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{
          width: 50,
          height: 50,
          justifyContent: 'center',
        }}
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity>
        <HeaderTitle
          style={{
            paddingRight: 16,
            fontSize: 16,
            fontWeight: 'bold',
            color: '#818de2',
          }}>
          완료
        </HeaderTitle>
      </TouchableOpacity>
    ),
    headerTitle: () => <HeaderTitle>클라이언트 캐스팅 등록</HeaderTitle>,
  });

  //체크
  const _handleChecker = idx => {
    // for (let i = 0; i < data.length; i++) {
    //   if (data[i].check) {
    //     data[i].check = false;
    //   }
    // }
    const datas = [...data];
    datas[idx].check = !data[idx].check;
    setData(datas);
  };

  const _handleCheckerDuration = idx => {
    // for (let i = 0; i < data.length; i++) {
    //   if (data[i].check) {
    //     data[i].check = false;
    //   }
    // }
    const datas = [...durationIsUndecided];
    datas[idx].check = !durationIsUndecided[idx].check;
    setDurationIsUndecided(datas);
  };
  return (
    <>
      <SafeAreaView>
        <ScrollView>
          <Wrapper style={{borderTopColor: '#ededed', borderTopWidth: 5}}>
            <View
              style={{
                marginTop: 28,
              }}>
              <ContactTitle>공고 정보</ContactTitle>
              <View style={{marginTop: 20}}>
                <ContactSubText style={{marginBottom: 5}}>
                  공고 제목
                </ContactSubText>
                <MainInput
                  placeholder="공고 제목을 입력해주세요"
                  placeholderTextColor="#c5c8ce"
                  style={{backgroundColor: '#f5f5f5'}}
                />
              </View>

              <View>
                <ContactSubText style={{marginBottom: 5}}>
                  모집 분야
                </ContactSubText>
                <MainInput
                  placeholder="모집 분야을 입력해주세요"
                  placeholderTextColor="#c5c8ce"
                  style={{backgroundColor: '#f5f5f5'}}
                />
              </View>

              <View>
                <ContactSubText style={{marginBottom: 5}}>
                  모집 인원
                </ContactSubText>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="모집 인원을 입력해주세요"
                  style={{backgroundColor: '#f5f5f5'}}
                />
              </View>

              <View>
                <ContactSubText style={{marginBottom: 5}}>성별</ContactSubText>

                <DropDownPicker
                  placeholder="선택해주세요."
                  open={open}
                  value={value}
                  items={items}
                  setOpen={setOpen}
                  setValue={setValue}
                  setItems={setItems}
                  labelStyle={{borderColor: '#ededed'}}
                  textStyle={{fontSize: 15, fontWeight: 'bold'}}
                  dropDownContainerStyle={{
                    backgroundColor: '#f5f5f5',
                    borderWidth: 0,
                  }}
                  min={0}
                  max={5}
                  style={{
                    backgroundColor: '#f5f5f5',
                    borderWidth: 0,
                    marginBottom: 10,
                  }}
                  listMode="SCROLLVIEW"
                />

                <MultiSlider
                  values={[0, 10]}
                  min={1}
                  max={10}
                  isMarkersSeparated={true}
                  customMarkerLeft={CustomMarker}
                  customMarkerRight={CustomMarker}
                  // onValuesChange={(e) => {
                  //   console.log(e);
                  //   setTemp({
                  //     first: e[0],
                  //     last: e[1],
                  //   });
                  // }}
                  snapped={true}
                  smoothSnapped={true}
                  containerStyle={{alignSelf: 'center'}}
                  sliderLength={width - 60}
                />

                {data?.map((el, idx) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        _handleChecker(idx);
                      }}>
                      <FlexBox
                        dir="row"
                        align="center"
                        style={{marginBottom: 30, width: 75, height: 17}}>
                        {el?.check ? (
                          <View style={{width: 18}}>
                            <Image source={Check} />
                          </View>
                        ) : (
                          <LinearView
                            colors={['#a780ff', '#20ad97', '#20ad97']}
                            start={{x: 0.1, y: 0.35}}
                            end={{x: 2.0, y: 0}}
                            style={{
                              width: 18,
                              height: 18,
                            }}>
                            <MainCheckBox
                              style={{width: 16, height: 16}}></MainCheckBox>
                          </LinearView>
                        )}

                        <ContactContents
                          opa
                          style={{color: '#000', marginLeft: 10}}>
                          연령 무관
                        </ContactContents>
                      </FlexBox>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </View>
          </Wrapper>
          <Wrapper style={{borderTopColor: '#ededed', borderTopWidth: 5}}>
            <View
              style={{
                marginTop: 28,
              }}>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{width: '100%'}}>
                <ContactTitle>근무 기간</ContactTitle>

                {durationIsUndecided?.map((el, idx) => {
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        _handleCheckerDuration(idx);
                      }}>
                      <FlexBox dir="row" align="center" style={{width: '100%'}}>
                        {el?.check ? (
                          <View style={{width: 18}}>
                            <Image source={Check} />
                          </View>
                        ) : (
                          <LinearView
                            colors={['#a780ff', '#20ad97', '#20ad97']}
                            start={{x: 0.1, y: 0.35}}
                            end={{x: 2.0, y: 0}}
                            style={{
                              width: 18,
                              height: 18,
                            }}>
                            <MainCheckBox
                              style={{width: 16, height: 16}}></MainCheckBox>
                          </LinearView>
                        )}

                        <ContactContents
                          opa
                          style={{color: '#000', marginLeft: 10}}>
                          기간 미정
                        </ContactContents>
                      </FlexBox>
                    </TouchableOpacity>
                  );
                })}
              </FlexBox>
              <View style={{marginTop: 20}}>
                <ContactSubText style={{marginBottom: 5}}>
                  근무 시작일
                </ContactSubText>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="근무 시작일을 입력해주세요"
                  style={{backgroundColor: '#f5f5f5'}}
                />
              </View>
              <View>
                <ContactSubText style={{marginBottom: 5}}>
                  근무 종료일
                </ContactSubText>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="근무 종료일을 입력해주세요"
                  style={{backgroundColor: '#f5f5f5'}}
                />
              </View>
              <View>
                <ContactSubText style={{marginBottom: 5}}>
                  오디션 모집 시작일
                </ContactSubText>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="오디션 모집 시작일을 입력해주세요"
                  style={{backgroundColor: '#f5f5f5'}}
                />
              </View>
              <View>
                <ContactSubText style={{marginBottom: 5}}>
                  오디션 모집 마갑일
                </ContactSubText>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="오디션 모집 마갑일을 입력해주세요"
                  style={{backgroundColor: '#f5f5f5'}}
                />
              </View>
              <ContactSubText style={{marginBottom: 5}}>
                결과 발표 방식
              </ContactSubText>

              <DropDownPicker
                placeholder="선택해주세요."
                open={debriefing}
                value={debriefingValue}
                items={debriefingItems}
                setOpen={setDebriefing}
                setValue={setDebriefingValue}
                setItems={setDebriefingItems}
                labelStyle={{borderColor: '#ededed'}}
                textStyle={{fontSize: 15, fontWeight: 'bold'}}
                dropDownContainerStyle={{
                  backgroundColor: '#f5f5f5',
                  borderWidth: 0,
                }}
                min={0}
                max={5}
                style={{
                  backgroundColor: '#f5f5f5',
                  borderWidth: 0,
                  marginBottom: 10,
                }}
                listMode="SCROLLVIEW"
              />
              <View>
                <ContactSubText style={{marginBottom: 5}}>
                  페이 입력
                </ContactSubText>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="근무 시작일을 입력해주세요"
                  style={{backgroundColor: '#f5f5f5'}}
                />
              </View>

              <View>
                <ContactSubText style={{marginBottom: 5}}>
                  태그 입력["," 분리 가능]
                </ContactSubText>
                <MainInput
                  placeholderTextColor="#c5c8ce"
                  placeholder="태그를 입력해주세요"
                  style={{backgroundColor: '#f5f5f5'}}
                />
              </View>
            </View>
          </Wrapper>

          <PortContainer
            style={{
              height: 260,
              borderTopWidth: 5,
              borderTopColor: '#ededed',
              borderBottomColor: '#ededed',
              borderBottomWidth: 5,
            }}>
            <Wrapper>
              <Strapline style={{marginBottom: 0}}>
                상세 정보 첨부 기능
              </Strapline>
              <PortImg />
            </Wrapper>
          </PortContainer>

          <PortContainer style={{height: '100%'}}>
            <Wrapper>
              <Strapline>추가 질문 (FAQ) 추가</Strapline>
              <View>
                <SubContent>추가 질문</SubContent>
                <DetailInfo
                  placeholder="상세 내용..."
                  multiline={true}
                  style={{backgroundColor: '#f5f5f5'}}></DetailInfo>
              </View>
            </Wrapper>
          </PortContainer>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default ClientCasting;
