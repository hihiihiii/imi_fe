import React from "react";
import {
  Image,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import {
  FlexBox,
  HeaderTitle,
  MainBox,
  MainText,
  Wrapper,
} from "../../settings/Styled/Global";
import {
  PortContainer,
  PortImg,
  Strapline,
  SubContent,
  DetailInfo,
} from "../../settings/Styled/Profile";
import { BottomChevron } from "../../Assets";
import { useHeader } from "../../Handler";

const Portfolio = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <HeaderTitle>포트폴리오</HeaderTitle>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
        >
          <HeaderTitle
            style={{
              paddingRight: 16,
              fontSize: 16,
              fontWeight: "bold",
              color: "#818de2",
            }}
          >
            완료
          </HeaderTitle>
        </TouchableOpacity>
      </>
    ),
    headerTitle: () => <HeaderTitle></HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", height: "100%" }}>
          <PortContainer style={{ height: 209 }}>
            <Wrapper>
              <PortImg />
            </Wrapper>
          </PortContainer>

          <PortContainer style={{ height: 252 }}>
            <Wrapper>
              <Strapline>필수 입력</Strapline>
              <SubContent>카테고리</SubContent>

              <MainBox
                style={{
                  flexDirection: "row",
                  backgroundColor: "#ededed",
                  justifyContent: "space-between",
                  alignItems: "center",
                  paddingRight: 10,
                }}
              >
                <MainText>카테고리</MainText>
                <Image source={BottomChevron} />
              </MainBox>

              <SubContent>직품 분류</SubContent>
              <MainBox
                style={{
                  flexDirection: "row",
                  backgroundColor: "#ededed",
                  justifyContent: "space-between",
                  alignItems: "center",
                  paddingRight: 10,
                }}
              >
                <MainText>분류</MainText>
                <Image source={BottomChevron} />
              </MainBox>
            </Wrapper>
          </PortContainer>

          <PortContainer style={{ height: 229 }}>
            <Wrapper>
              <Strapline>글 올리기</Strapline>
              <SubContent>상세정보</SubContent>
              <DetailInfo
                placeholder="상세내용..."
                multiline={true}
                style={{ backgroundColor: "#ededed" }}
              ></DetailInfo>
            </Wrapper>
          </PortContainer>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Portfolio;
