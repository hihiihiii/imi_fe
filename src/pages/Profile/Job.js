import React, { useState } from "react";
import { SafeAreaView, ScrollView, View, Text, Image } from "react-native";
import {
  MainBox,
  MainText,
  MainCheckBox,
  Wrapper,
  MainTextCheckBox,
  LinearView,
  FlexBox,
  HeaderTitle,
} from "../../settings/Styled/Global";
import { Back, Check } from "../../Assets";
import { useHeader } from "../../Handler";
import { TouchableOpacity } from "react-native-gesture-handler";

const Job = ({ navigation }) => {
  const [data, setData] = useState([
    {
      job: "연기자",
      value: 0,
      check: false,
    },
    {
      job: "뮤지션",
      value: 0,
      check: false,
    },
    {
      job: "모델",
      value: 0,
      check: false,
    },
    {
      job: "댄서",
      value: 0,
      check: false,
    },
    {
      job: "아나운서",
      value: 0,
      check: false,
    },
    {
      job: "댄서",
      value: 0,
      check: false,
    },
    {
      job: "코미디언",
      value: 0,
      check: false,
    },
    {
      job: "성우",
      value: 0,
      check: false,
    },
    {
      job: "가수 지망생",
      value: 0,
      check: false,
    },
  ]);

  const _handleChecker = (idx) => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].check) {
        data[i].check = false;
      }
    }
    const datas = [...data];
    datas[idx].check = !data[idx].check;
    setData(datas);
  };

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>직업</HeaderTitle>,
  });

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", height: "100%" }}>
          <Wrapper>
            <View style={{ marginTop: 45 }}>
              {data?.map((el, idx) => {
                return (
                  <MainBox
                    style={{ paddingRight: 15 }}
                    onPress={() => {
                      _handleChecker(idx);
                    }}
                    action={el?.check}
                  >
                    <MainTextCheckBox
                      style={{ justifyContent: "space-between" }}
                    >
                      <MainText>{el.job}</MainText>
                      {el?.check ? (
                        <Image source={Check} />
                      ) : (
                        <LinearView
                          colors={["#a780ff", "#20ad97", "#20ad97"]}
                          start={{ x: 0.1, y: 0.35 }}
                          end={{ x: 2.0, y: 0 }}
                          style={{
                            width: 16,
                            height: 16,
                          }}
                        >
                          <MainCheckBox></MainCheckBox>
                        </LinearView>
                      )}
                    </MainTextCheckBox>
                  </MainBox>
                );
              })}
            </View>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Job;
