import React from 'react'
import { SafeAreaView, ScrollView, View } from 'react-native'
import { ContentsImg, ContentsText, ContentsTitle, FlexBox, MainBox, Wrapper } from '../../settings/Styled/Global';

const Alarm = () => {
    const data = [{
        title: "hihi",
        content: "hihihi",
        time: "4:04",
    },{
        title: "hihi",
        content: "hihihi",
        time: "4:04",
    },{
        title: "hihi",
        content: "hihihi",
        time: "4:04",
    },{
        title: "hihi",
        content: "hihihi",
        time: "4:04",
    },{
        title: "hihi",
        content: "hihihi",
        time: "4:04",
    },{
        title: "hihi",
        content: "hihihi",
        time: "4:04",
    },]
    return (
      <>
        <SafeAreaView>
          <ScrollView>
            <Wrapper>
            <View style={{marginTop:25}}>
            {data?.map((el, idx) => {
              return (
                <MainBox>
                <FlexBox dir="row">
                <FlexBox dir="row" style={{width: "20%",}}>
                <ContentsImg></ContentsImg>
                </FlexBox>
                    <FlexBox style={{width:"65%"}}>
                      <ContentsTitle>{el.title}</ContentsTitle>
                      <ContentsText>{el.content}</ContentsText>
                    </FlexBox>
                  <FlexBox style={{width:"20%"}}>
                    <ContentsText >
                      {el.time}
                    </ContentsText>
                  </FlexBox>
                </FlexBox>
              </MainBox>
              );
            })}
          </View>
            </Wrapper>
          </ScrollView>
        </SafeAreaView>
      </>
    );
}

export default Alarm;