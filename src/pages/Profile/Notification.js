import { DrawerActions } from "@react-navigation/routers";
import React from "react";
import { View, SafeAreaView, ScrollView, Image, Text } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Back } from "../../Assets";
import { useHeader } from "../../Handler";

import {
  ContentsImg,
  ContentsText,
  ContentsTitle,
  FlexBox,
  HeaderTitle,
  MainBox,
  Wrapper,
} from "../../settings/Styled/Global";

const Notification = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{
          width: 50,
          height: 50,
          justifyContent: "center",
        }}
        onPress={() => {
          navigation.goBack();
          navigation.dispatch(DrawerActions.closeDrawer());
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>공지사항</HeaderTitle>,
  });

  const data = [
    {
      title: "공지입니다.",
      content: "공지사항입니다.",
      time: "4:14",
    },
    {
      title: "공지입니다.",
      content: "공지사항입니다.",
      time: "4:11",
    },
    {
      title: "공지입니다.",
      content: "공지사항입니다.",
      time: "4:12",
    },
  ];
  return (
    <>
      <SafeAreaView>
        <ScrollView>
          <Wrapper>
            <View style={{ marginTop: 25 }}>
              {data?.map((el, idx) => {
                return (
                  <MainBox>
                    <FlexBox dir="row">
                      <FlexBox dir="row" style={{ width: "20%" }}>
                        <ContentsImg></ContentsImg>
                      </FlexBox>
                      <FlexBox style={{ width: "65%" }}>
                        <ContentsTitle>{el.title}</ContentsTitle>
                        <ContentsText>{el.content}</ContentsText>
                      </FlexBox>
                      <FlexBox style={{ width: "20%" }}>
                        <ContentsText>{el.time}</ContentsText>
                      </FlexBox>
                    </FlexBox>
                  </MainBox>
                );
              })}
            </View>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Notification;
