import React from "react";
import { SafeAreaView, ScrollView, View, Text, Image } from "react-native";
import {
  FlexBox,
  HeaderTitle,
  LinearButton,
  Wrapper,
} from "../../settings/Styled/Global";
import {
  SelfIntroductionBox,
  SelfIntroductionButtonText,
} from "../../settings/Styled/Auth";
import { useHeader } from "../../Handler";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Back } from "../../Assets";

const SelfIntroduction = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
          <HeaderTitle></HeaderTitle>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>자기소개</HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView style={{ backgroundColor: "#ededed" }}>
        <ScrollView>
          <Wrapper>
            <SelfIntroductionBox
              placeholder="자기소개를 입력해주세요..."
              multiline={true}
            />
            <LinearButton
              colors={["#a780ff", "#20ad97", "#20ad97"]}
              start={{ x: 0.1, y: 0.35 }}
              end={{ x: 2.0, y: 0 }}
              style={{ marginTop: 344 }}
            >
              <SelfIntroductionButtonText>완료</SelfIntroductionButtonText>
            </LinearButton>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default SelfIntroduction;
