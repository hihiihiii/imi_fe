import React, {useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import {
  MainBox,
  MainText,
  MainCheckBox,
  MainTextCheckBox,
  Wrapper,
  LinearView,
  MainInput,
  FlexBox,
  HeaderTitle,
} from '../../settings/Styled/Global';

import {Back, Check} from '../../Assets/index';
import {useHeader} from '../../Handler';

const Affiliation = ({navigation}) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{width: 50, height: 50, justifyContent: 'center'}}
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
          <HeaderTitle></HeaderTitle>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>소속</HeaderTitle>,
  });
  const [data, setData] = useState([
    {
      job: '학생',
      value: 1,
      check: false,
    },
    {
      job: '프리랜서',
      value: 2,
      check: false,
    },
    {
      job: '소속 회사',
      value: 3,
      check: false,
    },
  ]);

  const _handleChecker = idx => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].check) {
        data[i].check = false;
      }
    }
    const datas = [...data];
    datas[idx].check = !data[idx].check;
    setData(datas);
  };

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{backgroundColor: '#ededed', height: '100%'}}>
          <Wrapper>
            <View style={{marginTop: 45}}>
              {data?.map((el, idx) => {
                return (
                  <MainBox
                    onPress={() => {
                      _handleChecker(idx);
                    }}
                    action={el?.check}
                    style={{justifyContent: 'center', paddingRight: 15}}>
                    <MainTextCheckBox style={{justifyContent: 'space-between'}}>
                      <MainText>{el.job}</MainText>
                      {el?.check ? (
                        <Image source={Check} />
                      ) : (
                        <LinearView
                          colors={['#a780ff', '#20ad97', '#20ad97']}
                          start={{x: 0.1, y: 0.35}}
                          end={{x: 2.0, y: 0}}
                          style={{
                            width: 16,
                            height: 16,
                          }}>
                          <MainCheckBox></MainCheckBox>
                        </LinearView>
                      )}
                    </MainTextCheckBox>
                  </MainBox>
                );
              })}
            </View>
            {data[2].check === true && (
              <MainInput
                placeholder="소속 회사를 입력해주세요."
                placeholderTextColor="#c5c8ce"
                style={{opacity: 0.7}}></MainInput>
            )}
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Affiliation;
