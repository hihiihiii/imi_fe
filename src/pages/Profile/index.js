import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  ScrollView,
  Image,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { Back, DarkSettings, HeaderRight, NounPlus } from "../../Assets";
import { LinearButton, Wrapper } from "../../settings/Styled/Global";
import {
  BoxShadow,
  HeadIconBox,
  ProfileBox,
  ProfileText,
  Portfolio_Profile,
  ProfileButton,
  PortfolioButton,
  ProfileBoxBox,
  ProfileImg,
  ProfilePic,
  PortfolioLeftImg,
  ProfileContent,
  ProfileContentTextBox,
  ProfileButtonText,
  ProfileName,
  ProfileSubContentBox,
  ProfileSubCount,
  ProfileSub,
  ProfileIntroduction,
  ProfileIntroductionText,
  BtnEff,
} from "../../settings/Styled/Profile";
import { FlexBox, LinearView } from "../../settings/Styled/Global";
import CustomModal from "../../Components/Modal";
import {
  SettingsModal,
  AppSettings,
} from "../../Components/Modal/Blocks/Profile/Settings";

const { width, height } = Dimensions.get("window");

const Profile = ({ navigation, route }) => {
  const [isOpen, setIsOpen] = useState(false); // Modal Toggle 관리
  const [settingOpen, setSettingOpen] = useState(false);
  const [option, setOption] = useState(false);

  const portpolio = [
    {
      id: 1,
      url: {},
      image: "",
    },
    {
      id: 2,
      url: {},
      image: "",
    },
    {
      id: 3,
      url: {},
      image: "",
    },
    {
      id: 3,
      url: {},
      image: "",
    },
  ];

  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        height={"50%"}
        Content={() => (
          <SettingsModal setIsOpen={setIsOpen} navigation={navigation} />
        )}
      />
      <CustomModal
        isOpen={settingOpen}
        setIsOpen={setSettingOpen}
        height={"45%"}
        Content={() => (
          <AppSettings setIsOpen={setSettingOpen} navigation={navigation} />
        )}
      />

      <SafeAreaView>
        <ScrollView>
          <BoxShadow
            style={{
              shadowOffset: {
                width: 0,
                height: 5,
              },
              shadowOpacity: 0.25,
              shadowRadius: 5.5,
              elevation: 9,
            }}
          >
            {route?.params !== undefined && (
              <View
                style={{
                  width: width,
                  height: 50,
                  backgroundColor: "#fff",
                  flexDirection: "row",
                  alignItems: "center",
                  paddingHorizontal: 10,
                }}
              >
                <TouchableOpacity
                  style={{
                    width: 50,
                    height: 50,
                    marginTop: 50,
                  }}
                  onPress={() => navigation.goBack()}
                >
                  <Image source={Back} />
                </TouchableOpacity>
              </View>
            )}
            <HeadIconBox>
              {route.params === undefined && (
                <TouchableOpacity
                  style={{ width: 50, height: 50 }}
                  onPress={() => {
                    setIsOpen(true);
                  }}
                >
                  <Image source={NounPlus} />
                </TouchableOpacity>
              )}
              {route.params === undefined ? (
                <LinearView
                  colors={["#a780ff", "#20ad97", "#20ad97"]}
                  start={{ x: 0.1, y: 0.35 }}
                  end={{ x: 2.0, y: 0 }}
                  style={{ width: 102 }}
                >
                  <TouchableOpacity>
                    <ProfilePic
                      source={{
                        uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
                      }}
                      borderRadius={98}
                    />
                  </TouchableOpacity>
                </LinearView>
              ) : (
                <View
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <LinearView
                    colors={["#a780ff", "#20ad97", "#20ad97"]}
                    start={{ x: 0.1, y: 0.35 }}
                    end={{ x: 2.0, y: 0 }}
                    style={{ width: 102 }}
                  >
                    <TouchableOpacity>
                      <ProfilePic
                        source={{
                          uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
                        }}
                        borderRadius={98}
                      />
                    </TouchableOpacity>
                  </LinearView>
                </View>
              )}
              {route.params === undefined && (
                <TouchableOpacity
                  style={{
                    width: 50,
                    height: 50,
                    alignItems: "flex-end",
                  }}
                  onPress={() => {
                    setSettingOpen(true);
                  }}
                >
                  <Image source={DarkSettings} />
                </TouchableOpacity>
              )}
            </HeadIconBox>
            <ProfileName>hihiihiii</ProfileName>
            <ProfileIntroduction>
              <ProfileIntroductionText>
                🖱Diseño ui/ux y Fotografia 📷 Zihuatanejo, Mexico #LifeStyle
                #Design #Photography #Urban #Art
              </ProfileIntroductionText>
            </ProfileIntroduction>
            <ProfileSubContentBox>
              <TouchableOpacity>
                <FlexBox>
                  <ProfileSubCount>735</ProfileSubCount>
                  <ProfileSub>게시물</ProfileSub>
                </FlexBox>
              </TouchableOpacity>
              <TouchableOpacity>
                <FlexBox>
                  <ProfileSubCount>234</ProfileSubCount>
                  <ProfileSub>팔로워</ProfileSub>
                </FlexBox>
              </TouchableOpacity>
              <TouchableOpacity>
                <FlexBox>
                  <ProfileSubCount>456</ProfileSubCount>
                  <ProfileSub>팔로잉</ProfileSub>
                </FlexBox>
              </TouchableOpacity>
            </ProfileSubContentBox>

            <View
              style={{ alignItems: "center", marginTop: 18, marginBottom: 21 }}
            >
              <TouchableOpacity>
                <LinearButton
                  colors={["#a780ff", "#20ad97", "#20ad97"]}
                  start={{ x: 0.1, y: 0.35 }}
                  end={{ x: 2.0, y: 0 }}
                  style={{ width: 160, height: 38 }}
                >
                  <ProfileButtonText>팔로우</ProfileButtonText>
                </LinearButton>
              </TouchableOpacity>
            </View>
          </BoxShadow>

          <Wrapper>
            {route.params === undefined && (
              <ProfileBoxBox>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("PaymentDetails");
                  }}
                >
                  <ProfileBox>
                    <ProfileText>결제 내역</ProfileText>
                  </ProfileBox>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate("PageThatOtherUsersLook", {
                      Details: false,
                    });
                  }}
                >
                  <ProfileBox>
                    <ProfileText>마이 마켓</ProfileText>
                  </ProfileBox>
                </TouchableOpacity>
              </ProfileBoxBox>
            )}
            <Portfolio_Profile>
              <TouchableOpacity
                onPress={() => {
                  setOption(false);
                }}
              >
                <FlexBox style={{ width: 75, marginRight: 54 }}>
                  <PortfolioButton>포트폴리오</PortfolioButton>
                  <BtnEff display={!option ? "flex" : "none"} />
                </FlexBox>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  setOption(true);
                }}
              >
                <FlexBox style={{ width: 75 }}>
                  <ProfileButton>프로필</ProfileButton>
                  <BtnEff display={!option ? "none" : "flex"} />
                </FlexBox>
              </TouchableOpacity>
            </Portfolio_Profile>

            {!option ? (
              <View style={{ width: "100%" }}>
                <FlexBox dir="row" style={{ flexWrap: "wrap", width: "100%" }}>
                  <View
                    style={{
                      width: "100%",
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <PortfolioLeftImg
                      second={false}
                      style={{ backgroundColor: "#ededed" }}
                    />
                    <FlexBox style={{ width: 142 }}>
                      <PortfolioLeftImg
                        second={true}
                        style={{ backgroundColor: "#ededed" }}
                      />
                      <PortfolioLeftImg
                        second={true}
                        style={{ backgroundColor: "#ededed" }}
                      />
                    </FlexBox>
                  </View>
                </FlexBox>
                <View style={{ width: "100%" }}>
                  <FlexBox
                    dir="row"
                    style={{ flexWrap: "wrap" }}
                    justify="space-between"
                  >
                    {portpolio?.map((el, idx) => {
                      if (idx > 1) {
                        return (
                          <PortfolioLeftImg
                            style={{
                              backgroundColor: "#ededed",
                              width: width * 0.4425,
                              height: width * 0.4425,
                            }}
                          ></PortfolioLeftImg>
                        );
                      }
                    })}
                  </FlexBox>
                </View>
              </View>
            ) : (
              <>
                <ProfileContent>소셜/웹 사이트</ProfileContent>
                <ProfileContentTextBox></ProfileContentTextBox>
                <ProfileContent>자기소개</ProfileContent>
                <ProfileContentTextBox></ProfileContentTextBox>
                <ProfileContent>소속</ProfileContent>
                <ProfileContentTextBox></ProfileContentTextBox>
                <ProfileContent>직업</ProfileContent>
                <ProfileContentTextBox></ProfileContentTextBox>
                <ProfileContent>월렛 주소</ProfileContent>
                <ProfileContentTextBox></ProfileContentTextBox>
                <View style={{ marginBottom: 10 }} />
              </>
            )}
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Profile;
