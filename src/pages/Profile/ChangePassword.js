import React from 'react';
import {
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Back} from '../../Assets';
import {useHeader} from '../../Handler';
import {ChangePasswordButtonText} from '../../settings/Styled/Auth';
import {
  FlexBox,
  HeaderTitle,
  LinearButton,
  MainInput,
  Wrapper,
} from '../../settings/Styled/Global';

const ChangePassword = ({navigation}) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
          <HeaderTitle>비밀번호 변경 </HeaderTitle>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle></HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView>
        <View style={{backgroundColor: '#ededed', height: '100%'}}>
          <ScrollView style={{height: '85%'}}>
            <Wrapper>
              <View style={{marginTop: 45}}>
                <MainInput
                  placeholder="기존 비밀번호를 입력하세요"
                  placeholderTextColor="#c5c8ce"
                />
                <MainInput
                  placeholder="새 비밀번호를 입력하세요"
                  placeholderTextColor="#c5c8ce"
                />
                <MainInput
                  placeholder="새 비밀번호를 재입력하세요"
                  placeholderTextColor="#c5c8ce"
                />
              </View>
            </Wrapper>
          </ScrollView>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Login');
            }}>
            <LinearButton
              colors={['#a780ff', '#20ad97', '#20ad97']}
              start={{x: 0.1, y: 0.35}}
              end={{x: 2.0, y: 0}}
              style={{width: 160, height: 38, alignSelf: 'center'}}>
              <ChangePasswordButtonText>변경하기</ChangePasswordButtonText>
            </LinearButton>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </>
  );
};

export default ChangePassword;
