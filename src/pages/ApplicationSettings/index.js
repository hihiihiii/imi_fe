import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  Switch,
  Image,
} from "react-native";
import {
  MainTextCheckBox,
  MainMarginTop,
  MainCheckBox,
  Wrapper,
  MainBox,
  MainText,
  OnOffBox,
  OnOff,
  HeaderTitle,
  FlexBox,
} from "../../settings/Styled/Global";
import { Back, Chevron } from "../../Assets";
import { useHeader } from "../../Handler";

const ApplicationSettings = ({ navigation }) => {
  const [isSwitchOn, setIsSwitchOn] = useState(false);
  const onToggleSwitch = () => setIsSwitchOn(!isSwitchOn);

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
          <HeaderTitle>어플리케이션 설정</HeaderTitle>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle></HeaderTitle>,
  });

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", height: "100%" }}>
          <Wrapper>
            <MainMarginTop>
              <MainBox
                border
                style={{ justifyContent: "center", paddingRight: 15 }}
              >
                <MainTextCheckBox style={{ justifyContent: "space-between" }}>
                  <MainText>알림 on/off</MainText>
                  <View>
                    <Switch
                      value={isSwitchOn}
                      onValueChange={onToggleSwitch}
                      trackColor={{ true: "#eaebff", false: "#eaebff" }}
                      thumbColor={"#868be5"}
                      style={{ transform: [{ scaleX: 1.2 }, { scaleY: 1.2 }] }}
                    />
                  </View>
                </MainTextCheckBox>
              </MainBox>

              <MainBox
                border
                onPress={() => {
                  navigation.navigate("TermsConditions");
                }}
                style={{ justifyContent: "center", paddingRight: 5 }}
              >
                <MainTextCheckBox style={{ justifyContent: "space-between" }}>
                  <MainText>이용약관</MainText>
                  <Image source={Chevron} />
                </MainTextCheckBox>
              </MainBox>

              <MainBox
                border
                onPress={() => {
                  navigation.navigate("ChangePassword");
                }}
                style={{ justifyContent: "center", paddingRight: 5 }}
              >
                <MainTextCheckBox style={{ justifyContent: "space-between" }}>
                  <MainText>비밀번호 변경</MainText>
                  <Image source={Chevron} />
                </MainTextCheckBox>
              </MainBox>
            </MainMarginTop>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default ApplicationSettings;
