import React, { useState } from "react";
import {
  SafeAreaView,
  Dimensions,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import Carousel from "react-native-snap-carousel";
import { Back } from "../../Assets";
import CustomModal from "../../Components/Modal";
import SponsorModal from "../../Components/Modal/Blocks/Sponsor";
import { useHeader } from "../../Handler";
import Navigation from "../../Navigation/StackNavigation";
import { CategoryProfileImg } from "../../settings/Styled/Catefory";
import {
  FlexBox,
  HeaderTitle,
  MainBox,
  MainText,
  Wrapper,
} from "../../settings/Styled/Global";
import { CategoryTitle, ProductImg } from "../../settings/Styled/StoreStyled";
const { width, height } = Dimensions.get("window");
const SponsorDetails = ({ navigation }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [entries, setEntries] = useState([
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Post malone",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "The Kid LAROI",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Justin Bieber",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Walker Hayes",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Ed Sheeran",
    },
  ]);

  const _renderItem = ({ item, index }) => {
    return (
      <>
        <ProductImg source={{ uri: item?.img }} resizeMode="cover"></ProductImg>
      </>
    );
  };

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>협찬</HeaderTitle>,
  });
  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        height={"35%"}
        Content={SponsorModal}
      />
      <SafeAreaView>
        <ScrollView>
          <Carousel
            ref={(c) => {
              _carousel = c;
            }}
            data={entries}
            renderItem={_renderItem}
            sliderWidth={width}
            // autoplay={true}
            // lockScrollWhileSnapping={true}
            // enableMomentum={false}
            itemWidth={width * 0.85}
            loop={true}
            layoutCardOffset={18}
          />
          <Wrapper>
            <FlexBox
              dir="row"
              align="center"
              style={{
                marginBottom: 12,
                marginTop: 15,
              }}
            >
              <FlexBox dir="row" align="center">
                <CategoryProfileImg
                  style={{ backgroundColor: "#ededed" }}
                  source={{
                    uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
                  }}
                  resizeMode={"cover"}
                  borderRadius={35}
                ></CategoryProfileImg>
                <CategoryTitle>PostMalone</CategoryTitle>
              </FlexBox>
            </FlexBox>
            <View style={{ width: 212, height: 43 }}>
              <CategoryTitle
                style={{
                  fontSize: 16,
                  letterSpacing: -0.35,
                  marginLeft: 0,
                }}
              >
                Aspha 여성 셔츠
              </CategoryTitle>
              <CategoryTitle
                style={{
                  fontSize: 13,
                  letterSpacing: -0.35,
                  marginTop: 10,
                  marginLeft: 0,
                }}
              >
                인스타 공유
              </CategoryTitle>
            </View>

            <MainBox
              style={{ backgroundColor: "#798fdc", marginTop: 25 }}
              onPress={() => {
                navigation.navigate("SponsoredSupport");
              }}
            >
              <MainText style={{ color: "white", textAlign: "center" }}>
                지원하기
              </MainText>
            </MainBox>
          </Wrapper>

          <View
            style={{
              borderTopWidth: 5,
              borderBottomWidth: 5,
              borderBottomColor: "#ededed",
              borderTopColor: "#ededed",
              height: 300,
            }}
          >
            <Wrapper>
              <View
                style={{
                  marginTop: 23.8,
                  borderBottomWidth: 0.5,
                  borderBottomColor: "#575757",
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    setIsOpen(true);
                  }}
                >
                  <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                    협찬 정보
                  </CategoryTitle>
                </TouchableOpacity>
              </View>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{
                  marginTop: 23.8,
                  borderBottomWidth: 0.5,
                  borderBottomColor: "#575757",
                }}
              >
                <CategoryTitle
                  style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
                >
                  협찬 제품
                </CategoryTitle>
                <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                  AsPha 판매중인 상품 중 택1
                </CategoryTitle>
              </FlexBox>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{
                  marginTop: 23.8,
                  borderBottomWidth: 0.5,
                  borderBottomColor: "#575757",
                }}
              >
                <CategoryTitle
                  style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
                >
                  협찬 조건
                </CategoryTitle>
                <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                  합의
                </CategoryTitle>
              </FlexBox>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{
                  marginTop: 23.8,
                  borderBottomWidth: 0.5,
                  borderBottomColor: "#575757",
                }}
              >
                <CategoryTitle
                  style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
                >
                  협찬 협찬 내용
                </CategoryTitle>
                <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                  셔츠를 입고 ~에서 전신 사진
                </CategoryTitle>
              </FlexBox>
              <FlexBox
                dir="row"
                align="center"
                justify="space-between"
                style={{
                  marginTop: 23.8,
                  borderBottomWidth: 0.5,
                  borderBottomColor: "#575757",
                }}
              >
                <CategoryTitle
                  style={{ fontSize: 16, marginBottom: 6.2, opacity: 0.45 }}
                >
                  전화번호
                </CategoryTitle>
                <CategoryTitle style={{ fontSize: 16, marginBottom: 6.2 }}>
                  010-7913-3341
                </CategoryTitle>
              </FlexBox>
            </Wrapper>
          </View>
          <View style={{ marginTop: 25 }}>
            <CategoryTitle
              style={{
                fontSize: 18,
                marginBottom: 15,
                paddingLeft: 16,
                fontWeight: "bold",
              }}
            >
              상세 정보
            </CategoryTitle>
            <ProductImg
              style={{ width: "100%" }}
              source={{
                uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
              }}
            ></ProductImg>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default SponsorDetails;
