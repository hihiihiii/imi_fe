import React, { useState } from "react";
import {
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  Image,
} from "react-native";
import { SubText } from "../../settings/Styled/Auth";
import { Wrapper, FlexBox } from "../../settings/Styled/Global";
import {
  ContactContents,
  ContactTitle,
  GoodsImg,
  ContactImgBox,
} from "../../settings/Styled/StoreStyled";
import { useHeader } from "../../Handler";
import { Back, Plus } from "../../Assets/index";
import { DrawerActions } from "@react-navigation/routers";

const Sponsorship = ({ navigation }) => {
  const [data, setData] = useState([
    {
      title: "Hollywood’s Bleeding",
      name: "1",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "beerbongs & bentleys",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Sunflower",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Motley crew",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Post malone pink-jacket",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
    {
      title: "Congratulations",
      name: "",
      price: "99,000원",
      image:
        "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
    },
  ]);

  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{
          width: 50,
          height: 50,
          justifyContent: "center",
        }}
        onPress={() => {
          navigation.goBack();
          navigation.dispatch(DrawerActions.closeDrawer());
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />

          <Text
            style={{
              fontWeight: "bold",
              fontSize: 24,
              textAlign: "left",
              letterSpacing: -0.39,
              width: 156,
            }}
          >
            {"협찬"}
          </Text>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("SponsoredSupport");
        }}
      >
        <View style={{ paddingRight: 28 }}>
          <SubText style={{ fontSize: 16, fontWeight: "bold" }}>
            협찬 신청
          </SubText>
        </View>
      </TouchableOpacity>
    ),
    headerTitle: () => <Text></Text>,
  });

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ backgroundColor: "#ededed", minHeight: "100%" }}>
          <Wrapper>
            <ContactTitle
              style={{ opacity: 0.5, alignSelf: "flex-end", marginTop: 25 }}
            >
              총 21개
            </ContactTitle>

            <ContactImgBox>
              {data?.map((el, idx) => {
                return (
                  <>
                    {/* width 값 주의 */}
                    <FlexBox style={{ width: "47.5%", marginBottom: 20 }}>
                      <TouchableOpacity
                        onPress={() => {
                          navigation.navigate("SponsorDetails");
                        }}
                      >
                        <GoodsImg
                          source={{ uri: el.image }}
                          borderRadius={14}
                        ></GoodsImg>
                      </TouchableOpacity>
                      <ContactTitle>{el.title}</ContactTitle>
                      <ContactContents>{el.price}</ContactContents>
                    </FlexBox>
                  </>
                );
              })}
            </ContactImgBox>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Sponsorship;
