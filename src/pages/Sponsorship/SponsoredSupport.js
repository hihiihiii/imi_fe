import React, {useState} from 'react';
import {Image, SafeAreaView, ScrollView, TouchableOpacity} from 'react-native';
import {
  FlexBox,
  Wrapper,
  MainInput,
  HeaderTitle,
} from '../../settings/Styled/Global';
import {
  PortContainer,
  PortImg,
  Strapline,
  SubContent,
  DetailInfo,
} from '../../settings/Styled/Profile';
import {Back, BottomChevron} from '../../Assets';
import {useHeader} from '../../Handler';

const SponsoredSupport = ({navigation}) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => (
      <TouchableOpacity
        style={{
          width: 50,
          height: 50,
          justifyContent: 'center',
        }}
        onPress={() => {
          navigation.goBack();
        }}>
        <HeaderTitle
          style={{
            paddingRight: 16,
            fontSize: 16,
            fontWeight: 'bold',
            color: '#818de2',
          }}>
          완료
        </HeaderTitle>
      </TouchableOpacity>
    ),

    headerTitle: () => <HeaderTitle>협찬 지원</HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView>
        <ScrollView style={{backgroundColor: '#ededed'}}>
          <PortContainer style={{height: 209}}>
            <Wrapper>
              <PortImg />
            </Wrapper>
          </PortContainer>

          <PortContainer style={{height: '100%'}}>
            <Wrapper>
              <Strapline>상품 정보</Strapline>
              <SubContent>상품명</SubContent>
              <MainInput
                placeholder="상품명을 입력해주세요."
                placeholderTextColor="#c5c8ce"
                style={{
                  backgroundColor: '#f5f5f5',
                  fontWeight: 'bold',
                  fontSize: 16,
                }}
              />
              <SubContent>금액</SubContent>
              <MainInput
                placeholder="판매가를 입력해주세요."
                placeholderTextColor="#c5c8ce"
                style={{
                  backgroundColor: '#f5f5f5',
                  fontWeight: 'bold',
                  fontSize: 16,
                }}
              />

              <SubContent>상세정보</SubContent>
              <DetailInfo
                placeholder="배송지명을 입력해주세요..."
                placeholderTextColor="#c5c8ce"
                multiline={true}
                style={{backgroundColor: '#f5f5f5'}}></DetailInfo>
            </Wrapper>

            <PortContainer
              style={{
                borderTopColor: '#ededed',
                borderTopWidth: 5,
                height: '100%',
                marginTop: 20,
              }}>
              <Wrapper>
                <Strapline>지원자 정보</Strapline>
                <SubContent>이름</SubContent>
                <MainInput
                  placeholder="이름을 입력해주세요."
                  placeholderTextColor="#c5c8ce"
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
                <SubContent>이메일</SubContent>
                <MainInput
                  placeholder="이메일을 입력해주세요."
                  placeholderTextColor="#c5c8ce"
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
                <SubContent>연락처</SubContent>
                <MainInput
                  placeholder="연락처를 입력해주세요."
                  placeholderTextColor="#c5c8ce"
                  style={{
                    backgroundColor: '#f5f5f5',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}
                />
              </Wrapper>
            </PortContainer>
          </PortContainer>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default SponsoredSupport;
