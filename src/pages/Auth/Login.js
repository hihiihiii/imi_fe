import React from "react";
import {
  Image,
  KeyboardAvoidingView,
  SafeAreaView,
  Text,
  View,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { SplashLogo } from "../../Assets";
import {
  MainButtonText,
  SubText,
  TextFelxEnd,
  BottomText,
  AuthBox,
  AuthInputBox,
  WelcomeBox,
  WelcomeText,
} from "../../settings/Styled/Auth";
import { MainInput, Wrapper, LinearButton } from "../../settings/Styled/Global";

const Login = ({ navigation }) => {
  return (
    <>
      <SafeAreaView>
        <Wrapper>
          <Image
            source={SplashLogo}
            style={{
              alignSelf: "center",
              marginBottom: 45,
              marginTop: 85,
              width: 60,
              height: 60,
            }}
          />
          <AuthInputBox>
            <MainInput
              border
              placeholder="이메일을 입력해주세요"
              placeholderTextColor="#c5c8ce"
            />
            <MainInput
              border
              placeholder="비밀번호를 입력해주세요"
              placeholderTextColor="#c5c8ce"
            />
            <TextFelxEnd>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("ChangePassword");
                }}
              >
                <SubText>비밀번호를 잊으셨나요?</SubText>
              </TouchableOpacity>
            </TextFelxEnd>
          </AuthInputBox>
          <AuthBox>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("Welcome");
              }}
            >
              <LinearButton
                colors={["#a780ff", "#20ad97", "#20ad97"]}
                start={{ x: 0.1, y: 0.35 }}
                end={{ x: 2.0, y: 0 }}
              >
                <MainButtonText>로그인</MainButtonText>
              </LinearButton>
            </TouchableOpacity>

            <BottomText>
              <SubText color="#43454b"> 아직 계정이 없으신가요?</SubText>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate("Register");
                }}
              >
                <SubText>회원가입</SubText>
              </TouchableOpacity>
            </BottomText>
          </AuthBox>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

const Register = ({ navigation }) => {
  return (
    <>
      <SafeAreaView>
        <Wrapper>
          <Image
            source={SplashLogo}
            style={{
              alignSelf: "center",
              marginBottom: 45,
              marginTop: 85,
              width: 60,
              height: 60,
            }}
          />
          <AuthInputBox>
            <MainInput
              border
              placeholder="이름을 입력해주세요"
              placeholderTextColor="#c5c8ce"
            />
            <MainInput
              border
              placeholder="비밀번호를 입력해주세요"
              placeholderTextColor="#c5c8ce"
            />
          </AuthInputBox>
          <AuthBox>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("Welcome");
              }}
            >
              <LinearButton
                colors={["#a780ff", "#20ad97", "#20ad97"]}
                start={{ x: 0.1, y: 0.35 }}
                end={{ x: 2.0, y: 0 }}
              >
                <MainButtonText>회원가입</MainButtonText>
              </LinearButton>
            </TouchableOpacity>
          </AuthBox>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

const Welcome = ({ navigation }) => {
  return (
    <>
      <SafeAreaView>
        <Wrapper>
          <Image
            source={SplashLogo}
            style={{
              alignSelf: "center",
              marginTop: 85,
              width: 60,
              height: 60,
            }}
          />

          <WelcomeBox>
            <WelcomeText>hihiihiii님, IMI에 오신것을 환영합니다.</WelcomeText>
          </WelcomeBox>

          <AuthBox>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("Bottom");
              }}
            >
              <LinearButton
                colors={["#a780ff", "#20ad97", "#20ad97"]}
                start={{ x: 0.1, y: 0.35 }}
                end={{ x: 2.0, y: 0 }}
              >
                <MainButtonText>가입완료</MainButtonText>
              </LinearButton>
            </TouchableOpacity>
          </AuthBox>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

export { Login, Register, Welcome };
