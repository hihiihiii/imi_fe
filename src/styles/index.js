import React, { useState } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Alert,
  Dimensions,
  View,
  Text,
} from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { CartIcon, HomeIcon, LikeIcon, MyIcon, SearchIcon } from "../Assets";
const { width } = Dimensions.get("window");

//icon

 const Tabbar = ({state,descriptors,navigation}) => {
  const route = state.routes.map((route) => descriptors[route.key]);
  const { tabBarVisible } = route[state.index]["options"];
  const insets = useSafeAreaInsets();

  return (
    <View
      style={{
        ...styles.container,
        height: 55 + insets.bottom,
        paddingBottom: insets.bottom,
        display: tabBarVisible ? "flex" : "none",
      }}
    >
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;
        let icon;
        if (route.name === "HOME_STACK") {
          icon = isFocused ? HomeIcon : HomeIcon;
        } else if (route.name === "SEARCH_STACK") {
          icon = isFocused ? SearchIcon : SearchIcon;
        } else if (route.name === "CART_STACK") {
          icon = isFocused ? CartIcon : CartIcon;
        } else if (route.name === "LIKE_STACK") {
          icon = isFocused ? LikeIcon : LikeIcon;
        } else {
          //MY_STACK
          icon = isFocused ? MyIcon : MyIcon;
        }

        const onPress = () => {
          const event = navigation.emit({
            type: "tabPress",
            target: route.key,
            canPreventDefault: true,
          });
          /*   const STACK = route.key.split("-")[0]; */

          /*    if (STACK === "MY_STACK") {
            if (!profile) {
              Alert.alert("IMI", "먼저 로그인하세요", [
                {
                  text: "확인",
                  style: "cancel",
                },
              ]);
              return;
            }
            return;
          } */
          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        return (
          <TouchableOpacity
            key={route.name}
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            onPress={onPress}
            style={{ flex: 1, alignItems: "center" }}
          >
            <Image
              source={icon}
              style={{ width: 25, height: 25, resizeMode: "contain" }}
            />
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 5,
    borderTopWidth: 1,
    borderTopColor: "#eee",
    shadowColor: "#777",
    shadowOffset: {
      width: 4,
      height: 4,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 1,
  },
  modalContainer: {
    bottom: -60,
    position: "absolute",
    width: width,
    marginLeft: -20,
    padding: 20,
  },
  button: {
    padding: 18,
    borderWidth: 1,
    borderColor: "#eee",
    backgroundColor: "#fff",
  },
});

export {Tabbar}