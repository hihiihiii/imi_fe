import Logo from "./files/logo.png";
import SplashLogo from "./files/splashlogo.png";
import SearchImg from "./files/search_bar2x.png";
import NounPlus from "./files/noun_Plus_.png";
import DarkSettings from "./files/dark_settings.png";
import HomeIcon from "./files/home.png";
import CartIcon from "./files/cart.png";
import LikeIcon from "./files/like.png";
import SearchIcon from "./files/search.png";
import MyIcon from "./files/my.png";
import Chevron from "./files/icons_chevron.png";
import HeaderRight from "./files/useHeaderRight.png";
import CancleBox from "./files/CancleBox.png";
import HartIcon from "./files/LikeIcon.png";
import Check from "./files/CheckedIcon.png";
import BigHeart from "./files/bigheart.png";
import BottomChevron from "./files/Bottom_arrow.png";
import Plus from "./files/plus.png";
import HeartBeat from "./files/heatbeat.png";
import Back from "./files/icons-back.png";
import Filter from "./files/icons-darl-filter.png";
import Xdark from "./files/icons-dark-x.png";
import CountMinus from "./files/minus.png";
import CountPlus from "./files/plus-2.png";

export {
  Logo,
  Plus,
  Xdark,
  CountMinus,
  CountPlus,
  BottomChevron,
  SplashLogo,
  SearchImg,
  NounPlus,
  DarkSettings,
  HomeIcon,
  Filter,
  CartIcon,
  LikeIcon,
  SearchIcon,
  MyIcon,
  Chevron,
  HeaderRight,
  HartIcon,
  CancleBox,
  Check,
  Back,
  BigHeart,
  HeartBeat,
};
