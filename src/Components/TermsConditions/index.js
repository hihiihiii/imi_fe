import React from "react";
import {
  SafeAreaView,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
} from "react-native";
import { Back } from "../../Assets";
import { useHeader } from "../../Handler";
import {
  Condi,
  Condi_par,
  Condi_Text,
  Condi_Title,
  FlexBox,
  HeaderTitle,
  Wrapper,
} from "../../settings/Styled/Global";
import { TermsConditionsBackground } from "../../settings/Styled/TermsConditions";

const TermsConditions = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{ width: 50, height: 50, justifyContent: "center" }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <FlexBox dir="row" align="center" style={{ paddingLeft: 16 }}>
          <Image source={Back} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>이용약관</HeaderTitle>,
  });

  return (
    <>
      <ScrollView style={{ backgroundColor: "#f7f7f7" }}>
        <SafeAreaView>
          <TermsConditionsBackground>
            <SafeAreaView>
              <Wrapper style={{ backgroundColor: "#f7f7f7" }}>
                <Condi_Title style={{ marginTop: 37 }}>
                  제 1조 (목적)
                </Condi_Title>
                <Condi>
                  <Condi_par>
                    <Condi_Text>
                      본 약관은 주식회사 INFR International(이하 "회사")이
                      운영하는 웹사이트(이하 "사이트")에서 제공하는 인터넷 관련
                      서비스를 이용함에 있어, 사이트와 회원 간의 이용 조건 및
                      제반 절차, 기타 필요한 사항을 규정함을 목적으로 한다.
                    </Condi_Text>
                  </Condi_par>
                </Condi>
                <Condi_Title style={{ marginTop: 37 }}>
                  제 2조 (용어의 정리)
                </Condi_Title>
                <Condi>
                  <Condi_par>
                    <Condi_Text>
                      1. “사이트”라 함은 회사가 서비스를 “회원”에게 제공하기
                      위하여 컴퓨터 등 정보 통신 설비를 이용하여 설정한 가상의
                      영업장 또는 회사가 운영하는 웹사이트(
                      ***.iam-influencer.com/)를 말한다. 2. “서비스”라 함은
                      회사가 운영하는 사이트를 통해 개인이 등록한 자료를
                      관리하여 기업 정보를 제공하는 서비스, 구직 등의 목적으로
                      등록하는 자료를 DB화하여 각각의 목적에 맞게 분류 가공,
                      집계하여 정보를 제공하는 서비스와 사이트에서 제공하는 모든
                      부대 서비스를 말한다. 3. “회원”이라 함은 서비스를 이용하기
                      위하여 동 약관에 동의하거나 인스타그램 등 연동 된 서비스를
                      통해 “회사”와 이용 계약을 체결한 “개인회원”을 말한다. 4.
                      “아이디”라 함은 회원의 식별과 회원의 서비스 이용을 위하여
                      “회원”이 가입 시 사용한 이메일 주소를 말한다. 5.
                      “비밀번호”라 함은 “회사”의 서비스를 이용하려는 사람이
                      아이디를 부여 받은 자와 동일인임을 확인하고 “회원”의
                      권익을 보호하기 위하여 “회원”이 선정한 문자와 숫자의 조합
                      또는 이와 동일한 용도로 쓰이는 “사이트”에서 자동 생성된
                      인증코드를 말한다. 6. “비회원”이라 함은 “회원”에 가입하지
                      않고 “회사”가 제공하는 서비스를 이용하는 자를 말한다.
                    </Condi_Text>
                  </Condi_par>
                </Condi>
              </Wrapper>
            </SafeAreaView>
          </TermsConditionsBackground>
        </SafeAreaView>
      </ScrollView>
    </>
  );
};

export default TermsConditions;
