import React, { useState } from "react";
import { Image, View } from "react-native";
import CustomModal from "../..";
import { BottomChevron } from "../../../../Assets";
import { SelfIntroductionBox } from "../../../../settings/Styled/Auth";
import {
  FlexBox,
  MainBox,
  MainText,
  ModalTitle,
  Wrapper,
} from "../../../../settings/Styled/Global";

const ProductDetailsModal = ({ navigation, setIsOpen }) => {
  const [type, setType] = useState(false);
  return (
    <>
      {type === false ? (
        <>
          <ModalTitle style={{ marginBottom: 13, height: 32 }}>
            옵션 선택
          </ModalTitle>
          <MainBox
            style={{
              flexDirection: "row",
              backgroundColor: "#f5f5f5",
              justifyContent: "space-between",
              alignItems: "center",
              paddingRight: 10,
            }}
          >
            <MainText>옵션 1</MainText>
            <Image source={BottomChevron} />
          </MainBox>
          <MainBox
            style={{
              flexDirection: "row",
              backgroundColor: "#f5f5f5",
              justifyContent: "space-between",
              alignItems: "center",
              paddingRight: 10,
            }}
          >
            <MainText>옵션 2</MainText>
            <Image source={BottomChevron} />
          </MainBox>

          <MainBox
            style={{
              borderRadius: 99,
              backgroundColor: "#798fdc",
              marginTop: 25,
            }}
          >
            <MainText
              onPress={() => {
                setType(true);
              }}
              style={{ color: "white", textAlign: "center" }}
            >
              추가
            </MainText>
          </MainBox>
        </>
      ) : (
        <ProductDetailOption navigation={navigation} setIsOpen={setIsOpen} />
      )}
    </>
  );
};

const ProductDetailOption = ({ navigation, setIsOpen }) => {
  return (
    <>
      <MainBox
        style={{ backgroundColor: "#fc9d9d", marginTop: 35, paddingLeft: 0 }}
      >
        <MainText style={{ color: "white", textAlign: "center" }}>
          옵션 선택하기
        </MainText>
      </MainBox>
      <SelfIntroductionBox
        multiline={true}
        style={{ backgroundColor: "#f5f5f5", marginTop: 0, height: 125 }}
      />
      <FlexBox
        dir="row"
        style={{ width: "100%", flex: 1 }}
        align="center"
        justify="space-between"
      >
        <MainBox
          onPress={() => {
            navigation.navigate("ShoppingBasket");
            setIsOpen(false)
          }}
          style={{
            width: "49%",
            backgroundColor: "#798fdc",
            marginRight: 7,
            paddingLeft: 0,
          }}
        >
          <MainText style={{ color: "white", alignSelf: "center" }}>
            장바구니
          </MainText>
        </MainBox>
        <MainBox
          onPress={() => {
            navigation.navigate('Payment')
            setIsOpen(false);
          }}
          style={{ width: "49%", backgroundColor: "#798fdc", paddingLeft: 0 }}
        >
          <MainText style={{ color: "white", alignSelf: "center" }}>
            결제하기{" "}
          </MainText>
        </MainBox>
      </FlexBox>
    </>
  );
};

export default ProductDetailsModal;
