import React from "react";
import { Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Chevron } from "../../../../Assets";
import {
  FlexBox,
  ModalMainText,
  Wrapper,
} from "../../../../settings/Styled/Global";

const StoreModal = ({ navigation, setIsOpen }) => {
  return (
    <>
      <Wrapper>
        <FlexBox justify="space-between" style={{ marginTop: 35, height: 156 }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("PageThatOtherUsersLook", {
                Details: false,
              });
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>마이마켓</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("ShoppingBasket");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>장바구니</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("SellerEntry");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>입점 신청하기</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("ContactDetails");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>문의 내역</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
        </FlexBox>
      </Wrapper>
    </>
  );
};

export default StoreModal;
