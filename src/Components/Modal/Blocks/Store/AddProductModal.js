import React from 'react';
import {ScrollView, View} from 'react-native';
import {
  MainBox,
  MainInput,
  MainText,
  ModalMainText,
  ModalTitle,
  Wrapper,
} from '../../../../settings/Styled/Global';

const AddProductModal = () => {
  return (
    <>
      <ScrollView>
        <View style={{height: 32, marginBottom: 22}}>
          <ModalTitle>옵션 추가</ModalTitle>
        </View>
        <View>
          <ModalMainText style={{fontSize: 13, marginBottom: 10}}>
            옵션명
          </ModalMainText>
          <MainInput
            placeholderTextColor="#c5c8ce"
            placeholder="옵션명을 입력해주세요"
            style={{backgroundColor: '#f5f5f5'}}
          />
        </View>
        <View>
          <ModalMainText style={{fontSize: 13, marginBottom: 10}}>
            재고
          </ModalMainText>
          <MainInput
            placeholderTextColor="#c5c8ce"
            placeholder="판매가를 입력해주세요"
            style={{backgroundColor: '#f5f5f5'}}
          />
        </View>

        <View>
          <ModalMainText style={{fontSize: 13, marginBottom: 10}}>
            추가금액
          </ModalMainText>
          <MainInput
            placeholderTextColor="#c5c8ce"
            placeholder="추가금액을 입력해주세요"
            style={{backgroundColor: '#f5f5f5'}}
          />
        </View>
        <MainBox style={{backgroundColor: '#798fdc', marginTop: 25, flex: 1}}>
          <MainText style={{color: 'white', textAlign: 'center'}}>
            옵션추가
          </MainText>
        </MainBox>
      </ScrollView>
    </>
  );
};

export default AddProductModal;
