import React from "react";
import { View, ScrollView } from "react-native";
import { ModalTitle, Wrapper } from "../../../../settings/Styled/Global";
import {
  ContactContents,
  ContactSubText,
} from "../../../../settings/Styled/StoreStyled";

const OrderManagementModal = () => {
  return (
    <>
      <Wrapper>
        <ScrollView>
          <View style={{ marginTop: 38 }}>
            <ModalTitle style={{ marginBottom: 19.6 }}>배송조회</ModalTitle>
            <View
              style={{
                height: 51.5,
                borderBottomColor: "#f5f5f5",
                borderBottomWidth: 1,
                marginTop: 11.2,
              }}
            >
              <ContactContents style={{ marginBottom: 7.4 }}>
                주문자
              </ContactContents>
              <ContactContents opa>hihi / @nutella_bro</ContactContents>
            </View>
            <View
              style={{
                height: 51.5,
                borderBottomColor: "#f5f5f5",
                borderBottomWidth: 1,
                marginTop: 11.2,
              }}
            >
              <ContactContents style={{ marginBottom: 7.4 }}>
                전화번호
              </ContactContents>
              <ContactContents opa>010-7913-3341</ContactContents>
            </View>
            <View
              style={{
                height: 51.5,
                borderBottomColor: "#f5f5f5",
                borderBottomWidth: 1,
                marginTop: 11.2,
              }}
            >
              <ContactContents style={{ marginBottom: 7.4 }}>
                주소
              </ContactContents>
              <ContactContents opa>
                경기도 평택시 상서재로 55 304동 1803호
              </ContactContents>
            </View>
            <View
              style={{
                height: 51.5,
                borderBottomColor: "#f5f5f5",
                borderBottomWidth: 1,
                marginTop: 11.2,
              }}
            >
              <ContactContents style={{ marginBottom: 7.4 }}>
                이메일
              </ContactContents>
              <ContactContents opa>ahrthfla@naver.com</ContactContents>
            </View>
            <View
              style={{
                height: 51.5,
                borderBottomColor: "#f5f5f5",
                borderBottomWidth: 1,
                marginTop: 11.2,
              }}
            >
              <ContactContents style={{ marginBottom: 7.4 }}>
                배송 상태
              </ContactContents>
              <ContactContents opa>배송 완료</ContactContents>
            </View>
          </View>
        </ScrollView>
      </Wrapper>
    </>
  );
};

export default OrderManagementModal;
