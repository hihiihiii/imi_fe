import React from "react";
import { TouchableOpacity, Image } from "react-native";
import { Chevron } from "../../../../Assets";
import {
  FlexBox,
  ModalMainText,
  Wrapper,
} from "../../../../settings/Styled/Global";

const ContactModal = ({ navigation, setIsOpen }) => {
  return (
    <>
      <Wrapper>
        <FlexBox justify="space-between" style={{ marginTop: 35, height: 156 }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("OrderManagement");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>주문 관리</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("ProductManagement", {
                Details: false,
              });
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>상품 관리</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("ContactDetails");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>문의 내역</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("SellerEntry");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>셀러 신청하기</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
        </FlexBox>
      </Wrapper>
    </>
  );
};

export default ContactModal;
