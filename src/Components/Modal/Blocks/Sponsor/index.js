import React from "react";
import {
  FlexBox,
  HeaderTitle,
  MainBox,
  MainText,
} from "../../../../settings/Styled/Global";
import { SubContent } from "../../../../settings/Styled/Profile";

const SponsorModal = () => {
  return (
    <>
      <FlexBox style={{ marginTop: 20, height: 110 }} justify="space-between">
        <HeaderTitle>지원 방법</HeaderTitle>
        <SubContent style={{ fontSize: 15 }}>
          아래 기재된 번호로 연락!
        </SubContent>
        <MainBox style={{ backgroundColor: "#ededed", marginBottom: 0 }}>
          <MainText>010-9668-9636</MainText>
        </MainBox>
      </FlexBox>
    </>
  );
};

export default SponsorModal;
