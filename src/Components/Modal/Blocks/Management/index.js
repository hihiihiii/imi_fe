import React, { useState } from "react";
import { Text, View, Image, Dimensions } from "react-native";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import DropDownPicker from "react-native-dropdown-picker";

import { BottomChevron } from "../../../../Assets";
import styled from "@emotion/native";
import {
  HeaderTitle,
  MainBox,
  MainText,
  FlexBox,
} from "../../../../settings/Styled/Global";

const { width, height } = Dimensions.get("window");

const GenderBox = styled.TouchableOpacity`
  width: 100px;
  height: 45px;
  background-color: #fff;
  border-width: 1px;
  border-radius: 10px;
  border-color: ${(props) => {
    if (props.action) {
      return "#7e8ddf";
    }
    return "#b7b7bb";
  }};
  justify-content: center;
`;

const Gender = styled.Text`
  font-size: 15px;
  font-weight: 600;
  text-align: center;
  color: #050505;
`;

const ManagementModal = () => {
  const [data, setData] = useState([
    { gender: "남성", check: true },
    { gender: "여성", check: false },
  ]);

  const _handleChecker = (idx) => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].check) {
        data[i].check = false;
      }
    }
    const datas = [...data];
    datas[idx].check = !data[idx].check;
    setData(datas);
  };

  // 드롭박스 값
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    { label: "Apple", value: "apple" },
    { label: "Banana", value: "banana" },
  ]);

  //두번째 드롭박스 값
  const [openJob, setOpenJob] = useState(false);
  const [valueJob, setValueJob] = useState(null);
  const [itemsJob, setItemsJob] = useState([
    { label: "Apple", value: "apple" },
    { label: "Banana", value: "banana" },
  ]);

  return (
    <>
      <View style={{ marginTop: 25 }}>
        <HeaderTitle style={{ marginBottom: 12 }}>분류</HeaderTitle>
        <DropDownPicker
          placeholder="선택해주세요."
          open={open}
          value={value}
          items={items}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setItems}
          labelStyle={{ borderColor: "#ededed" }}
          textStyle={{ fontSize: 15, fontWeight: "bold" }}
          dropDownContainerStyle={{
            // 드롭다운 스타일
            backgroundColor: "#f5f5f5",
            borderWidth: 0,
          }}
          min={0}
          max={5}
          style={{
            backgroundColor: "#f5f5f5",
            borderWidth: 0,
            marginBottom: 10,
          }}
        />

        <HeaderTitle style={{ marginBottom: 12 }}>페이</HeaderTitle>
        <MultiSlider
          values={[0, 10]}
          min={0}
          max={10}
          isMarkersSeparated={true}
          // customMarkerLeft={CustomMarker}
          // customMarkerRight={CustomMarker}
          // onValuesChange={(e) => {
          //   console.log(e);
          //   setTemp({
          //     first: e[0],
          //     last: e[1],
          //   });
          // }}
          snapped={true}
          smoothSnapped={true}
          containerStyle={{ alignSelf: "center" }}
          sliderLength={width - 72}
        />

        <HeaderTitle style={{ marginBottom: 12 }}>직업</HeaderTitle>

        <DropDownPicker
          placeholder="선택해주세요."
          open={openJob}
          value={valueJob}
          items={itemsJob}
          setOpen={setOpenJob}
          setValue={setValueJob}
          setItems={setItemsJob}
          labelStyle={{ borderColor: "#ededed" }}
          textStyle={{ fontSize: 15, fontWeight: "bold" }}
          dropDownContainerStyle={{
            // 드롭다운 스타일
            backgroundColor: "#f5f5f5",
            borderWidth: 0,
          }}
          min={0}
          max={5}
          style={{
            backgroundColor: "#f5f5f5",
            borderWidth: 0,
            marginBottom: 10,
          }}
        />

        <HeaderTitle style={{ marginBottom: 12 }}>성별</HeaderTitle>

        <FlexBox dir="row" style={{ width: 176 }}>
          {data?.map((el, idx) => {
            return (
              <>
                <GenderBox
                  onPress={() => {
                    _handleChecker(idx);
                  }}
                  action={el?.check}
                  style={{ marginRight: 10 }}
                >
                  <Gender>{el?.gender}</Gender>
                </GenderBox>
              </>
            );
          })}
        </FlexBox>
        <MainBox
          style={{
            backgroundColor: "#798fdc",
            marginTop: 20,
            marginBottom: 15,
          }}
        >
          <MainText style={{ color: "white", textAlign: "center" }}>
            필터 적용
          </MainText>
        </MainBox>
      </View>
    </>
  );
};

export default ManagementModal;
