import React, { useState } from "react";
import { Text, View, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Check } from "../../../../Assets";
import {
  HeaderTitle,
  MainBox,
  MainText,
  FlexBox,
  LinearView,
  MainCheckBox,
} from "../../../../settings/Styled/Global";
import { ProfileBox, ProfileText } from "../../../../settings/Styled/Profile";

const MngmnRgstrModal = () => {
  const [data, setData] = useState([{ id: 1, check: false }]);

  const _handleChecker = (idx) => {
    // for (let i = 0; i < data.length; i++) {
    //   if (data[i].check) {
    //     data[i].check = false;
    //   }
    // }
    const datas = [...data];
    datas[idx].check = !data[idx].check;
    setData(datas);
  };

  return (
    <>
      <View>
        <HeaderTitle style={{ marginBottom: 20 }}>포트폴리오</HeaderTitle>
        <MainBox style={{ backgroundColor: "#f5f5f5" }}>
          <MainText style={{ opacity: 0.45 }}>포트폴리오 1</MainText>
        </MainBox>

        {data?.map((el, idx) => {
          return (
            <TouchableOpacity
              onPress={() => {
                _handleChecker(idx);
              }}
            >
              <ProfileBox
                style={{ alignItems: "flex-start", paddingHorizontal: 14 }}
              >
                <FlexBox dir="row" align="center" justify="space-between">
                  <MainText style={{ letterSpacing: -0.24 }}>
                    뮤지컬 주인공 (2021.02.02 ~ 2021.02.28)
                  </MainText>
                  {el?.check ? (
                    <Image source={Check} />
                  ) : (
                    <LinearView
                      colors={["#a780ff", "#20ad97", "#20ad97"]}
                      start={{ x: 0.1, y: 0.35 }}
                      end={{ x: 2.0, y: 0 }}
                      style={{
                        width: 16,
                        height: 16,
                      }}
                    >
                      <MainCheckBox></MainCheckBox>
                    </LinearView>
                  )}
                </FlexBox>
              </ProfileBox>
            </TouchableOpacity>
          );
        })}
      </View>
    </>
  );
};

export default MngmnRgstrModal;
