import React from "react";
import styled from "@emotion/native";
import { Text, View, TouchableOpacity, Image } from "react-native";
import {
  ModalMainText,
  Wrapper,
  FlexBox,
} from "../../../../../settings/Styled/Global";
import { Chevron } from "../../../../../Assets";

const SettingsModal = ({ navigation, setIsOpen }) => {
  return (
    <>
      <Wrapper>
        <FlexBox justify="space-between" style={{ marginTop: 35, height: 215 }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("SocialWeb");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>소셜 / 웹 사이트</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("SelfIntroduction");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>자기소개</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("Affiliation");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>소속</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("Job");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>직업</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("Portfolio");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>포트폴리오 등록</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
        </FlexBox>
      </Wrapper>
    </>
  );
};

const AppSettings = ({ navigation, setIsOpen }) => {
  return (
    <>
      <Wrapper>
        <FlexBox justify="space-between" style={{ marginTop: 35, height: 156 }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("ApplicationSettings");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>어플리케이션설정</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("BookMark");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>북마크 한 게시글</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("LikePage");
              setIsOpen(false);
            }}
          >
            <FlexBox dir="row" align="center" justify="space-between">
              <ModalMainText>좋아요 한 게시글</ModalMainText>
              <Image source={Chevron} />
            </FlexBox>
          </TouchableOpacity>
        </FlexBox>
      </Wrapper>
    </>
  );
};

export { SettingsModal, AppSettings };
