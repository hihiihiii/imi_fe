import React, { useState } from "react";
import {
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { BottomChevron } from "../../../../Assets";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import styled from "@emotion/native";
import {
  HeaderTitle,
  MainBox,
  MainText,
  FlexBox,
} from "../../../../settings/Styled/Global";
import DropDownPicker from "react-native-dropdown-picker";

const { width, height } = Dimensions.get("window");

const GenderBox = styled.TouchableOpacity`
  width: 100px;
  height: 45px;
  background-color: #fff;
  border-width: 1px;
  border-radius: 10px;
  border-color: ${(props) => {
    if (props.action) {
      return "#7e8ddf";
    }
    return "#b7b7bb";
  }};
  justify-content: center;
`;

const Gender = styled.Text`
  font-size: 15px;
  font-weight: 600;
  text-align: center;
  color: #050505;
`;

// 슬라이더 thumb 커스텀 제작
const CustomMarker = () => {
  return (
    <>
      <View style={{ width: 20, height: 20, backgroundColor: "#000" }}></View>
    </>
  );
};

const ClientModal = ({ navigation, setIsOpen }) => {
  const [data, setData] = useState([
    { gender: "남성", check: true },
    { gender: "여성", check: false },
  ]);

  // 값
  const [temp, setTemp] = useState({
    first: "",
    last: "",
  });

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items, setItems] = useState([
    { label: "Apple", value: "apple" },
    { label: "Banana", value: "banana" },
  ]);

  const [openJob, setOpenJob] = useState(false);
  const [valueJob, setValueJob] = useState(null);
  const [itemsJob, setItemsJob] = useState([
    { label: "Apple", value: "apple" },
    { label: "Banana", value: "banana" },
    { label: "Apple", value: "apple" },
    { label: "Banana", value: "banana" },
    { label: "Apple", value: "apple" },
    { label: "Banana", value: "banana" },
  ]);

  const _handleChecker = (idx) => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].check) {
        data[i].check = false;
      }
    }
    const datas = [...data];
    datas[idx].check = !data[idx].check;
    setData(datas);
  };
  return (
    <>
      <View style={{ marginTop: 25 }}>
        <HeaderTitle style={{ marginBottom: 12 }}>분류</HeaderTitle>
        <DropDownPicker
          placeholder="선택해주세요."
          open={open}
          value={value}
          items={items}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setItems}
          dropDownContainerStyle={{
            backgroundColor: "#f5f5f5",
            borderWidth: 0,
          }}
          textStyle={{ fontSize: 15, fontWeight: "bold" }}
          min={0}
          max={5}
          style={{
            backgroundColor: "#f5f5f5",
            borderWidth: 0,
            marginBottom: 10,
          }}
        />

        <HeaderTitle style={{ marginBottom: 12 }}>페이</HeaderTitle>

        <MultiSlider
          values={[0, 10]}
          min={0}
          max={10}
          isMarkersSeparated={true}
          // customMarkerLeft={CustomMarker}
          // customMarkerRight={CustomMarker}
          // onValuesChange={(e) => {
          //   console.log(e);
          //   setTemp({
          //     first: e[0],
          //     last: e[1],
          //   });
          // }}
          snapped={true}
          smoothSnapped={true}
          containerStyle={{ alignSelf: "center", marginBottom: 5 }}
          sliderLength={width - 72}
        />

        {/* <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <Text>{temp?.first}</Text>
          <Text>{temp?.last}</Text>
        </View> */}

        <HeaderTitle style={{ marginBottom: 12 }}>직업</HeaderTitle>
        <DropDownPicker
          placeholder="선택해주세요."
          open={openJob}
          value={valueJob}
          items={itemsJob}
          setOpen={setOpenJob}
          setValue={setValueJob}
          setItems={setItemsJob}
          labelStyle={{ borderColor: "#ededed" }}
          textStyle={{ fontSize: 15, fontWeight: "bold" }}
          dropDownContainerStyle={{
            backgroundColor: "#f5f5f5",
            borderWidth: 0,
          }}
          min={0}
          max={5}
          style={{
            backgroundColor: "#f5f5f5",
            borderWidth: 0,
            marginBottom: 10,
          }}
        />

        <HeaderTitle style={{ marginBottom: 12 }}>성별</HeaderTitle>

        <FlexBox dir="row" style={{ width: 176 }}>
          {data?.map((el, idx) => {
            return (
              <>
                <GenderBox
                  onPress={() => {
                    _handleChecker(idx);
                  }}
                  action={el?.check}
                  style={{ marginRight: 10 }}
                >
                  <Gender>{el?.gender}</Gender>
                </GenderBox>
              </>
            );
          })}
        </FlexBox>
        <MainBox
          style={{
            backgroundColor: "#798fdc",
            marginTop: 20,
            marginBottom: 15,
            paddingLeft: 0,
          }}
          onPress={() => {
            setIsOpen(false);
          }}
        >
          <MainText
            style={{
              color: "white",
              textAlign: "center",
            }}
          >
            필터 적용
          </MainText>
        </MainBox>
      </View>
    </>
  );
};

export default ClientModal;
