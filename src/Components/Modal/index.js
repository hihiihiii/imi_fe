import React from "react";
import styled from "@emotion/native";
import Modal from "react-native-modal";
import { Dimensions, Platform } from "react-native";

const { width } = Dimensions.get("window");

const Wrapper = styled.View`
  border-top-left-radius: 14px;
  border-top-right-radius: 14px;
  position: absolute;
  bottom: -12px;
  align-self: center;
  padding: 24px;
  background-color: #fff;
`;

const HandleBar = styled.View`
  width: 65px;
  height: 3px;
  border-radius: 999px;
  background-color: #dbdbdb;
  position: absolute;
  top: 12px;
  align-self: center;
`;

const CustomModal = ({ isOpen, setIsOpen, height, Content }) => {
  const _handleClose = () => {
    setIsOpen(false);
  };

  return (
    <>
      <Modal
        isVisible={isOpen}
        onBackdropPress={() => _handleClose()}
        onBackButtonPress={() => _handleClose()}
        avoidKeyboard={true}
      >
        <Wrapper
          style={{
            width: width,
            height: height,
            bottom: Platform.OS === "android" ? -22 : -22,
          }}
        >
          <HandleBar />
          <Content />
        </Wrapper>
      </Modal>
    </>
  );
};

export default CustomModal;
