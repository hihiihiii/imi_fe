import React, { useEffect } from "react";
import { Image, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { SplashLogo } from "../../Assets";
import { AuthBox, SplashView } from "../../settings/Styled/Auth";
import { Wrapper } from "../../settings/Styled/Global";

const Splash = ({ navigation }) => {
  useEffect(() => {
    /* setInterval(() => {
      navigation.replace("Login");
    }, 1000); */ // setInterval 1초마다 작동

    setTimeout(() => {
      navigation.replace("Login");
    }, 1000);
  }, []);

  return (
    <>
      <SafeAreaView>
        <Wrapper>
          <SplashView>
            <Image source={SplashLogo} />
          </SplashView>
        </Wrapper>
      </SafeAreaView>
    </>
  );
};

export default Splash;
