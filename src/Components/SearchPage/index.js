import React, { useState } from "react";

import {
  Image,
  ImageBackground,
  TouchableOpacity,
  View,
  ScrollView,
  SafeAreaView,
  Dimensions,
} from "react-native";

import { SearchBoxImg_1, SearchImg, SearchBoxImg_2, Back } from "../../Assets";
import { FlexBox, HeaderTitle, Wrapper } from "../../settings/Styled/Global";
import {
  SearchBox,
  SearchText,
  SearchContents,
  SearchImgBox,
  SearchResult,
} from "../../settings/Styled/Search";
import { useHeader } from "../../Handler";
const { width, height } = Dimensions.get("window");
const SearchPage = ({ navigation }) => {
  const [data, setData] = useState([
    {
      id: "1",
      name: "1",
      uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      title: "검색결과.",
    },
    {
      id: "1",
      name: "1",
      uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      title: "검색결과.",
    },
    {
      id: "1",
      name: "1",
      uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      title: "검색결과.",
    },
    {
      id: "1",
      name: "1",
      uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      title: "검색결과.",
    },
  ]);

  useHeader({
    hideBorder: true,
    headerLeft: () => <></>,
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>검색</HeaderTitle>,
  });

  return (
    <>
      <SafeAreaView>
        <ScrollView
          style={{ height: "100%", backgroundColor: "#ededed", paddingTop: 22 }}
        >
          <Wrapper>
            <SearchBox>
              <TouchableOpacity
                style={{ alignSelf: "center", marginRight: 16.4 }}
              >
                <Image source={SearchImg} />
              </TouchableOpacity>
              <SearchText placeholder="검색어를 입력해주세요"></SearchText>
            </SearchBox>
            <SearchResult>검색 결과</SearchResult>
            <SearchImgBox>
              {data?.map((el, idx) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate("ProductDetails", {
                        Detilas: true,
                      });
                    }}
                  >
                    <SearchContents
                      style={{ width: width * 0.4175, height: width * 0.4175 }}
                      source={{
                        uri: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
                      }}
                      borderRadius={14}
                      resizeMode="cover"
                    ></SearchContents>
                  </TouchableOpacity>
                );
              })}
            </SearchImgBox>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default SearchPage;
