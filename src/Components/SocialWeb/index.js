import React from 'react';
import {TouchableOpacity, Image, ScrollView, SafeAreaView} from 'react-native';

import {Back} from '../../Assets';
import {useHeader} from '../../Handler';
import {
  FlexBox,
  HeaderTitle,
  MainInput,
  SocialWebMainBox,
  Wrapper,
} from '../../settings/Styled/Global';

const SocaialWeb = ({navigation}) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <TouchableOpacity
        style={{width: 50, height: 50, justifyContent: 'center'}}
        onPress={() => {
          navigation.goBack();
        }}>
        <FlexBox dir="row" align="center" style={{paddingLeft: 16}}>
          <Image source={Back} />
          <HeaderTitle></HeaderTitle>
        </FlexBox>
      </TouchableOpacity>
    ),
    headerRight: () => <HeaderTitle></HeaderTitle>,
    headerTitle: () => <HeaderTitle>소셜 / 웹 사이트</HeaderTitle>,
  });
  return (
    <>
      <SafeAreaView>
        <ScrollView style={{backgroundColor: '#ededed', height: '100%'}}>
          <Wrapper>
            <SocialWebMainBox>
              <MainInput
                placeholder="페이스북 링크를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="인스타그램 링크를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="트위터 링크를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="유튜브 링크를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="비메오 링크를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="사운드 클라우드 링크를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="홈페이지 링크를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
              <MainInput
                placeholder="월렛 주소를 입력해주세요"
                placeholderTextColor="#c5c8ce"></MainInput>
            </SocialWebMainBox>
          </Wrapper>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default SocaialWeb;
