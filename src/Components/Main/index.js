import React, { useState } from "react";
import {
  ScrollView,
  View,
  Dimensions,
  Text,
  Image,
  SafeAreaView,
} from "react-native";

import {
  HomeScrolView,
  HomeBox,
  HomeBackGround,
  InfluenceBox,
  InfluenceText,
  InfluenceCircleImg,
  InfluenceCircleName,
} from "../../settings/Styled/Home";
import { useHeader } from "../../Handler/index";
import Carousel from "react-native-snap-carousel";
import { TouchableOpacity } from "react-native-gesture-handler";
import { DrawerActions } from "@react-navigation/native";
import { HeaderRight } from "../../Assets/index";
import { FlexBox, LinearView } from "../../settings/Styled/Global";
const { width, height } = Dimensions.get("window");

const _renderItem = ({ item, index }) => {
  return (
    <>
      <TouchableOpacity>
        <HomeBackGround
          style={{
            width: "100%",
            height: 150,
            backgroundColor: "#ededed",
            alignSelf: "center",
            borderRadius: 25,
          }}
          borderRadius={15}
          source={{ uri: item?.img }}
          resizeMode="cover"
        ></HomeBackGround>
      </TouchableOpacity>
    </>
  );
};

const _renderItemBottom = ({ item, index }) => {
  console.log(item);
  return (
    <>
      <TouchableOpacity>
        <HomeBackGround
          style={{
            width: "100%",
            height: 65,
            backgroundColor: "#ededed",
            alignSelf: "center",
            borderRadius: 15,
          }}
          resizeMode="cover"
          borderRadius={15}
          source={{ uri: item?.img }}
        ></HomeBackGround>
      </TouchableOpacity>
    </>
  );
};

const Home = ({ navigation }) => {
  useHeader({
    hideBorder: true,
    headerLeft: () => (
      <Text
        style={{
          marginLeft: 26,
          fontWeight: "bold",
          fontSize: 24,
          textAlign: "left",
          letterSpacing: -0.39,
          width: 156,
        }}
      >
        Home
      </Text>
    ),
    headerRight: () => (
      <TouchableOpacity
        onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
      >
        <FlexBox style={{ paddingRight: 16 }} align="center">
          <Image source={HeaderRight} />
        </FlexBox>
      </TouchableOpacity>
    ),
    headerTitle: () => <Text></Text>,
  });

  const [entries, setEntries] = useState([
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Post malone",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "The Kid LAROI",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Justin Bieber",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Walker Hayes",
    },
    {
      id: 1,
      img: "https://mblogthumb-phinf.pstatic.net/MjAyMDA5MjlfMjMw/MDAxNjAxMzA3MTQ1NDk2.wTWv1qJl3UuIJgSohcmBwjncy1boMVM1_dkrB_dJ6tEg.r1aaWQfLddfJxy5yT-127HN_7oT8morm7Utt46C69kYg.PNG.33ehddks/SE-4614a3b1-9b0c-4b7f-a373-969246e05a18.png?type=w800",
      name: "Ed Sheeran",
    },
  ]);

  return (
    <>
      <SafeAreaView>
        <ScrollView style={{ height: "100%" }}>
          <View style={{ marginTop: 15 }} />
          <Carousel
            ref={(c) => {
              _carousel = c;
            }}
            data={entries}
            renderItem={_renderItem}
            sliderWidth={width}
            autoplay={true}
            lockScrollWhileSnapping={true}
            enableMomentum={false}
            itemWidth={width * 0.85}
            loop={true}
            layoutCardOffset={18}
          />
          <InfluenceBox>
            <InfluenceText>하이엔드 인플루언서</InfluenceText>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              {entries?.map((el, idx) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate("Profile", {
                        Detail: true,
                      });
                    }}
                  >
                    <View style={{ alignItems: "center", marginRight: 15 }}>
                      <LinearView
                        colors={["#a780ff", "#20ad97", "#20ad97"]}
                        start={{ x: 0.1, y: 0.35 }}
                        end={{ x: 2.0, y: 0 }}
                        style={{
                          width: 72,
                          height: 72,
                          padding: 3,
                        }}
                      >
                        <InfluenceCircleImg
                          source={{ uri: el.img }}
                          resizeMode="cover"
                          borderRadius={66}
                        ></InfluenceCircleImg>
                      </LinearView>

                      <InfluenceCircleName>{el.name}</InfluenceCircleName>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </InfluenceBox>
          <Carousel
            ref={(c) => {
              _carousel = c;
            }}
            data={entries}
            autoplay={true}
            lockScrollWhileSnapping={true}
            enableMomentum={false}
            renderItem={_renderItemBottom}
            sliderWidth={width}
            itemWidth={width * 0.85}
            loop={true}
            layoutCardOffset={18}
          />
          <InfluenceBox>
            <InfluenceText>프리미엄 인플루언서</InfluenceText>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              {entries?.map((el, idx) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate("Profile", {
                        Detail: true,
                      });
                    }}
                  >
                    <View style={{ alignItems: "center", marginRight: 15 }}>
                      <LinearView
                        colors={["#a780ff", "#20ad97", "#20ad97"]}
                        start={{ x: 0.1, y: 0.35 }}
                        end={{ x: 2.0, y: 0 }}
                        style={{
                          width: 72,
                          height: 72,
                          padding: 3,
                        }}
                      >
                        <InfluenceCircleImg
                          source={{ uri: el.img }}
                          resizeMode="cover"
                          borderRadius={66}
                        ></InfluenceCircleImg>
                      </LinearView>

                      <InfluenceCircleName>{el.name}</InfluenceCircleName>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </InfluenceBox>
          <InfluenceBox style={{ marginTop: 0 }}>
            <InfluenceText>인기 인플루언서</InfluenceText>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              {entries?.map((el, idx) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate("Profile", {
                        Detail: true,
                      });
                    }}
                  >
                    <View style={{ alignItems: "center", marginRight: 15 }}>
                      <LinearView
                        colors={["#a780ff", "#20ad97", "#20ad97"]}
                        start={{ x: 0.1, y: 0.35 }}
                        end={{ x: 2.0, y: 0 }}
                        style={{
                          width: 72,
                          height: 72,
                          padding: 3,
                        }}
                      >
                        <InfluenceCircleImg
                          source={{ uri: el.img }}
                          resizeMode="cover"
                          borderRadius={66}
                        ></InfluenceCircleImg>
                      </LinearView>

                      <InfluenceCircleName>{el.name}</InfluenceCircleName>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          </InfluenceBox>
          <InfluenceBox style={{ marginTop: 0, width: "100%" }}>
            <InfluenceText>급상승 인플루언서</InfluenceText>
            <ScrollView>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  flexWrap: "wrap",
                  height: "100%",
                  marginTop: -16,
                }}
              >
                {entries?.map((el, idx) => {
                  return (
                    <TouchableOpacity
                      style={{ marginTop: 16 }}
                      onPress={() => {
                        navigation.navigate("Profile", {
                          Detail: true,
                        });
                      }}
                    >
                      <View style={{ alignItems: "center", marginRight: 15 }}>
                        <LinearView
                          colors={["#a780ff", "#20ad97", "#20ad97"]}
                          start={{ x: 0.1, y: 0.35 }}
                          end={{ x: 2.0, y: 0 }}
                          style={{
                            width: 72,
                            height: 72,
                            padding: 3,
                          }}
                        >
                          <InfluenceCircleImg
                            source={{ uri: el.img }}
                            resizeMode="cover"
                            borderRadius={66}
                          ></InfluenceCircleImg>
                        </LinearView>

                        <InfluenceCircleName>{el.name}</InfluenceCircleName>
                      </View>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </ScrollView>
          </InfluenceBox>
          <InfluenceBox style={{ marginTop: 0, width: "100%" }}>
            <InfluenceText>신규 인플루언서</InfluenceText>
            <ScrollView>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  flexWrap: "wrap",
                  height: "100%",
                  marginTop: -16,
                }}
              >
                {entries?.map((el, idx) => {
                  return (
                    <TouchableOpacity
                      style={{ marginTop: 16 }}
                      onPress={() => {
                        navigation.navigate("Profile", {
                          Detail: true,
                        });
                      }}
                    >
                      <View style={{ alignItems: "center", marginRight: 15 }}>
                        <LinearView
                          colors={["#a780ff", "#20ad97", "#20ad97"]}
                          start={{ x: 0.1, y: 0.35 }}
                          end={{ x: 2.0, y: 0 }}
                          style={{
                            width: 72,
                            height: 72,
                            padding: 3,
                          }}
                        >
                          <InfluenceCircleImg
                            source={{ uri: el.img }}
                            resizeMode="cover"
                            borderRadius={66}
                          ></InfluenceCircleImg>
                        </LinearView>

                        <InfluenceCircleName>{el.name}</InfluenceCircleName>
                      </View>
                    </TouchableOpacity>
                  );
                })}
              </View>
            </ScrollView>
          </InfluenceBox>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

export default Home;
