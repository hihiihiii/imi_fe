import { NavigationContainer, DefaultTheme } from "@react-navigation/native";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import Splash from "../../Components/Splash";
import { Login, Register, Welcome } from "../../pages/Auth/Login";
import Home from "../../Components/Main";
import ChangePassword from "../../pages/Profile/ChangePassword";
import React from "react";
import BottomTabNavigator from "../BottomNavigation";
import Profile from "../../pages/Profile";
import Notification from "../../pages/Profile/Notification";
import SponsorDetails from "../../pages/Sponsorship/SponsorDetails";
import SponsoredSupport from "../../pages/Sponsorship/SponsoredSupport";
import Sponsorship from "../../pages/Sponsorship";
import ClientDetails from "../../pages/Client/ClientDetails";
import Management from "../../pages/Management";
import Client from "../../pages/Client";
import MngmnRgstr from "../../pages/Management/MngmnRgstr";
import ManagementDetails from "../../pages/Management/ManagementDetails";
import CastingRegistration from "../../pages/Management/CastingRegistration";
import ClientCasting from "../../pages/Client/ClientCasting";
import ClientSupport from "../../pages/Client/ClientSupport";
import OrderManagement from "../../pages/Store/OrderManagement";
import ProductManagement from "../../pages/Store/ProductManagement";
import ContactDetails from "../../pages/Store/ContactDetails";
import OneOneContact from "../../pages/Store/OneOneContact";
import PaymentDetails from "../../pages/Store/PaymentDetails";
import Store from "../../pages/Store";
import ProductDetails from "../../pages/Store/ProductDetails";
import Portfolio from "../../pages/Profile/Portfolio";
import ShoppingBasket from "../../pages/Store/ShoppingBasket";
import Payment from "../../pages/Store/Payment";
import PaymentCompleted from "../../pages/Store/PaymentCompleted";
import ProductModifications from "../../pages/Store/ProductModifications";
import {
  SellerEntry,
  SellerEntrySecond,
  SellerEntryThird,
} from "../../pages/Store/SellerEntry";

const CustomTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: "#fff",
  },
};

const Stack = createStackNavigator();

const Navigation = () => {
  return (
    <NavigationContainer theme={CustomTheme}>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{
            headerShown: false,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={{
            headerShown: false,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="ProductDetails"
          component={ProductDetails}
          options={{
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 로그인 */}
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            headerShown: false,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 회원가입 */}
        <Stack.Screen
          name="Register"
          component={Register}
          options={{
            headerShown: false,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 환영인사 */}
        <Stack.Screen
          name="Welcome"
          component={Welcome}
          options={{
            headerShown: false,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 비밀번호변경 */}
        <Stack.Screen
          name="ChangePassword"
          component={ChangePassword}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="Notification"
          component={Notification}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 협찬 */}
        <Stack.Screen
          name="Sponsorship"
          component={Sponsorship}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="Portfolio"
          component={Portfolio}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 협찬상세보기 */}
        <Stack.Screen
          name="SponsorDetails"
          component={SponsorDetails}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="Payment"
          component={Payment}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="PaymentCompleted"
          component={PaymentCompleted}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="SellerEntry"
          component={SellerEntry}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="SellerEntrySecond"
          component={SellerEntrySecond}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="SellerEntryThird"
          component={SellerEntryThird}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />

        <Stack.Screen
          name="ContactDetails"
          component={ContactDetails}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />

        <Stack.Screen
          name="ShoppingBasket"
          component={ShoppingBasket}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 협찬지원 */}
        <Stack.Screen
          name="SponsoredSupport"
          component={SponsoredSupport}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 클라이언트 */}
        <Stack.Screen
          name="Client"
          component={Client}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 클라이언트 상세보기 */}
        <Stack.Screen
          name="ClientDetails"
          component={ClientDetails}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 매니지먼트 */}
        <Stack.Screen
          name="Management"
          component={Management}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 매니지먼트 지원 */}
        <Stack.Screen
          name="MngmnRgstr"
          component={MngmnRgstr}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 매니지먼트 지원 */}
        <Stack.Screen
          name="ManagementDetails"
          component={ManagementDetails}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 매니지먼트 캐스팅 등록 */}
        <Stack.Screen
          name="CastingRegistration"
          component={CastingRegistration}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 클라이언트캐스팅 */}
        <Stack.Screen
          name="ClientCasting"
          component={ClientCasting}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 클라이언트지원  ClientSupport */}
        <Stack.Screen
          name="ClientSupport"
          component={ClientSupport}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="Bottom"
          component={BottomTabNavigator}
          options={{
            headerShown: false,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="OrderManagement"
          component={OrderManagement}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="ProductManagement"
          component={ProductManagement}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        <Stack.Screen
          name="OneOneContact"
          component={OneOneContact}
          options={{
            headerShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* 결제 내역 */}

        <Stack.Screen
          name="ProductModifications"
          component={ProductModifications}
          options={{
            herderShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        ></Stack.Screen>

        <Stack.Screen
          name="PaymentDetails"
          component={PaymentDetails}
          options={{
            herderShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        ></Stack.Screen>
        {/* 결제 내역 */}
        <Stack.Screen
          name="Store"
          component={Store}
          options={{
            herderShown: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        ></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
