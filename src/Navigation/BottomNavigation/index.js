import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { getFocusedRouteNameFromRoute } from "@react-navigation/native";
import Cart from "./Cart";
import Home from "./Home";
import Like from "./Like";
import My from "./My";
import Search from "./Search";
import { Tabbar } from "../../styles";
import { View } from "react-native";
import React from "react";
import DrawerNavi from "../Drawer";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Navigation from "../StackNavigation";

const getTabBarVisibility = (route, hideList) => {
  const routeName = getFocusedRouteNameFromRoute(route);
  return hideList.includes(routeName) ? false : true;
};

const MainTab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const BottomTabNavigator = () => {
  return (
    <View style={{ flex: 1 }}>
      <MainTab.Navigator
        screenOptions={{ headerShown: false }}
        initialRouteName="HOME_STACK"
        tabBar={(props) => <Tabbar {...props} />}
      >
        <MainTab.Screen
          name="HOME_STACK"
          component={Home}
          options={({ route }) => {
            return {
              tabBarVisible: getTabBarVisibility(route, ["Home"]),
            };
          }}
        />

        <MainTab.Screen
          name="SEARCH_STACK"
          component={Search}
          options={({ route }) => {
            return {
              tabBarVisible: getTabBarVisibility(route, ["Search"]),
            };
          }}
        />

        <MainTab.Screen
          name="CART_STACK"
          component={Cart}
          options={({ route }) => {
            return {
              tabBarVisible: getTabBarVisibility(route, ["Cart"]),
            };
          }}
        />
        <MainTab.Screen
          name="LIKE_STACK"
          component={Like}
          options={({ route }) => {
            return {
              tabBarVisible: getTabBarVisibility(route, ["Like"]),
            };
          }}
        />
        <MainTab.Screen
          name="MY_STACK"
          component={My}
          options={({ route }) => {
            return {
              tabBarVisible: getTabBarVisibility(route, ["My"]),
            };
          }}
        />
      </MainTab.Navigator>
    </View>
  );
};
export default ({ navigation }) => (
  <Drawer.Navigator
    initialRouteName="Post"
    drawerPosition="right"
    screenOptions={{ headerShown: false }}
    drawerStyle={{ width: 250 }}
    drawerContent={() => <DrawerNavi navigation={navigation} />}
  >
    <Drawer.Screen name="Post" component={BottomTabNavigator} />
  </Drawer.Navigator>
);
