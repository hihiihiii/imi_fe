import React from "react";
import { Text } from "react-native";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import Main from "../../../Components/Main";
import Profile from "../../../pages/Profile";
import ApplicationSettings from "../../../pages/ApplicationSettings";
import BookMark from "../../../pages/BookMark";
import LikePage from "../../../pages/Like/LikePage";
import SocialWeb from "../../../Components/SocialWeb";
import Job from "../../../pages/Profile/Job";
import SelfIntroduction from "../../../pages/Profile/SelfIntroduction";
import Affiliation from "../../../pages/Profile/Affiliation";

const HomeStack = createStackNavigator();
const Home = () => {
  return (
    <HomeStack.Navigator screenOptions={{ headerShown: true }}>
      <HomeStack.Screen
        name="Home"
        component={Main}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></HomeStack.Screen>
      <HomeStack.Screen
        name="ApplicationSettings"
        component={ApplicationSettings}
        options={{
          headerShown: true,
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></HomeStack.Screen>

      <HomeStack.Screen
        name="BookMark"
        component={BookMark}
        options={{
          headerShown: true,
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></HomeStack.Screen>
      <HomeStack.Screen
        name="LikePage"
        component={LikePage}
        options={{
          headerShown: true,
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></HomeStack.Screen>
      <HomeStack.Screen
        name="SocialWeb"
        component={SocialWeb}
        options={{
          headerShown: true,
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></HomeStack.Screen>
      <HomeStack.Screen
        name="Affiliation"
        component={Affiliation}
        options={{
          headerShown: true,
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></HomeStack.Screen>

      <HomeStack.Screen
        name="SelfIntroduction"
        component={SelfIntroduction}
        options={{
          headerShown: true,
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></HomeStack.Screen>

      <HomeStack.Screen
        name="Job"
        component={Job}
        options={{
          headerShown: true,
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></HomeStack.Screen>
    </HomeStack.Navigator>
  );
};

export default Home;
