import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import Influence from "../../../pages/Influence";
import BottomTabNavigator from "../../BottomNavigation";
import Main from "../../../Components/Main";
import CHairMakeup from "../../../pages/Influence/CHairMakeup";
import CInfluence from "../../../pages/Influence/CInfluence";
import CPhotoVideo from "../../../pages/Influence/CPhotoVideo";
import CProducer from "../../../pages/Influence/CProducer";
import CStudio from "../../../pages/Influence/CStudio";
import Detail from "../../../pages/Influence/Detail";
import Portfolio from "../../../pages/Profile/Portfolio";

const LikeStack = createStackNavigator();
const Like = () => {
  return (
    <LikeStack.Navigator screenOptions={{ headerShown: true }}>
      <LikeStack.Screen
        name="Influence"
        component={Influence}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></LikeStack.Screen>
      <LikeStack.Screen
        name="Main"
        component={Main}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></LikeStack.Screen>

      <LikeStack.Screen
        name="CHairMakeup"
        component={CHairMakeup}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></LikeStack.Screen>
      <LikeStack.Screen
        name="CInfluence"
        component={CInfluence}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></LikeStack.Screen>
      <LikeStack.Screen
        name="CPhotoVideo"
        component={CPhotoVideo}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></LikeStack.Screen>
      <LikeStack.Screen
        name="CProducer"
        component={CProducer}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></LikeStack.Screen>
      <LikeStack.Screen
        name="CStudio"
        component={CStudio}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></LikeStack.Screen>
      <LikeStack.Screen
        name="Detail"
        component={Detail}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></LikeStack.Screen>
   
      <LikeStack.Screen
        name="Bottom"
        component={BottomTabNavigator}
        options={{
          headerShown: false,
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
    </LikeStack.Navigator>
  );
};

export default Like;
