import React from "react";
import Store from "../../../pages/Store";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import {
  SellerEntry,
  SellerEntrySecond,
  SellerEntryThird,
} from "../../../pages/Store/SellerEntry";
import AddProduct from "../../../pages/Store/AddProduct";
import CategoryItem from "../../../pages/Store/CategoryItem";
import ContactDetails from "../../../pages/Store/ContactDetails";
import OneOneAnswer from "../../../pages/Store/OneOneAnswer";
import OneOneContact from "../../../pages/Store/OneOneContact";
import OrderHistory from "../../../pages/Store/OrderHistory";
import OrderManagement from "../../../pages/Store/OrderManagement";
import PageThatOtherUsersLook from "../../../pages/Store/PageThatOtherUsersLook";
import Payment from "../../../pages/Store/Payment";
import PaymentCompleted from "../../../pages/Store/PaymentCompleted";
import ProductDetails from "../../../pages/Store/ProductDetails";
import ProductManagement from "../../../pages/Store/ProductManagement";
import ProductModifications from "../../../pages/Store/ProductModifications";
import ShoppingBasket from "../../../pages/Store/ShoppingBasket";
import PaymentDetails from "../../../pages/Store/PaymentDetails";
import CategoryMarket from "../../../pages/Store/CategoryMarket";

const CartStack = createStackNavigator();
const Cart = () => {
  return (
    <CartStack.Navigator screenOptions={{ headerShown: true }}>
      <CartStack.Screen
        name="Store"
        component={Store}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>
      {/* 판매입점첫번째 */}

      {/* 물품 추가 */}
      <CartStack.Screen
        name="AddProduct"
        component={AddProduct}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>
      {/* 카테고리 내역 */}
      <CartStack.Screen
        name="CategoryItem"
        component={CategoryItem}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>
      {/* 문의내역 상세보기 */}

      {/* 1대1답변 상세보기 */}
      <CartStack.Screen
        name="OneOneAnswer"
        component={OneOneAnswer}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>
      {/* 1대1답변 */}
      <CartStack.Screen
        name="OneOneContact"
        component={OneOneContact}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>
      {/* 주문내역 */}
      <CartStack.Screen
        name="OrderHistory"
        component={OrderHistory}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>
      {/* 주문관리 */}
      <CartStack.Screen
        name="OrderManagement"
        component={OrderManagement}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>
      {/* 다른사람이 보는 나의 페이지 */}
      <CartStack.Screen
        name="PageThatOtherUsersLook"
        component={PageThatOtherUsersLook}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>

      {/* 상품관리 */}
      <CartStack.Screen
        name="ProductManagement"
        component={ProductManagement}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>
      {/* 물품수정 */}

      {/* 장바구니 */}
      <CartStack.Screen
        name="ShoppingBasket"
        component={ShoppingBasket}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>
      <CartStack.Screen
        name="CategoryMarket"
        component={CategoryMarket}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      ></CartStack.Screen>
    </CartStack.Navigator>
  );
};
export default Cart;
