import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import React from "react";
import Profile from "../../../pages/Profile";
import ApplicationSettings from "../../../pages/ApplicationSettings";
import TermsConditions from "../../../Components/TermsConditions";
import ChangePassword from "../../../pages/Profile/ChangePassword";
import LikePage from "../../../pages/Like/LikePage";
import BookMark from "../../../pages/BookMark/index";
import SocaialWeb from "../../../Components/SocialWeb";
import SelfIntroduction from "../../../pages/Profile/SelfIntroduction";
import Job from "../../../pages/Profile/Job";
import Affiliation from "../../../pages/Profile/Affiliation";
import PageThatOtherUsersLook from "../../../pages/Store/PageThatOtherUsersLook";

const MyStack = createStackNavigator();
const My = () => {
  return (
    <MyStack.Navigator screenOption={{ headerShown: true }}>
      <MyStack.Screen
        name="ProfileMy"
        component={Profile}
        options={{ ...TransitionPresets.SlideFromRightIOS, headerShown: false }}
      ></MyStack.Screen>
      {/* 어플리케이션 셋팅 */}
      <MyStack.Screen
        name="ApplicationSettings"
        component={ApplicationSettings}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
      {/* 이용약관 */}
      <MyStack.Screen
        name="TermsConditions"
        component={TermsConditions}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
      {/* 좋아요 페이지 */}
      <MyStack.Screen
        name="LikePage"
        component={LikePage}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
      {/* 북마크 */}
      <MyStack.Screen
        name="BookMark"
        component={BookMark}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
      {/* 비밀번호 변경 */}
      <MyStack.Screen
        name="SocaialWeb"
        component={SocaialWeb}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
      <MyStack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
      <MyStack.Screen
        name="SelfIntroduction"
        component={SelfIntroduction}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
      <MyStack.Screen
        name="SocialWeb"
        component={SocaialWeb}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
      <MyStack.Screen
        name="Affiliation"
        component={Affiliation}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
      <MyStack.Screen
        name="Job"
        component={Job}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
      <MyStack.Screen
        name="PageThatOtherUsersLook"
        component={PageThatOtherUsersLook}
        options={{ ...TransitionPresets.SlideFromRightIOS }}
      ></MyStack.Screen>
    </MyStack.Navigator>
  );
};

export default My;
