import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import React from "react";
import { Text } from "react-native";
import SearchPage from "../../../Components/SearchPage";

const SearchStack = createStackNavigator();
const Search = () => {
  return (
    <SearchStack.Navigator screenOptions={{ headerShown: true }}>
      <SearchStack.Screen
        name="SearchPage"
        component={SearchPage}
        options={{
          ...TransitionPresets.SlideFromRightIOS,
        }}
      />
    </SearchStack.Navigator>
  );
};

export default Search;
