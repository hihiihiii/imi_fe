import React from "react";
import {
  StyleSheet,
  TouchableNativeFeedback,
  Image,
  Dimensions,
  TouchableOpacity,
  View,
  SafeAreaView,
} from "react-native";

import {
  DrewText,
  DrewBox,
  FlexBox,
  DrawerNaviStyle,
  DrawerNavi_Text,
} from "../../settings/Styled/Global";
import { CancleBox } from "../../Assets";
import { DrawerActions } from "@react-navigation/native";
const { width, height } = Dimensions.get("window");

export default ({ navigation }) => {
  const data = [
    {
      name: "Client",
      title: "클라이언트",
    },
    {
      name: "Management",
      title: "매니지먼트",
    },
    {
      name: "Sponsorship",
      title: "협찬",
    },
    {
      name: "Notification",
      title: "공지사항",
    },
  ];

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <DrewBox>
        <TouchableOpacity
          onPress={() => {
            navigation.dispatch(DrawerActions.closeDrawer());
          }}
          style={{ alignItems: "flex-end", marginBottom: 30 }}
        >
          <Image source={CancleBox} />
        </TouchableOpacity>
        {data?.map((el, idx) => {
          return (
            <DrawerNaviStyle
              first={idx === 0}
              onPress={() => {
                navigation.navigate(el.name);
              }}
            >
              <DrawerNavi_Text>{el.title}</DrawerNavi_Text>
            </DrawerNaviStyle>
          );
        })}
      </DrewBox>
    </SafeAreaView>
  );
};
