import React, { useEffect } from "react";
import { TouchableOpacity, BackHandler,Text } from "react-native";
import { useNavigation } from "@react-navigation/native";

const useHeader = ({
  headerLeft,
  headerTitle,
  headerRight,
  hideBorder,
  backColor,
}) => {
  const navigation = useNavigation();
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerStyle: {
        elevation: 0,
        borderBottomWidth: hideBorder ? 0 : 1,
        borderBottomColor: "#eee",
      },

      headerStyle: {
        backgroundColor:
          backColor === "task"
            ? "#F6F6F6"
            : backColor === "stream"
            ? "black"
            : "#fff",
      },

      headerTitleAlign: "center",
      headerTitle,
      headerLeft: () =>
        headerLeft ? (
          headerLeft()
        ) : (
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ padding: 10 }}
          >
            <Text></Text>
          </TouchableOpacity>
        ),
      headerRight,
    });
  }, [navigation, headerLeft, headerTitle, headerRight]);
};

const useHandleBack = (handler) => {
  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", handler);

    return () => {
      BackHandler.removeEventListener("hardwareBackPress", handler);
    };
  }, [handler]);
};

export { useHandleBack, useHeader };
