import axios from 'axios';
import React from 'react';
import Profile from './src/pages/Profile';
import SocaialWeb from './src/Components/SocialWeb';
import TermsConditions from './src/Components/TermsConditions';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Navigation from './src/Navigation/StackNavigation';
import PaymentDetails from './src/pages/Store/PaymentDetails';

//axios.defaults.baseURL = process.env.REACT_APP_BASE_URL;

const App = () => {
  return (
    <SafeAreaProvider>
      <Navigation />
    </SafeAreaProvider>
  );
};

export default App;
